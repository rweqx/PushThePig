package com.rweqx.ccomp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;


import com.rweqx.vars.Constants;

/**
 * Button that has a "clicked" and "not clicked" state.
 *
 *	Pretty much works like a checkbox except it's much clearner than ta checkbox... 
 *
 *
 *
 * @author RWEQX
 *
 */

public class MyClickButton extends JButton 
						implements ActionListener{
	
	boolean clicked = false;
	
	public MyClickButton(String s){
		super(s);
		format();
	}
	public MyClickButton(){
		super();
		format();
	}
	
	private void format(){
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.setContentAreaFilled(false);
		this.setFocusable(false);
		this.setFocusPainted(false);
		this.setFont(Constants.MENU_BUTTON_FONT);
		this.addActionListener(this);
	}
	
	
	@Override
    protected void paintComponent(Graphics g) {
		if(clicked){
			g.setColor(Constants.CLICK_SELECTED_COLOUR);
		}else{
			g.setColor(Constants.CLICK_UNSELECTED_COLOUR);
		}
        g.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g);
    }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(clicked){
			clicked = false;
		}else{
			clicked = true;
		}
		repaint();
	}
	
	public boolean isClicked(){
		return clicked;
	}

	public void reset() {
		clicked = false;
	}

	public void setClicked(boolean b) {
		clicked = b;
		repaint();
		
	}
}
