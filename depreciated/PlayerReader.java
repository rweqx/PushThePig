package com.rweqx.io;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.rweqx.vars.Constants;

/*
 * 
 * Reads PlayerID class. 
 */
public class PlayerReader {

	List<String> PlayerList = new ArrayList<String>();
	List<String> IDList = new ArrayList<String>();
	List<String> PlayerIcon = new ArrayList<String>();
	
	
	Reader reader;
	public PlayerReader(Reader reader){
		this.reader = reader;
		parsePlayers(reader.readSaveFile(Constants.PLAYER_ID_FILE_NAME));
	}
	
	
	public void rereadPlayers(){
		IDList.clear();
		PlayerList.clear();
		PlayerIcon.clear();
		
		parsePlayers(reader.readSaveFile(Constants.PLAYER_ID_FILE_NAME));
		
	}
	
	/**
	 * ID;name
	 * ID;name
	 * ... 
	 * @param readSaveFile
	 */
	private void parsePlayers(String[] data) {
		if(data != null){
			for(String s : data){
				String[] split = s.split(";");
				IDList.add(split[0].trim());
				PlayerList.add(split[1].trim());
				if(split.length > 2){
					PlayerIcon.add(split[2].trim());
				}else{
					PlayerIcon.add("null");
				}
			}
		}
		
	}

	public String makeNewUniqueID() {
		Random r = new Random();
		int i = r.nextInt();
		while(IDList.contains(String.valueOf(Math.abs(i)))){
			i = r.nextInt();
		}
		return String.valueOf(Math.abs(i));
		
	}

	

	public List<String> getPlayerList() {
		return PlayerList;
	}


	public void addPlayer(String name, String ID) {
		PlayerList.add(name);
	}

	public String getNameFromID(String playerID) {
		for(int i=0; i<IDList.size(); i++){
			if(IDList.get(i).equals(playerID)){
				return PlayerList.get(i);
			}
		}
		return null;
	}

	public int getIndexFromName(String name){
		for(int i=0; i<PlayerList.size(); i++){
			if(PlayerList.get(i).equals(name))
				return i;
			
		}
		return -1;
		
	}
	public String getPlayerIcon(String name) {
		int i = getIndexFromName(name);
		if(i != -1){
			return PlayerIcon.get(i);
		}
		
		return Constants.NO_IMAGE_ICON;
	}

}
