package com.rweqx.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import com.rweqx.global.Logger;
import com.rweqx.vars.Constants;

/**
 * Writes player ID class...
 * @author RWEQX
 *
 */
public class PlayerWriter {
	
	File saveFile;
	FileWriter FW;
	
	PlayerReader PR;
	
	
	public PlayerWriter(PlayerReader PR){
		this.PR = PR;
		String s = Constants.PLAYER_ID_FILE_NAME;
		saveFile = new File(s);
		
		if(!saveFile.exists()){
			try{
				Logger.log("Player ID file not found, creating new blank one", Logger.PROCESS);
				saveFile.getParentFile().mkdirs();
				saveFile.createNewFile();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		
		try{
			FW = new FileWriter(saveFile, true);
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	
	public void writeNewPlayer(String name){
		String ID = PR.makeNewUniqueID();
		try{
			FW.write(ID + ";" + name);
			FW.write(System.lineSeparator());
			
			FW.flush();
			
			PR.addPlayer(name, ID);
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}

	/**
	 * Writes player name if the name is new!
	 * @param string
	 */
	public void writeIfNew(String name) {
		if(!PR.getPlayerList().contains(name)){
			writeNewPlayer(name);
		}
		
	}
	
	
}
