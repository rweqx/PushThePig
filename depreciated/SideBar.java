package com.rweqx.ui;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import com.rweqx.CardLogic.Game;
import com.rweqx.ccomp.EmptyButton;
import com.rweqx.ccomp.MenuButton;
import com.rweqx.constants.Constants;
import com.rweqx.global.Logger;
import com.rweqx.global.UIMethods;
import com.rweqx.io.GameStateWriter;

/**
 * Currently only for Tracker, will in the future have side buttons for stats as well... Or maybe just make a new sidebar...
 * 
 * Currently, no save button, as realized that Save it quite a useless feature, and everyting is saved as we go along...
 * @Save ^
 * 
 * @author RWEQX
 *
 */
public class SideBar extends JPanel
						implements ActionListener{
	TrackerPanel TP;
	Window w;
	
	int iMode = Constants.TRACK_MODE;
	
	public SideBar(TrackerPanel TP, Window w){
		this.TP = TP;
		this.w = w;
		createPanel();
	}

	MenuButton Reset, NewGame, NewMatch, AppendLast, Save, Back, 
				Refresh, ShowAll, ShowAdvanced, ShowAllyEnemy, 
				ShowCumScore, ShowDailyScore, Win, Lose;

	MenuButton[] TrackButtons;
	MenuButton[] OpenButtons;
	MenuButton[] StatsButtons;
	MenuButton[] GraphsButtons;
	
	private void createPanel(){
		
		Reset = new MenuButton("Reset Cards");
		NewGame = new MenuButton("New Game");
		NewMatch = new MenuButton("New Match");
		Save = new MenuButton("Save");
		AppendLast = new MenuButton("Append Last Match");
		Back = new MenuButton("Back");
		
		Refresh = new MenuButton("Refresh");
		ShowAll = new MenuButton("Show All");
		ShowAdvanced = new MenuButton("Advanced");
		ShowAllyEnemy = new MenuButton("Ally/Enemy");
		
		ShowDailyScore = new MenuButton("Avg Score Daily");
		ShowCumScore = new MenuButton("Avg Cumulative Score");		
		Win = new MenuButton("Win");
		Lose = new MenuButton("Lose");
		
		
		MenuButton[] l1  = {Reset, NewGame, NewMatch, AppendLast};
		TrackButtons = l1.clone();
		MenuButton[] l2 = {Save, Back};
		OpenButtons = l2.clone();
		MenuButton[] l3 = {Refresh, ShowAll, ShowAdvanced, ShowAllyEnemy};
		StatsButtons = l3.clone();
		MenuButton[] l4 = {ShowCumScore, ShowDailyScore, Win, Lose, Refresh};
		GraphsButtons = l4.clone();
	
		MenuButton[] lAll = {Reset, NewGame, NewMatch, AppendLast, Save, Back, 
				Refresh, ShowAll, ShowAdvanced, ShowAllyEnemy,
				ShowCumScore, ShowDailyScore, Win, Lose};
		for(MenuButton MB : lAll){
			MB.addActionListener(this);
		}
		
		UIMethods.setLayout(this);
		addButtons(TrackButtons); // Default == Track Buttons
	}
	
	
	
	private void addButtons(MenuButton[] buttons){
		this.removeAll();
		for(int i=0; i<buttons.length; i++){
			UIMethods.addTo(this, buttons[i], 0, i, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		}
		UIMethods.addTo(this, new EmptyButton(), 0, 10, 1, 1, 1, 10, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
	}
	@Override
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		if(source == Reset){
			w.resetCards();
		}else if(source == NewGame){
			//Save only if the game is not empty
			save();
			TP.newGame();
		}else if(source == NewMatch){
			save();
			TP.newMatch();
		}else if(source == AppendLast){
			Game g = w.getDP().getLastPlayedGame();
			TP.openMatchAppendGame(
					g,
					w.getDP().getPreviousGameOf(g));
		}else if(source == Save){
			save();
		}else if(source == Back){
			//TODO do back stuff. 
		}else if(source == Refresh){
			if(iMode == Constants.STATS_MODE){
				w.getStatsPanel().refresh();
			}else if(iMode == Constants.GRAPHS_MODE){
				w.getGraphsPanel().rebuild();
			}
		}else if(source == ShowAll){
			w.getStatsPanel().viewAll();
		}else if(source == ShowAdvanced){
			w.getStatsPanel().viewAdvanced();
		}else if(source == ShowAllyEnemy){
			w.getStatsPanel().viewAllyEnemy();
		}else if(source == ShowDailyScore){
			w.getGraphsPanel().showDailyChart();
		}else if(source == ShowCumScore){
			w.getGraphsPanel().showCumChart();
		}else if(source == Win){
			w.getGraphsPanel().showWinChart();
		}else if(source == Lose){
			w.getGraphsPanel().showLoseChart();
		}
		
	}
	
	/*
	 * Saves game if the game board is not empty (reset values)
	 */
	private void save(){
		if(!w.getTrackP().isGameEmpty()){
			Logger.log("Saving!", Logger.PROCESS);
			w.save();
		}else{
			Logger.log("Not Saving, board was empty!", Logger.PROCESS);
		}
	}
	public void refresh(int SideBarMode) {
		this.iMode = SideBarMode;
		if(iMode == Constants.TRACK_MODE){
			addButtons(TrackButtons);
		}else if(iMode == Constants.OPEN_GAME_MODE){
			addButtons(OpenButtons);
		}else if(iMode == Constants.STATS_MODE){
			addButtons(StatsButtons);
		}else if(iMode == Constants.GRAPHS_MODE){
			addButtons(GraphsButtons);
		}
		this.repaint();
		this.validate();
		
	}
}
