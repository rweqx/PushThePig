package com.rweqx.settings;

import com.rweqx.vars.Globals;

public class SettingsProperties {
	
	/*
	 * UI Settings
	 */
	private String textScale = "100";
	
	private String checkUpdateOnStart = "false";

	private String showLoggerOnStart = "true";
	

	private String trackerLayout = "default";
	
	private String opacity = "0.5f";
	
	/*
	 * 
	 * SETTER AND GETTER METHODS... 
	 *
	 */
	public String getTextScale(){
		return textScale;
	}
	
	public boolean getCheckUpdateOnStart(){
		if(checkUpdateOnStart.equals("true")){
			return true;
		}
		return false;
	}
	
	public boolean getShowLoggerOnStart(){
		if(showLoggerOnStart.equals("true")){
			return true;
		}
		return false;
		
	}
	public String getTrackerLayout(){
		return trackerLayout;
	}
	public float getOpacity(){
		Float f = Float.parseFloat(opacity);
		return f;
	}
	
	public void setTrackerLayout(String s){
		trackerLayout = s;
	}
	
	public void setTextScale(String s){
		textScale = s;
	}
	
	public void setUpdateOnStart(String s){
		checkUpdateOnStart = s;
	}
	
	public void setShowLoggerOnStart(String s){
		showLoggerOnStart = s;
	}
	public void setOpacity(String s){
		opacity = s;
		Globals.opacity = getOpacity();
	}
	
}
