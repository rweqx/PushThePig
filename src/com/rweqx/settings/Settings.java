package com.rweqx.settings;

import com.rweqx.global.Logger;
import com.rweqx.io.PlayerJWriter;
import com.rweqx.io.SettingsJReader;
import com.rweqx.io.SettingsJWriter;

public class Settings {
	SettingsJReader JReader;
	SettingsJWriter JWriter;
	SettingsProperties properties;
	
	public Settings(){
		JReader = new SettingsJReader();
		JWriter = new SettingsJWriter(JReader);
		JReader.readFile();
		
		properties = JReader.getProperties();
		
		
		if(properties == null){
			Logger.log("Properties are null, something is wrong!", Logger.BAD);
		}else{
			System.out.println("TEXT SCALE " + properties.getTextScale());
		}
	}
	
	public SettingsProperties getProperties(){
		return properties;
	}

	public SettingsJWriter getWriter() {
		return JWriter;
	}
}
