package com.rweqx.vars;

/**
 * Global Variables
 * 
 * @author RWEQX
 *
 */
public class Globals {
	public static final int MATCH_TOTAL_SCORE = 500;

	public static String lastCheckUpdateTime = "";
	
	public static float opacity = 0.5f; //Default value... Adjusted by settings as they are loaded. 
	
	
}
