package com.rweqx.vars;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.datatransfer.DataFlavor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.rweqx.CardLogic.Card;
import com.rweqx.ccomp.MenuButton;

public class Constants {


	public static final String CURRENT_VERSION = "0.9.7";
	public static final String UPDATE_URL = "http://striveprograms.weebly.com/push-the-pig-updates.html";

	public static final String VERSION_TAG = "VERSION";
	public static final String CHANGELOG_TAG = "LOG";
	public static final String DOWNLOAD_TAG = "URL";
	
	public static final String PROGRAM_NAME = "Push the Pig Tracker";
	
	
	
	
	public static final Dimension FRAME_DEFAULT_SIZE = new Dimension(1200, 800);
	public static final Dimension LOG_SIZE = new Dimension(400, 600);
	
	
	public static final Object[] BTRACKER = {0, "Tracker", true};
	public static final Object[] BOPEN = {1, "Open Game", true};
	public static final Object[] BSTATS = {2, "Stats", true};
	public static final Object[] BGRAPHS = {3, "Graphs", true};
	public static final Object[] BSET = {4, "Settings", true};
	
	public static final Object[] T_NEW_GAME = {50, "New Game", false};
	public static final Object[] T_RESET = {60, "Reset Cards", false};
	public static final Object[] T_NEW_MATCH = {70, "New Match", false};
	public static final Object[] T_APPEND = {80, "Append Last Match", false};
	public static final Object[] T_OPEN_PREV = {90, "Open Previous Game", false};
	
	public static final Object[] O_SAVE = {111, "Save", false};
	public static final Object[] O_REFRESH = {113, "Refresh", false};
	
	public static final Object[] S_PLAYERS = {100, "Players", true};
	public static final Object[] S_COMPARE = {11, "Compare All", true};
	public static final Object[] S_TEAMWORK = {13, "Ally/Enemy", true};
	public static final Object[] S_REFRESH = {12, "Refresh", false};
	
	public static final Object[] G_CUM_SCORE ={14, "Avg Cumulative Score", true};
	public static final Object[] G_DAY_SCORE = {15, "Avg Daily Score", true};
	public static final Object[] G_WIN = {16, "Cumulative Winrate", true};
	public static final Object[] G_LOSE = {17, "Cumulative Loserate", true};
	public static final Object[] G_REFRESH = {18, "Refresh", false};
	
	public static final Object[] SET_GENERAL = {19, "General", true};
	public static final Object[] SET_APPEARANCE = {20, "Appearance", true};
	public static final Object[] SET_ADVANCED = {21, "Advanced", true};
	public static final Object[] SET_ABOUT = {22, "About", true};
	
	
	
	public static final Object[][] MENU_BUTTON = {
			{BTRACKER, T_RESET, T_NEW_GAME, T_NEW_MATCH, T_APPEND, T_OPEN_PREV},
			{BOPEN, O_SAVE, O_REFRESH},
			{BSTATS, S_PLAYERS, S_COMPARE, S_TEAMWORK, S_REFRESH},
			{BGRAPHS, G_CUM_SCORE, G_DAY_SCORE, G_WIN, G_LOSE, G_REFRESH},
			{BSET, SET_GENERAL, SET_APPEARANCE, SET_ADVANCED, SET_ABOUT}
	};

	
	
	public static final String LABEL_EXTRA_LARGE = "LABXL";
	public static final String LABEL_LARGE = "LABLARGE";
	public static final String LABEL_MED = "LABMED";
	

	public static final String FONT_FAMILY = "Times New Roman";
	public static final String FONT_FAMILY_1 = "Calibri";
	public static final Font XL_FONT = new Font(FONT_FAMILY_1, Font.BOLD, 24);
	public static final Font LARGER_FONT = new Font(FONT_FAMILY_1, Font.BOLD, 20);
	public static final Font NORMAL_FONT = new Font(FONT_FAMILY_1, Font.BOLD, 13);
	
	public static final Font LOGGER_FONT = new Font(FONT_FAMILY_1, Font.PLAIN, 16);

	public static final Font STATS_FONT = new Font(FONT_FAMILY_1, Font.PLAIN, 22);
	public static final Font STATS_FONT_BOLD = new Font(FONT_FAMILY_1, Font.BOLD, 22);
	public static final Font STATS_FONT_SMALL_BOLD = new Font(FONT_FAMILY_1, Font.BOLD, 16);
	public static final Font STATS_FONT_SMALL = new Font(FONT_FAMILY_1, Font.PLAIN, 12);

	public static final Font PLAYERS_LIST_FONT = new Font(FONT_FAMILY_1, Font.BOLD, 36);

	
	public static final Font MENU_BUTTON_FONT = new Font(FONT_FAMILY, Font.BOLD, 24);
	public static final Font SMALL_MENU_BUTTON_FONT = new Font(FONT_FAMILY, Font.BOLD, 18);
	
	public static final Font CARD_FONT = new Font(FONT_FAMILY, Font.BOLD, 24);

	public static final int CHART_FONT_SIZE = 20;
	
	
	public static final Color CARD_FONT_COLOUR = Color.BLACK;
	public static final Color CARD_BACKGROUND_COLOUR = Color.WHITE;

	public static final Color CARD_AREA_BACKGROUND_COLOUR = new Color(9, 232, 105);
	public static final Color MENU_UNCLICKED_COLOUR = new Color(240, 240, 240);
	public static final Color MENU_CLICKED_COLOUR = new Color(150, 150, 150);
	public static final Color MENU_SELECTED_COLOUR = new Color(210, 210, 210);
	public static final Color MENU_SMALL_SELECTED_COLOUR = new Color(190, 190, 190);
	
	public static final Color EDITING_BACKGROUND_COLOUR = Color.WHITE;
	public static final Color DEFAULT_BACKGROUND_COLOUR = new Color(240, 240, 240);

	public static final Color CLICK_UNSELECTED_COLOUR = new Color(230, 230, 230);
	public static final Color CLICK_SELECTED_COLOUR = CLICK_UNSELECTED_COLOUR.darker();
	
	

	public static final Color LIGHT_DEFAULT = new Color(240, 240, 240);
	public static final Color DARK_DEFAULT = new Color(200, 200, 200);

	public static final Color BUTTON_BORDER_COLOUR = new Color(50, 50, 50);
	

	public static final Color SCROLLBAR_THUMB_COLOR = new Color(150, 150, 150);
	public static final Color SCROLLBAR_TRACK_COLOR = new Color(220, 220, 220);
	
	
	public static final int INDENT = 10;

	public static final int ICON_SIZE = 100;
	
	
	
	
	public static final DataFlavor CARD_FLAVOUR = new DataFlavor(Card.class, Card.class.getSimpleName());

	public static Card H2 = new Card("H 2", -2);
	public static Card H3 = new Card("H 3", -3);
	public static Card H4 = new Card("H 4", -4);
	public static Card H5 = new Card("H 5", -5);
	public static Card H6 = new Card("H 6", -6);
	public static Card H7 = new Card("H 7", -7);
	public static Card H8 = new Card("H 8", -8);
	public static Card H9 = new Card("H 9", -9);
	public static Card H10 =new Card("H 10", -10);
	public static Card HJ =new Card("H J", -20);
	public static Card HQ =new Card("H Q", -30);
	public static Card HK =new Card("H K", -40);
	public static Card HA =new Card("H A", -50);
	public static Card DJ =new Card("D J", 100);
	public static Card C10 =new Card("C 10", 0);
	public static Card SQ =new Card("S Q", -100);

	
	public static final Card[] allCards = {H2, H3, H4, H5, H6, H7, H8, H9, H10, HJ, HQ, HK, HA, DJ, C10, SQ};
	public static final Card[] TransCards = {H2, H3, H4, H5, H6, H7, H8, H9, H10, HJ, HQ, HK, HA};
	public static final Card[] NegCards = {H2, H3, H4, H5, H6, H7, H8, H9, H10, HJ, HQ, HK, HA, SQ};

	public static final List<Card> TransCardsList = new ArrayList<Card>(Arrays.asList(TransCards));
	
	public final static int[] SPECIAL_VALUES = {100, -100, 0};

	public static final String ROOT_DIR = "PTP res/";
	public static final String ICONS_ROOT = ROOT_DIR + "Icons";
	
	public static final String DATA_FILE = ROOT_DIR + "Files/DATA.txt";
	public static final String CHANGELOG_FILE_NAME = ROOT_DIR + "Files/Change.log";
	public static final String PLAYER_ID_FILE_NAME = ROOT_DIR + "Files/PLAYERIDs.txt";

	public static final String SETTINGS_FILE = ROOT_DIR + "Files/preferences.json";
	public static final String PLAYER_DATA_FILE = ROOT_DIR + "Files/Players.json";
	
	
	public static final int CARD_WIDTH = 60;
	public static final int CARD_HEIGHT = 100;

	public static final String HEARTS_FILE = "/Images/HEARTS.jpg";
	public static final String SPADES_FILE = "/Images/SPADES.jpg";
	public static final String DIAMONDS_FILE = "/Images/DIAMONDS.jpg";
	public static final String CLUBS_FILE = "/Images/CLUBS.jpg";
	
	public static final String HEARTS = "heart";
	public static final String CLUBS = "club";
	public static final String DIAMONDS = "diamond";
	public static final String SPADES = "spades";
	public static final String HEARTS_SHORT = "H";
	public static final String CLUBS_SHORT = "C";
	public static final String DIAMONDS_SHORT = "D";
	public static final String SPADES_SHORT = "S";

	
	public static final int TOP_DOWN_BOARD_VIEW = 0;
	public static final int SURROUND_BOARD_VIEW = 1;
	public static final int DEFAULT_BOARD_VIEW = TOP_DOWN_BOARD_VIEW;



	
	
	public static final int BUTTONS_PER_ROW = 3;



	

	public static final int WIN_SCORE = 500;
	public static final int LOSE_SCORE = -500;

	public static final int TRACK_MODE = 0;
	public static final int OPEN_GAME_MODE = 1;
	public static final int STATS_MODE = 2;
	public static final int GRAPHS_MODE = 3;

	
	public static final Object[][] DIVIDER = {{"DIVIDER"}};

	public static final Color DIVIDER_COLOUR = Color.BLACK;
	public static final int DIVIDER_SPACING = 5;

	public static final String NO_GAMES = "NO_GAMES";
	
	public static final String NO_IMAGE_ICON = "BLANK";
	
	public static final int SCROLLBAR_WIDTH = 10;
	
	public static final String NO_RESPONSE = "[NO_RESPONSE]";
	
	






	

	
}
