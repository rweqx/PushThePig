package com.rweqx.PlayerLogic;

import java.util.ArrayList;
import java.util.List;

import com.rweqx.CardLogic.Card;
import com.rweqx.CardLogic.Game;
import com.rweqx.global.Logger;
import com.rweqx.global.Maths;
import com.rweqx.vars.Constants;
import com.rweqx.vars.Globals;

public class Player {
	List<Game> playedGames = new ArrayList<Game>();
	List<Integer> number = new ArrayList<Integer>();
	
	String name; 
	
	int nCards = Constants.allCards.length;
	int[] allCardsEaten = new int[nCards];
	int[] noTransEaten = new int[nCards];
	int[] noAttemptEaten = new int[nCards];
	int[] attemptEaten = new int[nCards];
	
	int badCardsEaten = 0;
	int badCardsNoTransEaten = 0;
	int badCardsNoAttemptEaten = 0;
	int heartsEaten = 0;
	
	
	int attempts;
	int transforms;
	
	
	public Player(String name){
		this.name = name;
		
		attempts = 0;
		transforms = 0;
		
		for(int i=0; i<allCardsEaten.length; i++){
			allCardsEaten[i] = 0;
			noTransEaten[i] = 0;
			noAttemptEaten[i] = 0;
			attemptEaten[i] = 0;
		}
	}
	
	public void addGame(Game g, int position){
		playedGames.add(g);
		number.add(position);
		boolean b = g.transformSuccess(position);
		
		boolean trans = false;
		boolean attempt = false;
		
		if(g.attemptTransform(position)){
			attempts ++;
			attempt = true;
			if(b){
				transforms ++;
				trans = true;
			}
			
		}else if(b){
			Logger.log("User likely forgot to record attempt for game: " + g.getMatchNumber() + "-" + g.getGameNumber(), Logger.WARNING);
		}
		
		
		addCards(g, attempt, trans);
	}
	
	
	List<Integer> cardCount = new ArrayList<Integer>();
	
	
	private void addCards(Game g, boolean attempt, boolean trans){
		List<Card> cards = g.getCards(name);
		for(Card c : cards){
			for(int i=0; i<Constants.allCards.length; i++){
				if(Constants.allCards[i].getName().equals(c.getName())){
					allCardsEaten[i] ++;
					if(!trans){
						noTransEaten[i] ++;
					}
					if(attempt){
						attemptEaten[i]++;
						
					}else{
						noAttemptEaten[i] ++;
					}
					
					//Negative Cards... 
					if(i <= 12 || c.getName().equals(Constants.SQ)){
						badCardsEaten++;
						if(!trans){
							badCardsNoTransEaten++;
							if(!attempt){
								badCardsNoAttemptEaten++;
							}
						}
						if (!c.getName().equals(Constants.SQ)){ //TRACK NUMBER OF NEGATIVE NUMBERS EATEN. 
							if(attempt){
								heartsEaten ++;
							}
						}
					}
				}
			}
		}
	}
	
	
	public int numberOfGames(){
		return playedGames.size();
	}

	public List<Game> getGames(){
		return playedGames;
	}
	
	public List<Integer> getPosition(){
		return number;
	}
	
	public String getName(){
		return name;
	}


	public int getNumberOfAttempts() {
		return attempts;
	}
	public int getNumberOfTransforms(){
		return transforms;
	}

	public double getCardsEaten(int i) {
		if(playedGames.size() == 0){
			return -1;
		}
		return Maths.decimalCalc(allCardsEaten[i], playedGames.size(), 4);
	}

	public double getCardsEatenNotTransform(int j) {
		if(playedGames.size() == 0){
			return -1;
		}
		return Maths.decimalCalc(noTransEaten[j], (playedGames.size() - transforms), 4);
	}

	public double getCardsEatenNotAttempt(int j) {
		if(playedGames.size() == 0){
			return -1;
		}
		return Maths.decimalCalc(noAttemptEaten[j], (playedGames.size()-attempts), 4);
	}

	public double getCardsEatenWhenAttempt(int j) {
		if(playedGames.size() == 0){
			return -1;
		}
		return Maths.decimalCalc(attemptEaten[j], attempts, 4);
	}
	
	public double getBadCardsEaten(){
		return Maths.decimalCalc(badCardsEaten, playedGames.size(), 4);
	}
	public double getBadCardNoTransEaten(){
		return Maths.decimalCalc(badCardsNoTransEaten, (playedGames.size() - transforms), 4);
	}
	public double getBadCardNoAttemptEaten(){
		return Maths.decimalCalc(badCardsNoAttemptEaten, (playedGames.size() - attempts), 4);
	}
	
	public double getHeartsEatenOnAttempt(){
		return Maths.decimalCalc(heartsEaten, attempts);
	}
	

	public double getAvgMatchLength(){
		
		int matches = 0;
		int games = 0;
		
		
		for(Game g : playedGames){
			if(g.isLastGame()){
				boolean past = false;
				for(int i :  g.getMatchScores()){
					if(i >= Globals.MATCH_TOTAL_SCORE  || i <= - Globals.MATCH_TOTAL_SCORE){
						past = true;
					}
				}
				if(past){
					matches ++;
					games += g.getGameNumber();
				}
			}
			//Skips matches which did not end.
			
		}
		if(matches != 0){
			return Maths.decimalCalc(games, matches);
		}
		return 0.0;
	}

}
