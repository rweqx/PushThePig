package com.rweqx.PlayerLogic;

import com.rweqx.vars.Constants;

/**
 * Holds a player's ID, name, and Icon.
 * @author RWEQX
 *
 */
public class PlayerData {
	String name;
	String id;
	String icon = Constants.NO_IMAGE_ICON;
	
	public PlayerData(){
		
	}
	public PlayerData(String name, String id, String icon) {
		this.name = name;
		this.id = id;
		this.icon = icon;
	}
	
	public void setName(String name){
		this.name = name;
	}
	public void setID(String ID){
		this.id = ID;
	}
	public void setIcon(String icon){
		this.icon = icon;
	}
	
	public String getName(){
		return name;
	}
	public String getID(){
		return id;
	}
	public String getIcon(){
		return icon;
	}
}
