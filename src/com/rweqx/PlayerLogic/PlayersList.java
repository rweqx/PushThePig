package com.rweqx.PlayerLogic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.rweqx.io.PlayerJWriter;
import com.rweqx.vars.Constants;

/**
 * Holds a list of playerDatas...
 * @author RWEQX
 *
 */


public class PlayersList {
	List<PlayerData> pData;
	
	PlayerJWriter writer;
	
	public PlayersList(List<PlayerData> playerData, PlayerJWriter writer) {
		pData = playerData;
		this.writer = writer;
		System.out.println("NUMBER OF PLAYERS: " + playerData.size());
		
	}
	
	public List<String> getListOfPlayerNames(){
		List<String> names = new ArrayList<String>();
		
		for(PlayerData p : pData){
			names.add(p.getName());
		}
		return names;
	}
	
	public boolean addIfNew(String name){
		boolean write = true;
		for(PlayerData p : pData){
			if(p.getName().equals(name)){
				write = false;
			}
		}
		
		if(write){
			addNewPlayer(name);
			return true;
		}
		return false;
	}
	public void addNewPlayer(String name){
		String ID = makeNewUniqueID();
		String icon = Constants.NO_IMAGE_ICON;
		
		pData.add(new PlayerData(name, ID, icon));
		
		writer.save(pData);
		
	}
	
	private String makeNewUniqueID() {
		Random r = new Random();
		int i = r.nextInt();
		while(!isIDUnique(Math.abs(i))){
			i = r.nextInt();
		}
		return String.valueOf(Math.abs(i));
	}
	private boolean isIDUnique(int i){
		for(PlayerData p : pData){
			if(p.getID().equals(String.valueOf(i))){
				return false;
			}
		}
		return true;
	}
	
	public String getNameFromID(String ID){
		for(PlayerData p : pData){
			if(p.getID().equals(ID)){
				return p.getName();
			}
		}
		return null;
	}
	public String getIconFromName(String name){
		for(PlayerData p : pData){
			if(p.getName().equals(name)){
				return p.getIcon();
			}
		}
		return Constants.NO_IMAGE_ICON;
	}

	public void setIcon(String name, String loc) {
		for(PlayerData p : pData){
			if(p.getName().equals(name)){
				p.setIcon(loc);
			}
		}
	}
	
	public void deletePlayer(String playerName) {
		for(PlayerData p : pData){
			if(p.getName().equals(playerName)){
				pData.remove(p);
				return;
			}
		}
	}

	public List<PlayerData> getPData() {
		return pData;
	}
	
}
