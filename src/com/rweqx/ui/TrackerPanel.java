package com.rweqx.ui;

import java.awt.GridBagConstraints;
import java.util.List;

import javax.swing.JPanel;

import com.rweqx.CardLogic.Card;
import com.rweqx.CardLogic.Game;
import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.ccomp.DisplayLabel;
import com.rweqx.global.Logger;
import com.rweqx.global.UIMethods;
import com.rweqx.ui.tracker.CardArea;
import com.rweqx.ui.tracker.Mover;
import com.rweqx.ui.tracker.Player;
import com.rweqx.ui.tracker.PlayerArea;
import com.rweqx.vars.Constants;

public class TrackerPanel extends JPanel{
	Window w;
	public TrackerPanel(Window w, PlayersList PL){
		this.w = w;
		
		
		lGameMatchNumber = new DisplayLabel(Constants.LABEL_EXTRA_LARGE);
		lGameMatchNumber.setText(getGameMatchString());
		
		CA = new CardArea();
		PA = new PlayerArea(PL);
		M = new Mover(CA, PA, this);
		
		CA.giveMover(M);
		PA.giveMover(M);
		
		UIMethods.setLayout(this);
		
		
		createPanel();
		
		
	}
	
	Game tempGame;
	
	CardArea CA;
	PlayerArea PA;
	Mover M;
	
	int MatchNumber = 1; 
	int GameNumber = 1; 
	DisplayLabel lGameMatchNumber;
	
	int view = Constants.DEFAULT_BOARD_VIEW;
	
	JPanel inside; 
	private void createPanel(){
		
		inside = new JPanel();
		UIMethods.setLayout(inside);
		UIMethods.addTo(this, lGameMatchNumber, 0, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 10, 0, 0);
		UIMethods.addTo(this, inside, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		if(view == Constants.SURROUND_BOARD_VIEW){
			
			//TODO - add this view...
			
		}else{ //View == Constants.TOP_DOWN_BOARD_VIEW;
			UIMethods.addTo(inside, CA, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
			UIMethods.addTo(inside, PA, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		}
		
	}
	
	private String getGameMatchString(){
		return "Match: " + MatchNumber + " - " + GameNumber;
	}

	public void newGame() {
		if(!isGameEmpty()){
			GameNumber++;
		}
		lGameMatchNumber.setText(getGameMatchString());
		PA.newGame();
		CA.reset();
		
	}
	public void newMatch(){
		if(!isGameEmpty() || !(GameNumber == 1)){
			MatchNumber++;
			GameNumber = 1;
		}
		
		lGameMatchNumber.setText(getGameMatchString());
		PA.newMatch();
		CA.reset();
	}

	public String[][] compileScores() {
		String data[][] = new String[5][];
		
		String gameState[] = {Integer.toString(MatchNumber), Integer.toString(GameNumber)};
		data[0] = gameState;
		
		for(int i=0; i<4; i++){
			data[i+1] = PA.getPlayers()[i].getData();
		}
		
		return data;
	}

	public void resetBoard() {
		PA.reset();
		CA.reset();
		
	}

	public void setMatchGame(int m, int g) {
		this.MatchNumber = m;
		this.GameNumber = g;
		lGameMatchNumber.setText(getGameMatchString());
	}

	public PlayerArea getPlayerArea(){
		return PA;
	}

	public boolean isGameEmpty() {
		for(int i=0; i<4; i++){
			if(!PA.getPlayers()[i].isEmpty()){
				return false;
			};
		}
		return true;
	}

	
	/**
	 * Given Game g, opens to the last match, creating a new game apened to the last match. 
	 * @param g
	 */
	public void openMatchAppendGame(Game g, Game lastGame) {
		int m = g.getMatchNumber();
		int n = g.getGameNumber();		
		reloadScores(g, lastGame);
		setMatchGame(m, n+1);
		
	}

	public void loadGame(Game g, Game lastG) {		
		reloadScores(g, lastG);
		int m = g.getMatchNumber();
		int n = g.getGameNumber();	
		setMatchGame(m, n);
		loadCards(g);
		loadOtherDetails(g);
		repaint();
		validate();
		
	}
	public void loadCards(Game g){
		for(int i=0; i<4; i++){
			List<Card> cards = g.getCards(i);
			for(Card c : cards){
				CA.removeCard(c);
			}
			PA.getPlayers()[i].loadCards(cards);
		}
	}
	
	private void reloadScores(Game g, Game lastGame){

		int m = g.getMatchNumber();
		int n = g.getGameNumber();
		Logger.log("Opening Match Game " + m + " " + n, Logger.PROCESS);
		String names[] = g.getNames();
		int mScores[] = g.getMatchScores();
		
		int deltaScores[] = {0, 0, 0, 0};
		if(lastGame != null){
			deltaScores = g.getGameScores();
		}

		
		for(int i=0; i<4; i++){
			PA.getPlayers()[i].setPlayerName(names[i]);
			PA.getPlayers()[i].setMatchScore(mScores[i]);
			PA.getPlayers()[i].setLastScore(deltaScores[i]);
		}

	}

	/**
	 * Loads attempt and place
	 * @param g
	 */
	private void loadOtherDetails(Game g){
		for(int i=0; i<4; i++){
			PA.getPlayers()[i].setPos(g.getPos(i));
			PA.getPlayers()[i].setAttempt(g.isAttempt(i));
		}
		
	}
}
