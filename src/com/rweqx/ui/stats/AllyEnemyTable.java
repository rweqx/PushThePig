package com.rweqx.ui.stats;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.rweqx.PlayerLogic.Player;
import com.rweqx.ccomp.DisplayLabel;
import com.rweqx.ccomp.MyScrollPane;
import com.rweqx.ccomp.MyTable;
import com.rweqx.global.Maths;
import com.rweqx.global.UIMethods;
import com.rweqx.stats.AllyEnemy;
import com.rweqx.stats.AllyEnemy.ScoreKeeper;
import com.rweqx.vars.Constants;
import com.rweqx.stats.StatsManager;

public class AllyEnemyTable extends JPanel{
	StatsManager SM;
	List<Player> players;
	AllyEnemy AE;
	
	public AllyEnemyTable(StatsManager SM){
		this.SM = SM;
		this.AE = SM.getAllyEnemy();
		
		players = SM.getPlayers();
		
		makeData();
		makePanel();
		
	}
	
	
	
	DisplayLabel l1 = new DisplayLabel(Constants.LABEL_LARGE);
	DisplayLabel l2 = new DisplayLabel(Constants.LABEL_LARGE);
	JPanel titleBar;
	
	private void makePanel(){
		UIMethods.setLayout(this);
		MyScrollPane[] scrolls = new MyScrollPane[8];
		MyTable[] tables  = new MyTable[8];
		
		JPanel tablesPanel = new JPanel();
		UIMethods.setLayout(tablesPanel);
		
		int perRow = 2;
		for(int i=0; i<8; i++){
			titles[0] = tableTitles[i];
			tables[i] = new MyTable(packagedData.get(i), titles);
			
			scrolls[i] = new MyScrollPane(true, true);
			scrolls[i].setViewportView(tables[i]);
			
			UIMethods.addTo(tablesPanel, scrolls[i], i%perRow, i/perRow, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		}
		
		titleBar = new JPanel();
		UIMethods.setLayout(titleBar);
		
		l1.setHorizontalAlignment(SwingConstants.CENTER);
		l2.setHorizontalAlignment(SwingConstants.CENTER);

		l1.setText("Avg Score / Game vs normal");
		l2.setText("Win % vs normal");
		
		UIMethods.addTo(titleBar, l1, 0, 0, 1, 1, 1, 1,GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(titleBar, l2, 1, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		

		UIMethods.addTo(this, titleBar, 0, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, tablesPanel, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
	}
	
	
	@Override
	protected void paintComponent(Graphics g){
		
		int i = this.getWidth()/2;
		int j = l1.getHeight();
		Dimension d = new Dimension(i, j);
		l1.setPreferredSize(d);
		l2.setPreferredSize(d);
		l1.setMinimumSize(d);
		l2.setMinimumSize(d);
		super.paintComponent(g);
		
		
	}
	List<Object[][]> packagedData = new ArrayList<Object[][]>();

	Object[][] dataScoreSame;
	Object[][] dataScoreAfter;
	Object[][] dataScoreBefore;
	Object[][] dataScoreOpposite;
	Object[][] dataWinSame;
	Object[][] dataWinAfter;
	Object[][] dataWinBefore; 
	Object[][] dataWinOpposite;
	String[] titles;
	//Note after and before reversed, because the variables are the 2nd person is ___
	// while the table titles are the 1st person is ______

	String[] tableTitles = {"With", "With", 
			"Before", "Before",
			"After", "After",
			"Opposite", "Opposite"
			};
	
	private void makeData(){
		int size = players.size();
		
		dataScoreSame = new Object[size][size + 1];
		dataScoreAfter = new Object[size][size + 1];
		dataScoreBefore = new Object[size][size + 1];
		dataScoreOpposite = new Object[size][size + 1];
		dataWinSame = new Object[size][size + 1];
		dataWinAfter = new Object[size][size + 1];
		dataWinBefore = new Object[size][size + 1];
		dataWinOpposite = new Object[size][size + 1];
		
		titles = new String[size + 1];
		
		titles[0] = "...";
		
		for(int i=0; i<size; i++){
			String name = players.get(i).getName();
			dataScoreSame[i][0] = name;
			dataScoreAfter[i][0] = name;
			dataScoreBefore[i][0] = name;
			dataScoreOpposite[i][0] = name;
			
			dataWinSame[i][0] = name;
			dataWinAfter[i][0] = name;
			dataWinBefore[i][0] = name;
			dataWinOpposite[i][0] = name;
			
			titles[i + 1] = name;			
		}
		
		ScoreKeeper[][] scoreSame = AE.getSameScore();
		ScoreKeeper[][] scoreAfter = AE.getAfterScore();
		ScoreKeeper[][] scoreBefore = AE.getBeforeScore();
		ScoreKeeper[][] scoreOpposite = AE.getOppositeScore();
		
		String[][] avgScores = SM.getAverageScores();
		int[] wins = SM.getNumberOfWins();
		int[] gamesPlayed = SM.getNumberOfGamesPlayed();
		
		ScoreKeeper[][] winSame = AE.getSameWin();
		ScoreKeeper[][] winAfter = AE.getAfterWin();
		ScoreKeeper[][] winBefore = AE.getBeforeWin();
		ScoreKeeper[][] winOpposite = AE.getOppositeWin();
		
		
		
		for(int i=0; i<size; i++){
			for(int j=0; j<size; j++){
				if(i != j){
					double avg = Double.parseDouble(avgScores[i][1]);
					double win = Maths.decimalCalc(wins[i], gamesPlayed[i], 4);
					
					if(scoreSame[i][j].getCount() != 0){
						dataScoreSame[i][j+1] = Maths.subtract(scoreSame[i][j].getAvg(), avg) + 
								" (" + scoreSame[i][j].getCount() + ")";
					}
					
					if(scoreAfter[i][j].getCount() != 0){
						dataScoreAfter[i][j+1] = Maths.subtract(scoreAfter[i][j].getAvg(), avg)+ 
								" (" + scoreAfter[i][j].getCount() + ")";
					}
					
					if(scoreBefore[i][j].getCount()!= 0){
						dataScoreBefore[i][j+1] = Maths.subtract(scoreBefore[i][j].getAvg(), avg)+ 
								" (" + scoreBefore[i][j].getCount() + ")";
					}
					
					if(scoreOpposite[i][j].getCount() != 0){
						dataScoreOpposite[i][j+1] = Maths.subtract(scoreOpposite[i][j].getAvg(), avg)+ 
								" (" + scoreOpposite[i][j].getCount() + ")";
						
					}
					
					if(winSame[i][j].getCount() != 0){
						dataWinSame[i][j+1] = Maths.multiply100(Maths.subtract(winSame[i][j].getAvg(), win, 4)) +
							"(" + winSame[i][j].getCount() + ")";
					}
					if(winAfter[i][j].getCount() != 0){
						dataWinAfter[i][j+1] = Maths.multiply100(Maths.subtract(winAfter[i][j].getAvg(), win, 4)) + 
								" (" + winAfter[i][j].getCount() + ")";
					}
					
					if(winBefore[i][j].getCount() != 0){
						dataWinBefore[i][j+1] = Maths.multiply100(Maths.subtract(winBefore[i][j].getAvg(), win, 4)) + 
								" (" + winBefore[i][j].getCount() + ")";
					}
					
					if(winOpposite[i][j].getCount() != 0){
						dataWinOpposite[i][j+1] = Maths.multiply100(Maths.subtract(winOpposite[i][j].getAvg(), win, 4)) + 
								" (" + winOpposite[i][j].getCount() + ")";		
					}
				}
			}
		}
		
		packagedData.add(dataScoreSame);
		packagedData.add(dataWinSame);

		packagedData.add(dataScoreAfter);
		packagedData.add(dataWinAfter);
		
		packagedData.add(dataScoreBefore);
		packagedData.add(dataWinBefore);

		packagedData.add(dataScoreOpposite);
		packagedData.add(dataWinOpposite);
		
		
	}
}
