package com.rweqx.ui.stats;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.LookAndFeel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.ccomp.DisplayLabel;
import com.rweqx.ccomp.DividerPanel;
import com.rweqx.ccomp.EmptyButton;
import com.rweqx.ccomp.MyIconButton;
import com.rweqx.ccomp.MyScrollPane;
import com.rweqx.ccomp.MyTable;
import com.rweqx.global.FileImporter;
import com.rweqx.global.UIMethods;
import com.rweqx.stats.StatsManager;
import com.rweqx.ui.StatsPanel;
import com.rweqx.vars.Constants;

/**
 * Holds the player's personal statistics (profile);
 * @author RWEQX
 *
 */
public class PlayerProfilePanel extends JPanel implements ActionListener{
	
	StatsManager SM;
	StatsPanel SP;
	
	PlayersList PL;
	
	JButton bBack;
	MyIconButton bIcon;
	
	DisplayLabel lName;
	
	String name;
	


	JFileChooser JFC;
	
	public PlayerProfilePanel(String name, StatsManager SM, StatsPanel SP, PlayersList PL){
		this.SM = SM;
		this.SP = SP;
		this.name = name;
		this.PL = PL;
		
		setupPanel();
	}
	public String getPlayerName(){
		return name;
	}
	
	JPanel topbarPanel;
	JPanel dataPanel;
	JPanel iconPanel; 
	JPanel centerPanel;
	JPanel tablesPanel;
	
	MyScrollPane dataScroll;
	private void setupPanel(){
		try {
			LookAndFeel previousLF = UIManager.getLookAndFeel();
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			JFC = new JFileChooser();
			UIManager.setLookAndFeel(previousLF);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		JFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		
		
		lName = new DisplayLabel(Constants.LABEL_LARGE);
		lName.setText(name);
		lName.setHorizontalAlignment(SwingConstants.CENTER);
		
		bBack = new JButton("BACK");
		bBack.setContentAreaFilled(false);
		bBack.setBorderPainted(false);
		bBack.setFont(new Font("Times New Roman", Font.BOLD, 16));
		
		bIcon = new MyIconButton(PL.getIconFromName(name));
		
		topbarPanel = new JPanel();
		UIMethods.setLayout(topbarPanel);
		dataPanel = new JPanel();
		UIMethods.setLayout(dataPanel);
		iconPanel = new JPanel();
		UIMethods.setLayout(iconPanel);
		centerPanel = new JPanel();
		UIMethods.setLayout(centerPanel);
		tablesPanel = new JPanel();
		UIMethods.setLayout(tablesPanel);
		
		dataScroll = new MyScrollPane(false, true);
		dataScroll.setViewportView(dataPanel);
		
		UIMethods.setLayout(this);
		
		UIMethods.addTo(topbarPanel, bBack, 0, 0, 1, 1, 0, 1, GridBagConstraints.VERTICAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(topbarPanel, new EmptyButton(), 1, 0, 1, 1, 1, 1, GridBagConstraints.VERTICAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		UIMethods.addTo(iconPanel, bIcon, 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE, GridBagConstraints.WEST, 0, 10, 0, 0);
		UIMethods.addTo(iconPanel, lName, 0, 1, 1, 1, 0, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, 0, 10, 0, 0);
		UIMethods.addTo(iconPanel, new EmptyButton(), 1, 0, 1, 2, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		
		makeDataPanel();
		makeTablesPanel();
		
		UIMethods.addTo(centerPanel, iconPanel, 0, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(centerPanel, dataScroll, 0, 1, 1, 1, 1, 5, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(centerPanel, tablesPanel, 0, 2, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		

		UIMethods.addTo(this, topbarPanel, 0, 0, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.NORTH, 0, 0, 0, 0);
		UIMethods.addTo(this, centerPanel, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		/*
		iconPanel.setBorder(BorderFactory.createLineBorder(Color.ORANGE));
		topbarPanel.setBorder(BorderFactory.createLineBorder(Color.RED));
		centerPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		*/
		
		bBack.addActionListener(this);
		bIcon.addActionListener(this);
	}

	
	/**
	 * Card specific stats!
	 * 
	 */
	private void makeTablesPanel(){

		String[] titles = SM.getCardStatTitles();
		Object[][] data = SM.getCardStats(name);
		
		MyScrollPane scroll = new MyScrollPane(true, false);
		MyTable table = new MyTable(data, titles);
		
		/*
		table.setFont(Constants.STATS_FONT);
		table.getTableHeader().setFont(Constants.STATS_FONT_BOLD);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setRowHeight(Constants.STATS_FONT.getSize() + 6);
		*/
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		scroll.setViewportView(table);
		UIMethods.addTo(tablesPanel, scroll, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
	}
	/**
	 * Stats Data  [][]
	 * 	each array = 1 stat. First entry = stat name, further entries = stat ... 
	 */
	private void makeDataPanel(){
		Object[][] data = SM.getAllNonCardStats(name);

		String statName;
		Object statType; 
		
		for (int i=0; i<data.length; i++){
			if(data[i] == Constants.DIVIDER){
				
				UIMethods.addTo(dataPanel, new DividerPanel(), 0, i, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.NORTH, 0, 0, 0, 0);
			}else{
				statName = (String) data[i][0];
				statType = data[i][1];
			
				JPanel panel = makeDataTab(statName, statType); 
				UIMethods.addTo(dataPanel, panel, 0, i, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.NORTH, 0, 5, 0, 5);
			}
		}
		
		UIMethods.addTo(dataPanel, new EmptyButton(), 0, data.length, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.NORTH, 0, 0, 0, 0);			
	}
	
	private JPanel makeDataTab(String statName, Object statType){
		JPanel panel = new JPanel();
		UIMethods.setLayout(panel);
		
		DisplayLabel d1 = new DisplayLabel(Constants.LABEL_LARGE);
		DisplayLabel d2 = new DisplayLabel(Constants.LABEL_LARGE);
		d1.setText(statName);
		d2.setText(String.valueOf(statType));
		d1.setHorizontalAlignment(SwingConstants.LEFT);
		d2.setHorizontalAlignment(SwingConstants.RIGHT);
		
		
		UIMethods.addTo(panel, d1, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 5, 0, 0);
		UIMethods.addTo(panel, d2, 1, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 5);
		return panel;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		
		if(source == bBack){
			SP.switchBack();
		}else if(source == bIcon){
			
			int response = JFC.showOpenDialog(new JFrame());
			
			if(response == JFileChooser.APPROVE_OPTION){
				File f = JFC.getSelectedFile();
				String s = FileImporter.importFileForIcon(f, name);
				PL.setIcon(name, s);
				bIcon.updateIcon(s);
				this.repaint();
				
			}else{
				//ignore.
			}
		}
		
	}
}
