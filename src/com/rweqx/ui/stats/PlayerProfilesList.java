package com.rweqx.ui.stats;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import com.rweqx.PlayerLogic.Player;
import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.ccomp.EmptyButton;
import com.rweqx.ccomp.MyScrollPane;
import com.rweqx.ccomp.ProfileButton;
import com.rweqx.global.UIMethods;
import com.rweqx.stats.StatsManager;
import com.rweqx.ui.StatsPanel;
import com.rweqx.vars.Constants;

public class PlayerProfilesList extends JPanel implements ActionListener, ComponentListener{

	
	StatsPanel SP;
	StatsManager SM;
	
	PlayersList PL;
	
	MyScrollPane scroll;
	JPanel inside;
	
	List<Player> players;
	
	List<ProfileButton> profileButtons = new ArrayList<ProfileButton>();
	
	
	public PlayerProfilesList(StatsManager SM, StatsPanel SP, PlayersList PL){
		this.SM = SM;
		this.SP = SP;
		this.PL = PL;
		
		UIMethods.setLayout(this);
		scroll = new MyScrollPane();
		
		inside = new JPanel();

		inside.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, Constants.SCROLLBAR_WIDTH));
		UIMethods.setLayout(inside);
		
		players = SM.getPlayers();

		UIMethods.addTo(this, scroll, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		scroll.setViewportView(inside);
		
		scroll.addComponentListener(this);
		inside.addComponentListener(this);
		this.addComponentListener(this);
		
		addStuff();
		
		
	}
	
	private void addStuff(){
		int x = Constants.BUTTONS_PER_ROW;
		for(int i=0; i<players.size(); i++){
			String name = players.get(i).getName();
			profileButtons.add(new ProfileButton(name, PL.getIconFromName(name)));
			UIMethods.addTo(inside, profileButtons.get(i), i%x, i/x, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0,0);
			profileButtons.get(i).addActionListener(this);
		}
		
		UIMethods.addTo(inside, new EmptyButton(), 0, players.size()/x, x, 0, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
	}

	public void rebuild() {
		players = SM.getPlayers();
		inside.removeAll();
		profileButtons.clear();
		addStuff();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		
		if(source instanceof ProfileButton){
			SP.findPanelByNameAndSwitch(((ProfileButton)source).getPlayerName(), false);
		}
		
	}

	/**
	 * DEPRECIATED. 
	 * @ PPC's overwritten paintComponent is an easier (and probably better) way
	 * 		to accomplish the same thing 
	 * Sets preferred size to be a square based on the width, instead of just filling as much 
	 * space as possible vertically... 
	 */
	/*
	@Override
	protected void paintComponent(Graphics g){
		int i = this.getWidth()/Constants.BUTTONS_PER_ROW;
		Dimension d = new Dimension(i, i);
		for(PlayerProfileCard PPC : profileCards){
			PPC.setPreferredSize(d);
		}
		System.out.println(i);
		super.paintComponent(g);
		
	}
	*/

	@Override
	public void componentHidden(ComponentEvent arg0) {}

	@Override
	public void componentMoved(ComponentEvent arg0) {}

	@Override
	public void componentResized(ComponentEvent arg0) {
		for(ProfileButton PB : profileButtons){
			PB.setDims((scroll.getWidth() - scroll.getVerticalScrollBar().WIDTH) / Constants.BUTTONS_PER_ROW);
		
		}
		inside.repaint();
		inside.validate();
	}

	@Override
	public void componentShown(ComponentEvent arg0) {}
	

}
