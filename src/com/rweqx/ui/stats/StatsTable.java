package com.rweqx.ui.stats;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.rweqx.ccomp.DisplayLabel;
import com.rweqx.ccomp.MyScrollPane;
import com.rweqx.ccomp.MyTable;
import com.rweqx.global.UIMethods;
import com.rweqx.stats.StatsManager;
import com.rweqx.ui.StatsPanel;
import com.rweqx.vars.Constants;

/**
 * Takes data from all players and shows them together as a table!
 * 
 * @author RWEQX
 *
 */

public class StatsTable extends JPanel implements ActionListener {

	StatsManager SM;
	StatsPanel SP;

	public StatsTable(StatsManager SM, StatsPanel SP) {
		this.SM = SM;
		this.SP = SP;
		UIMethods.setLayout(this);

		bBack = new JButton();
		bBack.setText("BACK");
		bBack.setFont(Constants.LARGER_FONT);
		bBack.setContentAreaFilled(false);
		bBack.addActionListener(this);

		createPanel();
	}

	JButton bBack;
	
	JPanel captureStats; 

	private void createPanel() {
		int size = SM.getPlayers().size();

		String columnNames[] = { "Name", "Games Played", "Games Won", "Games Lost",
				"Matches Played", "Matches Won", "Matches Lost", "Avg Match Length",
				"Transform Attempts", "Transform Success"
				
				};


		
		Object[][] data = new Object[size][columnNames.length];
		
		String[][] s = SM.getAverageScores();

		String columnNames2[] = {
				"Name",
				"Avg Score/Game", "Avg Score no A", "Avg Score no T",
				"StDev", "StDev no A", "StDev no T",
				"Capture", "A Capture", "No A Capture", "No T Capture",	
				"Bad No A"
		};
		
		Object[][] data2 = new Object[size][columnNames2.length];

		int x[] = SM.getNumberOfGamesPlayed();
		int y[] = SM.getNumberOfWins();
		int z[] = SM.getNumberOfLosses();
		
		double length[] = SM.getAvgMatchLength();

		int v = SM.getTotalNumberOfGamesPlayed();

		int m[][] = SM.getMatchWinLoss();
		
		int attempts[] = SM.getNumberOfAttempts();
		int transform[] = SM.getNumberOfTransforms();
		
		double stdev[][] = SM.getStdev();
		
		double capture[] = SM.getCaptureTotals();
		double aCapture[] = SM.getAttemptCaptureTotals();
		double nACapture[] = SM.getNoAttemptCaptureTotals();
		double nTCapture[] = SM.getNoTranformCaptureTotals();
		
		double bNACapture[] = SM.getBadNoAttemptCaptureTotals();
		
		for (int i = 0; i < data.length; i++) {
			String name = SM.getPlayers().get(i).getName();
			data[i][0] = name;
			data[i][1] = (x[i] + percentString(x[i], v));
			data[i][2] = new String(y[i] + percentString(y[i], x[i]));
			data[i][3] = new String(z[i] + percentString(z[i], x[i]));
			
			
			int mPlayed = m[i][0] + m[i][1] + m[i][2];
			
			data[i][4] = mPlayed;
			data[i][5] = new String(m[i][0] + percentString(m[i][0], mPlayed));
			data[i][6] = new String(m[i][2] + percentString(m[i][2], mPlayed));

			data[i][7] = length[i];
			
			data[i][8] = new String(attempts[i] + percentString(attempts[i], x[i]));
			data[i][9] = new String(transform[i] + percentString(transform[i], attempts[i]));			
		
			data2[i][0] = name;
			data2[i][1] = s[i][1];
			data2[i][2] = s[i][2];
			data2[i][3] = s[i][3];
			
			data2[i][4] = String.valueOf(stdev[i][0]);
			data2[i][5] = String.valueOf(stdev[i][1]);
			data2[i][6] = String.valueOf(stdev[i][2]);
			
			data2[i][7] = String.valueOf(capture[i]);
			data2[i][8] = String.valueOf(aCapture[i]);
			data2[i][9] = String.valueOf(nACapture[i]);
			data2[i][10] = String.valueOf(nTCapture[i]);
			data2[i][11] = String.valueOf(bNACapture[i]);
		}

		MyScrollPane topScroll = new MyScrollPane(true, false);
		MyTable topTable = new MyTable(data, columnNames);
		
		MyScrollPane botScroll = new MyScrollPane(true, false);
		MyTable botTable = new MyTable(data2, columnNames2);

		topScroll.setViewportView(topTable);
		botScroll.setViewportView(botTable);
		
		
		/*
		 * Do stuff related to individual card captures
		 */
		
		captureStats = new JPanel();
		UIMethods.setLayout(captureStats);
		List<Object[][]> captureData = SM.getAllCaptureData();
		
		JTable[] captureTables = new JTable[captureData.size() - 1];
		MyScrollPane[] scrolls = new MyScrollPane[captureData.size() - 1];
		
		for(int i=0; i<captureData.size()-1; i++){
			String[] titles = new String[Constants.allCards.length + 1];
			for(int j=0; j<titles.length; j++){
				titles[j] = (String) captureData.get(0)[i][j]; 
			}
			captureTables[i] = new JTable(captureData.get(i+1), titles);
			
			scrolls[i] = new MyScrollPane(false, true);
			scrolls[i].setViewportView(captureTables[i]);
			
			UIMethods.addTo(captureStats, scrolls[i], 0, i, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		}
		
		
		
		UIMethods.addTo(this, bBack, 0, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, topScroll, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, botScroll, 0, 2, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, captureStats, 0, 3, 1, 1, 1, 3, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
	}

	private String percentString(int i, int j) {
		return " (" + (double) (Math.round((double) i / j * 10000)) / 100 + "%)";
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		if (source == bBack) {
			SP.switchBack();
		}

	}

	public void rebuild() {
		this.removeAll();
		createPanel();
	}
}
