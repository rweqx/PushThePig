package com.rweqx.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.rweqx.global.DateMaker;
import com.rweqx.global.Logger;
import com.rweqx.global.UIMethods;
import com.rweqx.stats.StatsManager;
import com.rweqx.ui.graphs.AvgScoreChart;
import com.rweqx.ui.graphs.CumScoreChart;
import com.rweqx.ui.graphs.LoseRateChart;
import com.rweqx.ui.graphs.WinRateChart;

import javafx.application.Platform;
import javafx.concurrent.Task;

public class GraphsPanel extends JPanel{
	
	StatsManager SM;
	JPanel currentPanel;
	
	AvgScoreChart ASC;
	CumScoreChart CSC;
	WinRateChart WRC;
	LoseRateChart LRC;
	
	public GraphsPanel(StatsManager SM){
		this.SM = SM;
		buildGraphs();
		UIMethods.setLayout(this);
		UIMethods.addTo(this, CSC, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		currentPanel = CSC;
	}
	
	private void buildGraphs(){
		Logger.log("Building Graphs", Logger.PROCESS);
		long start = DateMaker.currentExactTime();
		System.out.println(this.getHeight() + " " + this.getWidth());
		ASC = new AvgScoreChart(this.getWidth(), this.getHeight(), SM);
		CSC = new CumScoreChart(this.getWidth(), this.getHeight(), SM);
		WRC = new WinRateChart(this.getWidth(), this.getHeight(), SM);
		LRC = new LoseRateChart(this.getWidth(), this.getHeight(), SM);
		
		
		long end = DateMaker.currentExactTime();
		Logger.log("Finished Building Graphs: " + (double)(end-start)/1000 + " s", Logger.PROCESS);
	}
	
	
	
	
	private void switchView(JPanel p, boolean force) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				if(p != currentPanel || force){
					if(currentPanel != null){
						remove(currentPanel);
					}
					
					Dimension d = new Dimension(getWidth(), getHeight());
					p.setPreferredSize(d);
					p.setMinimumSize(d);
					UIMethods.addTo(GraphsPanel.this, p, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
					currentPanel = p;
					
					
					Logger.log("Switching View", Logger.PROCESS);
					repaint();
					validate();
					
				}
			}
		});
	}

	public void showLoseChart(){
		switchView(LRC, false);
	}
	public void showWinChart(){
		switchView(WRC, false);
	}
	public void showCumChart() {
		switchView(CSC, false);
	}
	public void showDailyChart(){
		switchView(ASC, false);
	}
	
	
	RebuildGraphsTask task = null;
	//TODO - Make so rebuild remembers the panel you were on... 
	public void rebuild(){
		//remove(currentPanel);
		//currentPanel = null; // Forces it to swap panel to new one!
		SM.refresh();
		task = new RebuildGraphsTask();	
		Platform.runLater(task);
		
	}
	
	public class RebuildGraphsTask extends Task<Boolean>{
		@Override 
		protected Boolean call() throws Exception{
			long start = DateMaker.currentExactTime();
			ASC.rebuild();
			CSC.rebuild();
			WRC.rebuild();
			LRC.rebuild();
			long end = DateMaker.currentExactTime();
			Logger.log("Finished Rebuilding Graphs: " + (double)(end-start)/1000 + " s", Logger.PROCESS);
			
			
			/*
			SwingUtilities.invokeLater(()->{
				ASC.refresh();
				CSC.refresh();
				WRC.refresh();
				LRC.refresh();
			});
			*/
			
			return true;
		
		}
		
	}
	
}
