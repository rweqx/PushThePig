package com.rweqx.ui;

import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.rweqx.PlayerLogic.Player;
import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.global.Logger;
import com.rweqx.global.UIMethods;
import com.rweqx.stats.StatsManager;
import com.rweqx.ui.stats.AllyEnemyTable;
import com.rweqx.ui.stats.PlayerProfilePanel;
import com.rweqx.ui.stats.PlayerProfilesList;
import com.rweqx.ui.stats.StatsTable;

public class StatsPanel extends JPanel{
	
	JPanel currentPanel = null;
	
	
	List<PlayerProfilePanel> playerProfiles = new ArrayList<PlayerProfilePanel>();
	List<Player> players;
	PlayerProfilesList PPL;
	StatsTable ST;	

	AllyEnemyTable AET;
	
	StatsManager SM;
	//PlayerReader PR;
	PlayersList PL;
	
	
	Window w;
	public StatsPanel(StatsManager SM, Window w, PlayersList PL){
		this.SM = SM;
		this.w = w;
		this.PL = PL;
		
		ST = new StatsTable(SM, this);
		
		PPL = new PlayerProfilesList(SM, this, PL);
		
		players = SM.getPlayers();

		AET = new AllyEnemyTable(SM);
		
		
		for(int i=0; i<players.size(); i++){
			Logger.log("Creating Player " + players.get(i).getName(), Logger.PROCESS);
			playerProfiles.add(new PlayerProfilePanel(players.get(i).getName(), SM, this, PL));
		}
		
		
		UIMethods.setLayout(this);		
		UIMethods.addTo(this, PPL, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		currentPanel = PPL;
		
	}

	
	public void findPanelByNameAndSwitch(String name, boolean force){
		JPanel p = null;
		for(int i = 0; i<players.size(); i++){
			if(players.get(i).getName().equals(name)){
				p = playerProfiles.get(i);
			}
		}
		if(p!= null){
			switchView(p, force);
		}else{
			Logger.log("COULD NOT FIND PALYER PANEL TO SWAP TO", Logger.BAD);
		}
		
	}
	
	public void switchBack(){
		switchView(PPL, false);
		PPL.repaint();
		PPL.validate();
	}
	
	public void switchView(JPanel p, boolean force) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				if(p != currentPanel || force){
					remove(currentPanel);
					//Dimension d = new Dimension(getWidth(), getHeight());
					//p.setPreferredSize(d);
					//p.setMinimumSize(d);
					UIMethods.addTo(StatsPanel.this, p, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
					currentPanel = p;
					repaint();
					validate();
					revalidate();			
				}
			}
		});
	}

	public void rebuild(){
		PPL.rebuild();
		ST.rebuild();
		playerProfiles.clear();
		for(int i=0; i<players.size(); i++){
			playerProfiles.add(new PlayerProfilePanel(players.get(i).getName(), SM, this, PL));
		}
		
		switchView(PPL, true);
	}
	
	public void refresh(){
		SM.refresh();
		JPanel temp = currentPanel;
		String name = null;
		if(currentPanel instanceof PlayerProfilePanel){
			name = ((PlayerProfilePanel) currentPanel).getPlayerName();
		}
		rebuild();
		
		if(name == null){
			switchView(temp, false);
		}else{
			findPanelByNameAndSwitch(name, true);
		}

		
	}
	
	public void viewAll(){
		switchView(ST, false);
	}
	public void viewPlayers(){
		switchView(PPL, false);
		
	}
	
	public void viewAdvanced(){
		
	}


	public void viewAllyEnemy() {
		switchView(AET, false);
	}
	
	

	
}
