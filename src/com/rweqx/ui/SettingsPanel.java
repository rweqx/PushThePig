package com.rweqx.ui;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.ccomp.DisplayLabel;
import com.rweqx.ccomp.EmptyButton;
import com.rweqx.ccomp.InputPane;
import com.rweqx.ccomp.MyButton;
import com.rweqx.ccomp.MySelectableButton;
import com.rweqx.ccomp.MyTextField;
import com.rweqx.ccomp.SettingsButton;
import com.rweqx.global.DateMaker;
import com.rweqx.global.Logger;
import com.rweqx.global.Maths;
import com.rweqx.global.UIMethods;
import com.rweqx.global.Updater;
import com.rweqx.settings.SettingsProperties;
import com.rweqx.vars.Constants;
import com.rweqx.vars.Globals;

public class SettingsPanel extends JPanel
							implements ActionListener{

	Updater updater;
	SettingsProperties SP;
	PlayersList PL;
	
	public SettingsPanel(Updater u, SettingsProperties SP, PlayersList PL){
		updater = u;
		this.SP = SP;
		this.PL = PL;
		
		UIMethods.setLayout(this);
		makePanel();
	}
	
	JPanel pGeneral, pAppearance, pAbout, pAdvanced, pEditPlayers, pPlayerContainer, pEditToolbar;
	
	List<MySelectableButton> playerSelectButtons = new ArrayList<MySelectableButton>();
	
	SettingsButton bBack, bEditDelete, bAddNew;
	
	MyButton bOpenLoggerFrame, bForceCheckUpdate, bEditPlayers;
	
	
	private void makePanel(){
		pGeneral = new JPanel();
		UIMethods.setLayout(pGeneral);
		
		pAppearance = new JPanel();
		UIMethods.setLayout(pAppearance);
		
		pAbout = new JPanel();
		UIMethods.setLayout(pAbout);
		
		pAdvanced = new JPanel();
		UIMethods.setLayout(pAdvanced);
		
		pEditPlayers = new JPanel();
		UIMethods.setLayout(pEditPlayers);
		pPlayerContainer = new JPanel();
		UIMethods.setLayout(pPlayerContainer);
		pEditToolbar = new JPanel();
		UIMethods.setLayout(pEditToolbar);
		
		bEditPlayers = new MyButton("Edit Players");
		bEditPlayers.addActionListener(this);
		
		
		
		addContentToSubPanels();
		switchPanel(pGeneral);
	}
	
	MyTextField tTextScale;
	
	JLabel lUpdateAvailable, lLastChecked;
	SettingsButton bCheckForUpdate;
	JCheckBox cbCheckUpdateOnStart;
	
	private void addContentToSubPanels(){
		//pGeneral
		UIMethods.addTo(pGeneral, bEditPlayers, 0, 0, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 10, 10, 10, 10);
		
		UIMethods.addTo(pGeneral, new EmptyButton(), 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		
		//pAppearance
		tTextScale= new MyTextField(SP.getTextScale());
		UIMethods.addTo(pAppearance, tTextScale, 0, 1, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		tTextScale.addActionListener(this);
		tTextScale.setFont(Constants.LARGER_FONT);
		UIMethods.addTo(pAppearance, new EmptyButton(), 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		//pAbout
		lUpdateAvailable = new DisplayLabel(Constants.LABEL_LARGE);
		lUpdateAvailable.setText("No Update Available");
		lLastChecked = new DisplayLabel(Constants.LABEL_MED);
		lLastChecked.setText(Globals.lastCheckUpdateTime);
		
		bCheckForUpdate = new SettingsButton(SettingsButton.Size.MEDIUM, "Check for Updates");
		cbCheckUpdateOnStart = new JCheckBox("Check for Updates on Start");
		cbCheckUpdateOnStart.setSelected(SP.getCheckUpdateOnStart());
		
		bCheckForUpdate.addActionListener(this);
		cbCheckUpdateOnStart.addActionListener(this);
		
		UIMethods.addTo(pAbout, bCheckForUpdate, 0, 0, 1, 2, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(pAbout, lUpdateAvailable, 1, 0, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(pAbout, lLastChecked, 1, 1, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(pAbout, cbCheckUpdateOnStart, 2, 0, 2, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
	
		
		//pAdvanced
		bOpenLoggerFrame = new MyButton("Open Logger");
		UIMethods.addTo(pAdvanced, bOpenLoggerFrame, 0, 0, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(pAdvanced, new EmptyButton(), 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		bOpenLoggerFrame.addActionListener(this);
		
		//Edit Players
		List<String> players = PL.getListOfPlayerNames();
		for(int i=0; i<players.size(); i++){
			MySelectableButton MSB = new MySelectableButton(players.get(i), false, false);
			playerSelectButtons.add(MSB);
			UIMethods.addTo(pPlayerContainer, playerSelectButtons.get(i), i%Constants.BUTTONS_PER_ROW, i/Constants.BUTTONS_PER_ROW, 
					1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		}
		UIMethods.addTo(pPlayerContainer, new EmptyButton(), 0, players.size()/Constants.BUTTONS_PER_ROW + 1, 3, 1, 1, 1,
				GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		bBack = new SettingsButton(SettingsButton.Size.SMALL, "Back" );
		bAddNew = new SettingsButton(SettingsButton.Size.SMALL, "+");
		bEditDelete = new SettingsButton(SettingsButton.Size.SMALL, "Edit");
		bBack.addActionListener(this);
		bAddNew.addActionListener(this);
		bEditDelete.addActionListener(this);
		
		UIMethods.addTo(pEditToolbar, bBack, 0, 0, 1, 1, 0, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(pEditToolbar, new EmptyButton(), 1, 0, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(pEditToolbar, bAddNew, 2, 0, 1, 1, 0, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(pEditToolbar, bEditDelete, 3, 0, 1, 1, 0, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		UIMethods.addTo(pEditPlayers, pEditToolbar, 0, 0, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(pEditPlayers, pPlayerContainer, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
	}
	
	boolean isEditingPlayers = false;
	
	public void refresh(){
		SwingUtilities.invokeLater(()->{
			this.repaint();
			this.validate();
		});
	}
	private void switchPanel(JPanel p){
		this.removeAll();
		UIMethods.addTo(this, p, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		SwingUtilities.invokeLater(()->{
			this.repaint();
			this.validate();
			this.revalidate();
		});
	}
	
	public void switchPanelByName(String name){
		//TODO Should be Constants.SET_XXX[1]; ...
		if(name.equals("General")){
			switchPanel(pGeneral);
		}else if(name.equals("Appearance")){
			switchPanel(pAppearance);
		}else if(name.equals("Advanced")){
			switchPanel(pAdvanced);
		}else if(name.equals("About")){
			switchPanel(pAbout);
		}
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		if(source == bOpenLoggerFrame){
			Logger.showLogger();
		}else if(source == bForceCheckUpdate){
			if(updater.isUpdateAvailable()){
				//Change UI, Update is available... 
				
			}
		}else if(source == bEditPlayers){
			switchPanel(pEditPlayers);
		}else if(source == bBack){
			switchPanel(pGeneral);
			//Switches back to gen panel
		}else if(source == bAddNew){
			for(int i=0; i<playerSelectButtons.size(); i++){
				playerSelectButtons.get(i).setSelectable(false);
			}
			
			//Show dialog to add new player
			InputPane input = new InputPane("Name of New Player");
			
			int result = JOptionPane.showConfirmDialog(null, 
					input, 
					"Add Player",
					JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.PLAIN_MESSAGE);
			if(result == JOptionPane.OK_OPTION){
				String s = input.getInput();
				if(!s.equals(Constants.NO_RESPONSE)){
					if(PL.addIfNew(s)){
						//Add to UI
						MySelectableButton MSB = new MySelectableButton(s, false, false);
						playerSelectButtons.add(MSB);
						int i = playerSelectButtons.size() - 1;
						UIMethods.addTo(pPlayerContainer, MSB, i%Constants.BUTTONS_PER_ROW, i/Constants.BUTTONS_PER_ROW, 
								1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
						
						SwingUtilities.invokeLater(()->{
							this.validate();
						});
						
					}
				}
			}
			
			
		}else if(source == bEditDelete){
			if(isEditingPlayers){
				isEditingPlayers = false;
				bEditDelete.setText("Edit");
				
				//Delete selected and set remaining to edit mode...
				//Count down so you don't run into error while deleting stuff!
				for(int i=playerSelectButtons.size()-1; i>-1; i--){
					if(playerSelectButtons.get(i).isSel()){
						pPlayerContainer.remove(playerSelectButtons.get(i));
						PL.deletePlayer(playerSelectButtons.get(i).getPlayerName());
						playerSelectButtons.remove(i);
					}else{
						playerSelectButtons.get(i).setSelectable(false);
					}
				}
				refresh();
				
			}else{
				isEditingPlayers = true;
				bEditDelete.setText("Delete");
				for(int i=0; i<playerSelectButtons.size(); i++){
					playerSelectButtons.get(i).setSelectable(true);
				}
				refresh();
			}
		}else if (source == tTextScale){
			String s = tTextScale.getText().trim();
			if(Maths.isInt(s)){
				SP.setTextScale(s);
			}
		}else if(source == cbCheckUpdateOnStart){
			SP.setUpdateOnStart(String.valueOf(cbCheckUpdateOnStart.isSelected()));
			
		}else if(source == bCheckForUpdate){
			lLastChecked.setText("Last Checked: " + DateMaker.getDateYYYYMMDD() + " " + DateMaker.getDateHHMMSS());
			boolean b = updater.isUpdateAvailable();
		
			if(b){
				lUpdateAvailable.setText("Update Available!");
				updater.runUpdate();
			}else{
				lUpdateAvailable.setText("No Update Available");
			}
		}
			
		
	}
}
