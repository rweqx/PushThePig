package com.rweqx.ui;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.rweqx.CardLogic.Game;
import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.ccomp.DisplayLabel;
import com.rweqx.ccomp.EmptyButton;
import com.rweqx.ccomp.MyButton;
import com.rweqx.ccomp.MyScrollPane;
import com.rweqx.global.Logger;
import com.rweqx.global.UIMethods;
import com.rweqx.io.DataParser;
import com.rweqx.stats.StatsManager;
import com.rweqx.vars.Constants;

public class OpenGamePanel extends JPanel implements ActionListener{

	DataParser DP;
	Window w;
	
	Game games[];
	
	MyScrollPane scroll;
	

	StatsManager SM;
	TrackerPanel openP;
	JPanel listP;
	JPanel inside;
	
	public OpenGamePanel(DataParser DP, Window w, PlayersList PL, StatsManager SM) {
		this.DP = DP;
		this.w = w;
		this.SM = SM;
		
		openP = new TrackerPanel(w, PL);
		
		UIMethods.setLayout(this);
		listP = new JPanel();
		UIMethods.setLayout(listP);
		
		games = DP.getAllGames(null, null);
		
		scroll = new MyScrollPane();
		

		inside = new JPanel();
		UIMethods.setLayout(inside);
		MyButton bBack = new MyButton();
		bBack.setText("Back");
		bBack.addActionListener(this);
		
		UIMethods.addTo(inside, bBack, 0, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(inside, openP, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		createPanel();
		scroll.setViewportView(listP);
		UIMethods.addTo(this, scroll, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		
	}

	private void createPanel(){
		listP.removeAll();
		int j = scroll.getVerticalScrollBar().getWidth() + 1;
		
		for(int i=0; i<games.length; i++){
			GamePanel GP = new GamePanel(i+1, games[i].getMatchNumber(), games[i].getGameNumber(), games[i].getDate());
			UIMethods.addTo(listP, GP, 0, i, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, j);
		}
		UIMethods.addTo(listP, new EmptyButton(), 0, games.length, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, j);
		
	}
	
	public class GamePanel extends JPanel implements MouseListener{
		String date;
		int Game; 
		int Match;
		int number;
		public GamePanel(int number, int Match, int Game, String date){
			this.Game = Game;
			this.Match = Match;
			this.date = date;
			this.number = number;
			
			String GameMatchNumber = Match + " - " + Game;
			
			UIMethods.setLayout(this);
			DisplayLabel d1 = new DisplayLabel(Constants.LABEL_LARGE);
			if(number < 10){
				d1.setText(number + ".      " + GameMatchNumber);
			}else if(number < 100){
				d1.setText(number + ".    " + GameMatchNumber);
			}else if(number < 1000){
				d1.setText(number + ".  " + GameMatchNumber);
			}else{
				d1.setText(number + ". " + GameMatchNumber);
			}
			
			DisplayLabel d2 = new DisplayLabel(Constants.LABEL_LARGE);
			d2.setText(date);
			
			UIMethods.addTo(this, d1, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
			UIMethods.addTo(this, d2, 1, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
			d2.setHorizontalAlignment(SwingConstants.RIGHT);
			
			this.addMouseListener(this);
			
			if(number % 2 == 0){
				this.setBackground(Constants.LIGHT_DEFAULT);
			}else{
				this.setBackground(Constants.DARK_DEFAULT);
			}
		}
		
		@Override
		public void mouseClicked(MouseEvent arg0) {
			Logger.log("Opening Game", Logger.PROCESS);
			openGame(Match, Game);
		}
		@Override
		public void mouseEntered(MouseEvent arg0) {}
		@Override
		public void mouseExited(MouseEvent arg0) {}
		@Override
		public void mousePressed(MouseEvent arg0) {}
		@Override
		public void mouseReleased(MouseEvent arg0) {}
		
		
	}
	
	public void openGame(int match, int game){
		Game g = DP.getGameFromMatchGame(match, game);
		openP.loadGame(g,  DP.getPreviousGameOf(g));
		this.remove(scroll);
		UIMethods.addTo(this, inside, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		redraw();
		
	}
	
	public void refresh(){
		SM.refresh();
		games = DP.getAllGames(null, null);
		createPanel();
		redraw();
	}
	
	public void redraw(){
		this.repaint();
		this.validate();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.remove(inside);
		UIMethods.addTo(this, scroll, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		redraw();
	}

}
