package com.rweqx.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.rweqx.CardLogic.Game;
import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.global.Logger;
import com.rweqx.global.UIMethods;
import com.rweqx.global.Updater;
import com.rweqx.io.DataParser;
import com.rweqx.io.GameStateWriter;
import com.rweqx.main.Main;
import com.rweqx.settings.SettingsProperties;
import com.rweqx.stats.StatsManager;
import com.rweqx.vars.Constants;

//Creates the main JFrame
public class Window extends JFrame implements WindowListener{
	GameStateWriter GSW;
	DataParser DP;
	Updater updater;
	StatsManager SM;
	Main main;
	
	
	SideMenu SideM;
	TrackerPanel TrackP;
	StatsPanel StatsP;
	SettingsPanel SetP;
	GraphsPanel GraphsP;
	
	SettingsProperties SP;
	OpenGamePanel OpenGameP;
	
	
	
	JPanel mainPanel = new JPanel();
	JPanel centerPanel = new JPanel();
	
	//PlayerReader PR;
	PlayersList PL;
	
	int currentPanel;
	
	int SideBarMode = Constants.TRACK_MODE;
	
	public Window(GameStateWriter GSW, DataParser DP, StatsManager SM, Updater updater, PlayersList PL, SettingsProperties SP, Main main){
		this.main = main;
		this.SP = SP;
		this.GSW = GSW;
		this.DP = DP;
		this.SM = SM;
		this.PL = PL;
		this.updater = updater;
		this.setTitle(Constants.PROGRAM_NAME);
		createAndAddElements();
	}
	
	private void createAndAddElements(){
		UIMethods.setLayout(mainPanel);
		UIMethods.setLayout(centerPanel);
		
		Logger.log("Creating Side Menu", Logger.PROCESS);
		SideM = new SideMenu(this);

		Logger.log("Creating Tracker Panel", Logger.PROCESS);
		TrackP = new TrackerPanel(this, PL);

		Logger.log("Creating Stats Panel", Logger.PROCESS);
		StatsP = new StatsPanel(SM, this, PL);

		Logger.log("Creating Graphs", Logger.PROCESS);
		GraphsP = new GraphsPanel(SM);

		Logger.log("Creating Open Game Panel", Logger.PROCESS);
		OpenGameP = new OpenGamePanel(DP, this, PL, SM);
		
		Logger.log("Creating Settings Panel", Logger.PROCESS);
		SetP = new SettingsPanel(updater, SP, PL);
		
		
		UIMethods.addTo(mainPanel, SideM, 0, 0, 1, 1, 0, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(mainPanel, centerPanel, 1, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
	//	UIMethods.addTo(mainPanel, SideB, 2, 0, 1, 1, 0, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		setView(TrackP);

		currentPanel = (int)Constants.BTRACKER[0];
	}
	
	public void showWindow(){
		this.setContentPane(mainPanel);
		this.setSize(Constants.FRAME_DEFAULT_SIZE);
		
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		this.addWindowListener(this);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(
        		dim.width/2-this.getSize().width/2, 
        		dim.height/2 - this.getSize().height/2);
		this.setVisible(true);
		
		
	}
	boolean trackDisturbed = false;
	public void switchPanel (int i){
		if(i == (int)Constants.BTRACKER[0]){
			SideBarMode = Constants.TRACK_MODE;
			setView(TrackP);
		}else if(i == (int)Constants.BSTATS[0]){
			SideBarMode = Constants.STATS_MODE;
			setView(StatsP);
		}else if(i == (int)Constants.BSET[0]){
			setView(SetP);
		}else if(i == (int)Constants.BOPEN[0]){
			setView(OpenGameP);
		}else if(i == (int)Constants.BGRAPHS[0]){
			SideBarMode = Constants.GRAPHS_MODE;
			setView(GraphsP);
		}else{
			Logger.log("Cannot switch to view with ID " + i + " either does not exist or an error occured...", Logger.WARNING);
		}
		currentPanel = i;
		
	}
	
	private void setView(JPanel p){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				centerPanel.removeAll();
				Dimension d = new Dimension(centerPanel.getWidth(), centerPanel.getHeight());
				p.setPreferredSize(d);
				p.setMinimumSize(d);
				UIMethods.addTo(centerPanel, p, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
				centerPanel.repaint();
				centerPanel.validate();
			}
		});
		
	}
	
	public void save(){
		DP.saveGame(TrackP.compileScores());
	}

	public void resetCards() {
		TrackP.resetBoard();
		centerPanel.repaint();
		centerPanel.validate();
	}

	public TrackerPanel getTrackP() {
		return TrackP;
	}

	public DataParser getDP() {
		return DP;
	}

	public SettingsPanel getSetPanel() {
		return SetP;
	}	

	public StatsPanel getStatsPanel() {
		return StatsP;
	}

	public GraphsPanel getGraphsPanel() {
		return GraphsP;
	}
	public OpenGamePanel getOpenP() {
		return OpenGameP;
	}

	public void refreshUI() {
		centerPanel.repaint();
		centerPanel.validate();
		centerPanel.revalidate();
		this.repaint();
		this.validate();
		this.revalidate();
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		main.saveAndExit();
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowClosed(WindowEvent arg0) {}
	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	@Override
	public void windowOpened(WindowEvent arg0) {}


}
