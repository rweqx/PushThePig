package com.rweqx.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.rweqx.CardLogic.Game;
import com.rweqx.ccomp.EmptyButton;
import com.rweqx.ccomp.MenuButton;
import com.rweqx.global.Logger;
import com.rweqx.global.UIMethods;
import com.rweqx.vars.Constants;

/** 
 * Menu with buttons to adjust the center view of the program
 * @author RWEQX
 *
 */

public class SideMenu extends JPanel
						implements ActionListener{

	Window win;
	
	JPanel smallButtonsPanel;
	int selected = -1;
	
	
	public SideMenu(Window w){
		win = w;
		
		createMenu();
		
	}

	int length = Constants.MENU_BUTTON.length;
	
	MenuButton[][] allButtons = new MenuButton[length][];
	MenuButton lastClickedSmallButton[] = new MenuButton[length];
	
	private void createMenu(){
		
		UIMethods.setLayout(this);
		smallButtonsPanel = new JPanel();
		UIMethods.setLayout(smallButtonsPanel);
		//smallButtonsPanel.setBackground(Constants.MENU_SELECTED_COLOUR);
		
		
		for(int i=0; i<length; i++){
			int l = Constants.MENU_BUTTON[i].length;
			allButtons[i] = new MenuButton[l];
			for(int j=0; j<l; j++){
				Object o[] = (Object[]) Constants.MENU_BUTTON[i][j];
				allButtons[i][j] = new MenuButton((int)o[0], (String)o[1], (boolean)o[2]);
				allButtons[i][j].addActionListener(this);
				
			}
			UIMethods.addTo(this, allButtons[i][0], 0, i*2, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		}
		
		UIMethods.addTo(this, new EmptyButton(), 0, length * 2, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		
		//Set stats default to "Players" and Graphs default to "Cumulative Avg Score" and settings.
		allButtons[2][1].setMenuSelected();
		allButtons[3][1].setMenuSelected();
		allButtons[4][1].setMenuSelected();

		lastClickedSmallButton[0] = null;
		lastClickedSmallButton[1] = null;
		lastClickedSmallButton[2] = allButtons[2][1];
		lastClickedSmallButton[3] = allButtons[3][1];
		lastClickedSmallButton[4] = allButtons[4][1];
		//Add BTracker.
		allButtons[0][0].setMenuSelected();
		
		
		int i = (int) Constants.BTRACKER[0];
		addButtons(i);
		
		
		}
	
	private void addButtons(int i){
		this.remove(smallButtonsPanel);
		smallButtonsPanel.removeAll();
		if(i != -1){
			for(int j=1; j<allButtons[i].length; j++){
				UIMethods.addTo(smallButtonsPanel, allButtons[i][j], 0, j, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
			}
			
			UIMethods.addTo(this, smallButtonsPanel, 0, (i*2 + 1), 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		}
		if(selected != -1){
			allButtons[selected][0].setMenuDeselected();
		}
		
		selected = i;
		validate();	
	}

	
	@Override 
	protected void paintComponent(Graphics g){
		Dimension d = new Dimension(200, this.getHeight());
		this.setPreferredSize(d);
	//	this.setSize(d);
	//	this.setMaximumSize(d);
		this.setMinimumSize(d);
		
		super.paintComponent(g);
	}
	
	@Override
	public void actionPerformed(ActionEvent AE) {
		MenuButton source = (MenuButton)AE.getSource();
		int ID = source.getID();
		
		for(int i=0; i<length; i++){
			for(int j=0; j<allButtons[i].length; j++){
				if(source == allButtons[i][j]){
					
					if(j==0){
						if(selected != i){
							Logger.log("Switching Views to ID" + i, Logger.PROCESS);
							win.switchPanel(allButtons[i][j].getID());
						
							allButtons[i][j].setMenuSelected();
							addButtons(i);
						}
					}else{						
						if(ID == (int)Constants.T_RESET[0]){
							win.resetCards();
						}else if(ID == (int) Constants.T_NEW_GAME[0]){
							save();
							win.getTrackP().newGame();
						}else if(ID == (int) Constants.T_NEW_MATCH[0]){
							save();
							win.getTrackP().newMatch();
						}else if(ID == (int) Constants.T_APPEND[0]){
							Game g = win.getDP().getLastPlayedGame();
							win.getTrackP().openMatchAppendGame(g, win.getDP().getPreviousGameOf(g));
						}else if(ID == (int) Constants.T_OPEN_PREV[0]){
							Game g = win.getDP().getLastPlayedGame();
							win.getTrackP().loadGame(g, win.getDP().getPreviousGameOf(g));
						}else if(ID == (int) Constants.O_SAVE[0]){
							save();
							
						}else if(ID == (int) Constants.O_REFRESH[0]){
							win.getOpenP().refresh();
						}else if(ID == (int) Constants.S_PLAYERS[0]){
								
							win.getStatsPanel().viewPlayers();
							
						}else if(ID == (int) Constants.S_COMPARE[0]){
							win.getStatsPanel().viewAll();
						}else if(ID == (int) Constants.S_TEAMWORK[0]){
							win.getStatsPanel().viewAllyEnemy();
						}else if(ID == (int) Constants.S_REFRESH[0]){
							win.getStatsPanel().refresh();
						}else if(ID == (int) Constants.G_CUM_SCORE[0]){
							win.getGraphsPanel().showCumChart();
						}else if(ID == (int) Constants.G_DAY_SCORE[0]){
							win.getGraphsPanel().showDailyChart();
						}else if(ID == (int) Constants.G_WIN[0]){
							win.getGraphsPanel().showWinChart();
						}else if(ID == (int) Constants.G_LOSE[0]){
							win.getGraphsPanel().showLoseChart();
						}else if(ID == (int) Constants.G_REFRESH[0]){
							win.getGraphsPanel().rebuild();
						}else if(ID == (int) Constants.SET_GENERAL[0]){
							win.getSetPanel().switchPanelByName((String) Constants.SET_GENERAL[1]);
						}else if(ID == (int) Constants.SET_APPEARANCE[0]){
							win.getSetPanel().switchPanelByName((String) Constants.SET_APPEARANCE[1]);
						}else if(ID == (int) Constants.SET_ABOUT[0]){
							win.getSetPanel().switchPanelByName((String) Constants.SET_ABOUT[1]);
						}else if(ID == (int) Constants.SET_ADVANCED[0]){
							win.getSetPanel().switchPanelByName((String) Constants.SET_ADVANCED[1]);
						}

						boolean switched = source.isSwitchablePanel();
						/*
						if(ID == (int)Constants.G_REFRESH[0]){
							if(lastClickedSmallButton[3] != null){
								lastClickedSmallButton[3].setMenuDeselected();
							}
							lastClickedSmallButton[3] = allButtons[3][1];
							allButtons[3][1].setMenuSelected();
							//Special rules for pressing refresh on graphs... 
							
						}else*/ 
						if(switched){
							if(lastClickedSmallButton[i] != null){
								lastClickedSmallButton[i].setMenuDeselected();
							}

							lastClickedSmallButton[i] = allButtons[i][j];
							allButtons[i][j].setMenuSelected();
						}
					}
				}
			}
		}
	}
	
	private void save(){
		if(!win.getTrackP().isGameEmpty()){
			Logger.log("Saving!", Logger.PROCESS);
			win.save();
		}else{
			Logger.log("Not Saving, board was empty!", Logger.PROCESS);
		}
	}
	
}
