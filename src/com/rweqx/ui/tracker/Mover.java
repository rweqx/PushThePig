package com.rweqx.ui.tracker;

import javax.swing.JPanel;

import com.rweqx.CardLogic.Card;
import com.rweqx.ui.TrackerPanel;

public class Mover {
	CardArea CA;
	PlayerArea PA;
	
	Player[] players; 
	
	TrackerPanel TP;
	public Mover(CardArea CA, PlayerArea PA, TrackerPanel TP){
		this.CA = CA;
		this.PA = PA;
		this.TP = TP;
		players = PA.getPlayers();
		
	}

	public void moveCard(Card c, JPanel target) {
		removeAll(c);
		
		if(target == CA){
			CA.addCard(c);
		}else{
			for(Player p : players){
				if(target == p){
					p.addCard(c);
				}
			}
		}
		TP.validate();
		TP.repaint();
		TP.revalidate();
	}
	
	private void removeAll(Card c){
		CA.removeCard(c);
		for(Player p : players){
			p.removeCard(c);
		}
	}
}
