package com.rweqx.ui.tracker;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.rweqx.global.UIMethods;
import com.rweqx.vars.Constants;

/**
 * Field with 4 buttons, position spots 1, 2, 3, or 4.
 * @author RWEQX
 *
 */
public class PositionField extends JPanel implements ActionListener{
	PositionButton A[] = new PositionButton[4];
	
	String selectedName = "";
	
	int playerNumber;
	
	public PositionField(int playerNumber){
		super();
		this.playerNumber = playerNumber;
		
		A[0] = new PositionButton("1", "1");
		A[1] = new PositionButton("2", "2");
		A[2] = new PositionButton("3", "3");
		A[3] = new PositionButton("4", "4");
		
		A[0].addActionListener(this);
		A[1].addActionListener(this);
		A[2].addActionListener(this);
		A[3].addActionListener(this);
		
		UIMethods.setLayout(this);
		
		UIMethods.addTo(this, A[0], 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, A[1], 1, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, A[2], 2, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, A[3], 3, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		for(PositionButton p : A){
			if(p.getID().equals(String.valueOf(playerNumber))){
				selectedName = p.getID();
			}
		}
		
	}
	
	public void setPosition(String s){
		selectedName = s;
		repaint();
	}

	
	private class PositionButton extends JButton{
		String ID = null;
		
		/**
		 * 
		 * @param s - displayed text
		 * @param name - button name (ID)
		 */
		public PositionButton(String s, String id){
			super(s);
			this.ID = id;
			this.setContentAreaFilled(false);
			this.setFocusPainted(false);
			this.setFocusable(false);
		}
		
		public String getID(){
			return ID;
		}
		
		@Override
		protected void paintComponent(Graphics g){
			if(selectedName.equals(ID)){
				g.setColor(Constants.CLICK_SELECTED_COLOUR);
			}else{
				g.setColor(Constants.CLICK_UNSELECTED_COLOUR);
			}
	        g.fillRect(0, 0, getWidth(), getHeight());
	        super.paintComponent(g);
		}
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		PositionButton source = (PositionButton) event.getSource();
		String newName = source.getID();
		
		if(!selectedName.equals(newName)){
			((PlayerArea) this.getParent().getParent()).updatePos(playerNumber, newName, selectedName);
			selectedName = newName;
		}
		
		repaint();
	}

	public String getPosition() {
		return selectedName;
	}
	
	public int getPlayerNumber(){
		return playerNumber;
	}
}
