package com.rweqx.ui.tracker;

import java.awt.GridBagConstraints;

import javax.swing.JPanel;

import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.global.Logger;
import com.rweqx.global.UIMethods;
import com.rweqx.vars.Constants;

public class PlayerArea extends JPanel{

	boolean transform = false;

	Mover m;
	Player players[] = new Player[4];
	
	public PlayerArea(PlayersList PL){
		this.PL = PL;
	}
	PlayersList PL;
	
	private void createPanel(){

		UIMethods.setLayout(this);
		
		for(int i=0; i<players.length; i++){
			players[i] = new Player(i+1, m, this, PL);
			UIMethods.addTo(this, players[i], i, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		}
	}
	

	
	
	public void giveMover(Mover m) {
		this.m = m;

		createPanel();
	}

	public Player[] getPlayers() {
		return players;
	}
	
	public void newMatch() {
		transform = false;
		for(Player p : players){
			p.newMatch();
		}
	}
	public void newGame(){
		transform = false;
		for(Player p : players){
			p.newGame();
		}
	}
	
	public boolean isTransform() {
		return transform;
	}
	public void setTransform(boolean b) {
		if(transform != b){
			transform = b;
			for(Player p : players){
				p.updateScore();
			}
		}
	}
	
	
	public void updatePigSheepTransform() {
		boolean b = false; 
		for(Player p : players){
			if(p.updatePigSheepTransform()){
				b = true;
			}
		}
		setTransform(b);
	}
	
	public void reset() {
		transform = false;
		for(Player p : players){
			p.reset();
		}
	}
	public void setPlayerNames(String[] l) {
		for(int i=0; i<players.length; i++){
			players[i].setPlayerName(l[i]);
		}	
	
	}
	
	
	public void updatePos(int playerNumber, String current, String last) {
		for(int i=0; i<4; i++){
			//if(playerNumber != i){
				if(players[i].getPosField().getPosition().equals(current) && !current.equals("")){
					Logger.log("player " + i + "'s pos is same as " + current, Logger.PROCESS);
					players[i].getPosField().setPosition(last);
					return;
				}
			//}
		}
	}
	
	
	
	
}
