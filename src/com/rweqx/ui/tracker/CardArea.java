package com.rweqx.ui.tracker;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.rweqx.CardLogic.Card;
import com.rweqx.CardLogic.ScoreCalculator;
import com.rweqx.global.MyDropListener;
import com.rweqx.global.UIMethods;
import com.rweqx.vars.Constants;

public class CardArea extends JPanel implements ComponentListener{
	Mover m;
	
	Card[] copyCards = new Card[Constants.allCards.length];
	List<Card> cards = new ArrayList<Card>();
	
	public CardArea(){		
		for(int i=0; i<Constants.allCards.length; i++){
			copyCards[i] = new Card(Constants.allCards[i]); // Deep Copy of Card
		}

		cards = new ArrayList<Card>(Arrays.asList(copyCards));
		
		createArea();
	}

	public void giveMover(Mover m) {
		this.m = m;
		MyDropListener MDL = new MyDropListener(m, this);
		DropTarget DT = new DropTarget(this, DnDConstants.ACTION_MOVE, MDL, true, null);
		
	}
	
	JPanel hearts, other;
	int width = 0;
	int height = 0;
	
	private void createArea(){
		UIMethods.setLayout(this);

		hearts = new JPanel();
		UIMethods.setLayout(hearts);
		
		other = new JPanel();
		UIMethods.setLayout(other);
		
		UIMethods.addTo(this, hearts, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, other, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		organize();
		addCardsToUI();

		hearts.setOpaque(false);
		other.setOpaque(false);
		this.addComponentListener(this);
	}
	
	private void redraw(){
		SwingUtilities.invokeLater(()->{
			this.repaint();
			this.validate();
		});
	}

	public void addCard(Card c) {
		for(int i=0; i<Constants.allCards.length; i++){
			if(Constants.allCards[i].getName().equals(c.getName())){
				cards.add(copyCards[i]);
			}
		}
		organize();
		addCardsToUI();
	}
	
	public void removeCard(Card c) {
		for(int i=0; i<cards.size(); i++){
			if(cards.get(i).getName().equals(c.getName())){
				this.remove(cards.get(i));
				cards.remove(i);
			}
		}
		//organize();
		addCardsToUI();
	}
	
	private void organize(){
		Collections.sort(cards, new Comparator<Card>(){
			@Override
			public int compare(Card c1, Card c2){
				return ScoreCalculator.compare(c1, c2);
			}
		});
		
	}
	
	private void addCardsToUI(){
		hearts.removeAll();
		other.removeAll();
		
		int w = getWidth();
		
		int perRow = w/Constants.CARD_WIDTH;
		
		int x1 = 0;
		int y1 = 0;
		int x2 = 0;
		
		for(int i=0; i<cards.size(); i++){
			if(cards.get(i).getName().startsWith("H")){
				UIMethods.addTo(hearts, cards.get(i), x1, y1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
				x1++;
				if(x1 == perRow){
					x1 = 0;
					y1++; 
				}
			}else{
				UIMethods.addTo(other, cards.get(i), x2, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
				x2++;
			}
			
		}
		redraw();
	}

	
	
	public void reset() {
		cards = new ArrayList<Card>(Arrays.asList(copyCards));
		organize();
		addCardsToUI();
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {}

	@Override
	public void componentMoved(ComponentEvent arg0) {}

	@Override
	public void componentResized(ComponentEvent arg0) {
		addCardsToUI();
		
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		addCardsToUI();
	}

	

}
