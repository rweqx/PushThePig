package com.rweqx.ui.tracker;

import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.rweqx.CardLogic.Card;
import com.rweqx.CardLogic.ScoreCalculator;
import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.ccomp.DisplayLabel;
import com.rweqx.ccomp.EmptyButton;
import com.rweqx.ccomp.MySelectableButton;
import com.rweqx.ccomp.MyTextField;
import com.rweqx.global.ImageParser;
import com.rweqx.global.MyDropListener;
import com.rweqx.global.UIMethods;
import com.rweqx.vars.Constants;

/**
 * Creates palyer with string s, and keeps score... 
 * @author RWEQX
 *
 */
public class Player extends JPanel{

	Card[] copyCards = new Card[Constants.allCards.length];
	
	List<Card> cards = new ArrayList<Card>();
	
	int matchScore = 0;
	int gameScore = 0;
	int lastScore = 0;
	
	int playerNumber;
	String playerName;
	
	PlayerArea PA;

	ArrayList<Card> TRANSFORM_CARDS;
	
	public Player(int i, Mover m, PlayerArea PA, PlayersList PL){
		this.PA = PA;
		this.PL = PL;
		
		TRANSFORM_CARDS = new ArrayList<>();
		
		MyDropListener MDL = new MyDropListener(m, this);
		DropTarget DT = new DropTarget(this, DnDConstants.ACTION_MOVE, MDL, true, null);
		
		playerNumber = i;
		playerName = "Player " + i;
		
		
		for(int j=0; j<Constants.allCards.length; j++){
			copyCards[j] = new Card(Constants.allCards[j]); // Deep Copy of Card
			if(Constants.TransCardsList.contains(Constants.allCards[j])){
				TRANSFORM_CARDS.add(copyCards[j]);
			}
		}

		
		setImage();
		createPanel();
	}
	
	private void setImage(){
		String path = PL.getIconFromName(playerName); 
		image = ImageParser.makeTranslucent(
				ImageParser.readImage(path));
	}
	
	PlayersList PL;
	Image image; //Full resolution image...
	


	
	
	MyTextField nameField;
	DisplayLabel lGameScore, lMatchScore, lLastScore;
	
	PositionField posField;
	
	
	JPanel cardHolder = new JPanel(){
		@Override
		protected void paintComponent(Graphics g){
			int w = this.getWidth();
			int h = this.getHeight();
			g.drawImage(image, 0, 0, w, h, null);
			super.paintComponent(g);
		}
	};
	
	MySelectableButton bAttempt;
	private void createPanel(){

		
		UIMethods.setLayout(this);
		UIMethods.setLayout(cardHolder);
		cardHolder.setOpaque(false);
		this.setOpaque(false);
		
		nameField = new MyTextField(playerName);
		nameField.setFont(Constants.LARGER_FONT);
		nameField.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				playerName = nameField.getText();
				setImage();
				Player.this.repaint();
				
			}
		});
		
		

		lGameScore = new DisplayLabel(Constants.LABEL_LARGE);
		lMatchScore = new DisplayLabel(Constants.LABEL_LARGE);
		lLastScore = new DisplayLabel(Constants.LABEL_LARGE);
		
		bAttempt = new MySelectableButton("Attempt", true, false);
		
		posField = new PositionField(playerNumber);
		
		updateScoreDisplay();
		
		UIMethods.addTo(this, nameField, 0, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, posField, 0, 1, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, cardHolder, 0, 2, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, bAttempt, 0, 3, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, lGameScore, 0, 4, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, lMatchScore, 0, 5, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, lLastScore, 0, 6, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		
		
	}
	
	public void addCard(Card c) {
		for(int i=0; i<Constants.allCards.length; i++){
			if(Constants.allCards[i].getName().equals(c.getName())){
				//cards.add(Constants.allCards[i]);
				cards.add(copyCards[i]);
			}
		}
		organize();
		addCardsToUI();
		updateScore();
	}
	
	public void removeCard(Card c) {
		for(int i=0; i<cards.size(); i++){
			if(cards.get(i).getName().equals(c.getName())){
				cardHolder.remove(cards.get(i));
				cards.remove(i);
				organize();
				addCardsToUI();
				updateScore();
				return;
			}
		}
		
	}
	
	public void updateScore() {
		PA.updatePigSheepTransform();
		gameScore = ScoreCalculator.updateScore((ArrayList<Card>) cards, PA.isTransform(), updatePigSheepTransform());
		
		updateScoreDisplay();
	}
	
	private void updateScoreDisplay(){
		lGameScore.setText("Game Score: " + Integer.toString(gameScore));
		lMatchScore.setText("Match Score: " + Integer.toString(matchScore));
		lLastScore.setText("Last Game Δ: " + Integer.toString(lastScore));
	}
	
	private void organize(){
		Collections.sort(cards, new Comparator<Card>(){
			@Override
			public int compare(Card c1, Card c2){
				return ScoreCalculator.compare(c1, c2);
			}
		});
		
		addCardsToUI();		
	}
	
	private void addCardsToUI(){
		cardHolder.removeAll();
		int row1 = 0;
		int row2 = 0;
		int x = 0;
		
		for(int i=0; i<cards.size(); i++){
			if(cards.get(i).getName().startsWith("H")){
				UIMethods.addTo(cardHolder, cards.get(i), row1, x, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 2, 0, 2, 0);
				row1++;
				if(row1 == 4){
					row1 = 0; 
					x++;
				}
			}else{
				UIMethods.addTo(cardHolder, cards.get(i), row2, 5, 1, 1, 1, 0, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 2, 0, 2, 0);
				row2++;
			}
			
		}
		UIMethods.addTo(cardHolder, new EmptyButton(), 0, 6, 4, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);

		redraw();
	}
	
	//TODO WILL NEED CHANGING...
	public boolean updatePigSheepTransform() {
		return cards.containsAll(TRANSFORM_CARDS);
	}
	
	private void redraw(){
		SwingUtilities.invokeLater(()->{
			this.repaint();
			this.validate();
		});
	}

	
	public void newMatch() {
		matchScore = 0;
		gameScore = 0;
		lastScore = 0;
		bAttempt.reset();
		cards.clear();
		updateScoreDisplay();
		cardHolder.removeAll();
		redraw();
	}

	public void reset() {
	//	matchScore = 0;
		gameScore = 0;
		cards.clear();
		updateScoreDisplay();
		cardHolder.removeAll();
		redraw();
	}
	
	public void newGame(){
		cards.clear();
		matchScore += gameScore;
		lastScore = gameScore;
		gameScore = 0;
		bAttempt.reset();
		cardHolder.removeAll();
		updateScoreDisplay();
		redraw();
	}
	
	/**
	 * Returns data of current game in a String array.
	 * Disected by writer.
	 * @return
	 */
	public String[] getData() {
		List<String> data = new ArrayList<String>();
		data.add(nameField.getText());
		data.add(Integer.toString(matchScore + gameScore)); //Add the latest match values to the total game score!
		data.add(Integer.toString(gameScore));
		data.add(String.valueOf(bAttempt.isSel()));
		data.add(posField.getPosition());
		data.addAll(getAllCards());
		
		return data.toArray(new String[data.size()]);
	}
	
	private List<String> getAllCards(){
		List<String> cardNames = new ArrayList<String>();
		for(int i=0; i<cards.size(); i++){
			cardNames.add(cards.get(i).getName());
		}
		return cardNames;
	}


	public void setPlayerName(String s) {
		playerName = s;
		nameField.setText(s);
		setImage();
	}

	public boolean isEmpty() {
		return cards.isEmpty();
	}

	public void setMatchScore(int i) {
		matchScore = i;
		updateScoreDisplay();
	}

	public void setLastScore(int i) {
		lastScore = i;
		updateScoreDisplay();	
	}

	public PositionField getPosField() {
		return posField;
	}

	public void loadCards(List<Card> cards) {
		this.cards = cards;
		addCardsToUI();
		
	}

	public String getPlayerName() {
		return playerName;
	}

	public List<Card> getCards() {
		return cards;
	}

	public int getMScore() {
		return matchScore;
	}
	public boolean isAttempt(){
		return bAttempt.isSel();
	}
	
	public String getPos(){
		return posField.getPosition();
	}

	public void setPos(String pos) {
		posField.setPosition(pos);
	}
	
	public void setAttempt(boolean b){
		bAttempt.setSelected(b);
	}
	
	
}
