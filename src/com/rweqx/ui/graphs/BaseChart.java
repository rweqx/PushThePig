package com.rweqx.ui.graphs;

import java.awt.GridBagConstraints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JPanel;

import com.rweqx.global.UIMethods;
import com.rweqx.vars.Constants;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.chart.Axis;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;

public class BaseChart extends JPanel implements ComponentListener{

	LineChart<String, Number> chart;

	JFXPanel FXPanel = new JFXPanel();
	Scene scene;
	
	public BaseChart(int width, int height){
		super();
		this.addComponentListener(this);
		makeChart();

		scene = new Scene(chart, width, height);
		
		
		UIMethods.setLayout(this);
		UIMethods.addTo(this, FXPanel, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
	}
	
	public void refresh(){
		FXPanel.repaint();
		FXPanel.validate();
		FXPanel.revalidate();
		
		
		
	}
	
	CategoryAxis xAxis;
	NumberAxis yAxis;
	private void makeChart() {
		xAxis = new CategoryAxis();
		yAxis = new NumberAxis();
		chart = new LineChart<String, Number>(xAxis, yAxis);
		chart.setLegendVisible(true);

		chart.setAnimated(false);
		chart.setStyle("-fx-font-size: " + Constants.CHART_FONT_SIZE + "px;");
		
	}
	
	public void addDataToNewSeries(Object[][] data, String seriesName){

		Series series = new Series<>();
		series.setName(seriesName);
		
		for(Object[] o : data){
			// skip if the player did not play any games on that day. 
			if(!o[1].equals(Constants.NO_GAMES)){
				series.getData().add(new Data(o[0], o[1]));
			}
		}
		chart.getData().add(series);
		
	}

	public void setScene(){
		FXPanel.setScene(scene);

	}
	
	public CategoryAxis getXAxis(){
		return xAxis;
	}
	public NumberAxis getYAxis(){
		return yAxis;
	}
	public LineChart<String, Number> getChart(){
		return chart;
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		refresh();
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		
	}
}

