package com.rweqx.ui.graphs;

import java.util.List;

import com.rweqx.PlayerLogic.Player;
import com.rweqx.stats.StatsManager;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;

public class WinRateChart extends BaseChart{

	List<Player> players;
	StatsManager SM;
	
	public WinRateChart(int width, int height, StatsManager SM){
		super(width, height);
		this.SM = SM;
		players = SM.getPlayers();
		
		getXAxis().setLabel("Dates");
		getYAxis().setLabel("% Games Won");
		
		getChart().setTitle("Winrate over time (Higher is better!)");
		
		for(Player p : players){
			addDataToNewSeries(SM.getDateStats().getWinRate(p), p.getName());
		}
		
		List<String> dates = SM.getAllDates();
		if(dates != null){
			getXAxis().setCategories(FXCollections.observableArrayList(dates));
		}
		setScene();
	}
	
	public void rebuild(){
		chart.getData().clear();
		players = SM.getPlayers();
		System.out.println(players.size());
		for(Player p : players){
			addDataToNewSeries(SM.getDateStats().getWinRate(p), p.getName());
		}

		getXAxis().setCategories(FXCollections.observableArrayList(SM.getAllDates()));
		
	}

}
