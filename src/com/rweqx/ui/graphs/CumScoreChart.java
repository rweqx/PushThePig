package com.rweqx.ui.graphs;

import java.util.List;

import com.rweqx.PlayerLogic.Player;
import com.rweqx.global.Logger;
import com.rweqx.stats.StatsManager;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;

public class CumScoreChart extends BaseChart{
	
	List<Player> players;
	StatsManager SM;
	
	public CumScoreChart(int width, int height, StatsManager SM){
		super(width, height);
		this.SM = SM;
		players = SM.getPlayers();
		
		makeChart();
		setScene();
	}
	
	public void makeChart(){

		players = SM.getPlayers();
		getXAxis().setLabel("Dates");
		getYAxis().setLabel("Avg Score");
		
		getChart().setTitle("Cumulative Performance Over Time");
		
		for(Player p : players){
			addDataToNewSeries(SM.getDateStats().getDailyCumulativePerformance(p), p.getName());
		}
		
		List<String> dates = SM.getAllDates();
		if(dates != null){
			getXAxis().setCategories(FXCollections.observableArrayList(dates));
		}
		setScene();
		
	}
	
	
	public void rebuild(){
		chart.getData().clear();
		players = SM.getPlayers();

		for(Player p : players){
			addDataToNewSeries(SM.getDateStats().getDailyCumulativePerformance(p), p.getName());
		}

		getXAxis().setCategories(FXCollections.observableArrayList(SM.getAllDates()));
		
	}

}
