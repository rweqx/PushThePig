package com.rweqx.ui.graphs;

import java.util.List;

import com.rweqx.PlayerLogic.Player;
import com.rweqx.stats.StatsManager;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;

public class LoseRateChart extends BaseChart{

	List<Player> players;
	StatsManager SM;
	public LoseRateChart(int width, int height, StatsManager SM){
		super(width, height);
		this.SM = SM;
		players = SM.getPlayers();
		
		getXAxis().setLabel("Dates");
		getYAxis().setLabel("% Games Lost");
		
		getChart().setTitle("LoseRate over Time (Lower is better!)");
		
		for(Player p : players){
			addDataToNewSeries(SM.getDateStats().getLoseRate(p), p.getName());
		}
		
		List<String> dates = SM.getAllDates();
		if(dates != null){
			getXAxis().setCategories(FXCollections.observableArrayList(dates));
		}
		setScene();
		
		
	}
	
	
	public void rebuild(){
		//super.rebuild();

		chart.getData().clear();
		players = SM.getPlayers();
		for(Player p : players){
			addDataToNewSeries(SM.getDateStats().getLoseRate(p), p.getName());
		}

		getXAxis().setCategories(FXCollections.observableArrayList(SM.getAllDates()));
	}

	
}
