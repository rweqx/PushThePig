package com.rweqx.ccomp;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

import com.rweqx.global.ImageParser;
import com.rweqx.vars.Constants;

/**
 * Profile Button used in Stats... 
 * @author RWEQX
 *
 */
public class ProfileButton extends JButton{

	String name;
	String path;
	int borderWidth = 1;
	
	//Holds full resolution of image... to be used for painting. 
	Image IconFullRes;
	
	public ProfileButton(String name, String path){
		super();
		int size = Constants.ICON_SIZE;
		
		Thread runLater = new Thread(()->{
			if(!path.equals(Constants.NO_IMAGE_ICON)){
				IconFullRes = ImageParser.makeTranslucent(ImageParser.readImage(path));
			}else{
				IconFullRes = ImageParser.makeTranslucent(ImageParser.makeBlankImage(size, size));
			}
			});
		runLater.start();
		
		this.name = name;
		this.path = path;
		this.setContentAreaFilled(false);
		this.setBorder(new EmptyBorder(borderWidth, borderWidth, borderWidth, borderWidth));
		
		this.setText(name);
		
		setHorizontalTextPosition(JButton.CENTER);
		setVerticalTextPosition(JButton.CENTER);
		setFont(Constants.PLAYERS_LIST_FONT);
	}

	public String getPlayerName() {
		return name;
	}
	
	@Override
	protected void paintComponent(Graphics g){
		int w = this.getWidth();
		int h = this.getHeight();
		g.drawImage(IconFullRes, 0, 0, w, h, null);
		super.paintComponent(g);
	}

	public void setDims(int w){
		//if(this.getSize().width != this.getSize().height){
			Dimension d = new Dimension(w, w);
			this.setSize(d);
			this.setPreferredSize(d);
			this.repaint();
			this.validate();
		//}
	}
	
	
	
}
