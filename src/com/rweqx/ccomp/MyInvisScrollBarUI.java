package com.rweqx.ccomp;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ScrollBarUI;
import javax.swing.plaf.basic.BasicScrollBarUI;

import com.rweqx.vars.Constants;

/**
 * Scrollbar with no buttons up adn down, clean scrolling bar.. Much more modern overal...
 * @author RWEQX
 *
 */
public class MyInvisScrollBarUI extends BasicScrollBarUI {
	
	public MyInvisScrollBarUI(){
		
		//track = (BufferedImage) ImageParser.getImage(Constants.SCROLLBAR_UI_PATH + "/track.jpg", 0, 0);
		//thumb = (BufferedImage) ImageParser.getImage(Constants.SCROLLBAR_UI_PATH + "/white.png", 0, 0);
	}
	BufferedImage track, thumb;
	
	@Override
	protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds){
		//Image r = track.getScaledInstance(trackBounds.width, trackBounds.height, Image.SCALE_SMOOTH);
		int x = (int) Math.round(trackBounds.getX());
		int y = (int) Math.round(trackBounds.getY());
		int w = (int) Math.round(trackBounds.getWidth());
		int h = (int) Math.round(trackBounds.getHeight());
		
		g.setColor(Constants.SCROLLBAR_TRACK_COLOR);
		g.fillRect(x, y, w, h);
		
	}
	
	@Override
	public Dimension getPreferredSize(JComponent c){
		Dimension dim = new Dimension(Constants.SCROLLBAR_WIDTH, c.getHeight());
		return dim;
	}
	
	@Override
	protected Rectangle getTrackBounds(){
		Rectangle rect = super.getTrackBounds();
		
		int x = (int) Math.round(rect.getX());
		int y = (int) Math.round(rect.getY());
		int w = Constants.SCROLLBAR_WIDTH;
		int h = (int) Math.round(rect.getHeight());
		Rectangle rect2 = new Rectangle(x, y, w, h); 
		return rect2;
		
	}
	
	@Override
	protected Rectangle getThumbBounds(){
		Rectangle rect = super.getThumbBounds();
		
		int x = (int) Math.round(rect.getX());
		int y = (int) Math.round(rect.getY());
		int w = Constants.SCROLLBAR_WIDTH;
		int h = (int) Math.round(rect.getHeight());
		Rectangle rect2 = new Rectangle(x, y, w, h); 
		return rect2;
	}
	
	
	@Override
	protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds){
		//Image r = thumb.getScaledInstance(thumbBounds.width, thumbBounds.height, Image.SCALE_SMOOTH);
		int x = (int) Math.round(thumbBounds.getX());
		int y = (int) Math.round(thumbBounds.getY());
		int w = (int) Math.round(thumbBounds.getWidth());
		int h = (int) Math.round(thumbBounds.getHeight());
		
		g.setColor(Constants.SCROLLBAR_THUMB_COLOR);
		g.fillRect(x, y, w, h);
	}
	
	int A = 0;
	Dimension d = new Dimension(A, A);
	@Override
	protected JButton createIncreaseButton(int orientation){
		//Image r = thumb.getScaledInstance(A, A, Image.SCALE_SMOOTH);
		JButton b = new EmptyButton();
		b.setSize(d);
		b.setPreferredSize(d);
		b.setMaximumSize(d);
		return b;
	}
	
	@Override
	protected JButton createDecreaseButton(int orientation){
		//Image r = thumb.getScaledInstance(A, A, Image.SCALE_SMOOTH);
		JButton b = new EmptyButton();
		b.setSize(d);
		b.setPreferredSize(d);
		b.setMaximumSize(d);
		return b;
		
	}
}
