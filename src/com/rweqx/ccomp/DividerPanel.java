package com.rweqx.ccomp;

import java.awt.Graphics;

import javax.swing.JPanel;

import com.rweqx.vars.Constants;

public class DividerPanel extends JPanel{
	public DividerPanel(){
		super();
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Constants.DIVIDER_COLOUR);
		int x1 = Constants.DIVIDER_SPACING;
		int y1 = this.getHeight() / 2;
		int x2 = this.getWidth() - x1;
		g.drawLine(x1, y1, x2, y1);
	}
}
