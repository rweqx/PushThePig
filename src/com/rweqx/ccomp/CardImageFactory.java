package com.rweqx.ccomp;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.rweqx.global.ImageParser;
import com.rweqx.global.Logger;
import com.rweqx.vars.Constants;

public class CardImageFactory {

	public enum Suit{
		HEARTS,
		CLUBS,
		DIAMONDS,
		SPADES
	}
	
	/*
	public static final String HEARTS = "heart";
	public static final String CLUBS = "club";
	public static final String DIAMONDS = "diamond";
	public static final String SPADES = "spades";
	*/
	
	Image iClubs, iHearts, iSpades, iDiamonds;
	
	
	public CardImageFactory(){
	
		iClubs = readImage(Constants.CLUBS_FILE);
		iDiamonds = readImage(Constants.DIAMONDS_FILE);
		iHearts = readImage(Constants.HEARTS_FILE);
		iSpades = readImage(Constants.SPADES_FILE);
		
	}
	
	public Image getSuitImage(String suitShorthand){
		Image i;
		i = getSuitFile(translate(suitShorthand));
		return i;
	}

	private Image getSuitFile(Suit suit){
		if(suit == Suit.HEARTS){
			return iHearts;
		}else if(suit == Suit.DIAMONDS){
			return iDiamonds;
		}else if(suit == Suit.SPADES){
			return iSpades;
		}else if(suit == Suit.CLUBS){
			return iClubs;
		}
		return null;
	}
	
	private Suit translate(String s){
		if(s.equals("H")){
			return Suit.HEARTS;
		}else if(s.equals("C")){
			return Suit.CLUBS;
		}else if(s.equals("D")){
			return Suit.DIAMONDS;
		}else if(s.equals("S")){
			return Suit.SPADES;
		}
		Logger.log("Cannot find suit (technically impossible)", Logger.BAD);
		return null;
	}

	public Image readImage(String dir){
		try{
			Image i = ImageIO.read(getClass().getResourceAsStream(dir));
			i = i.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
			return i;
		}catch(Exception e){
			Logger.log("Could not find image " + dir, Logger.EXTREME);
		}
		return ImageParser.makeBlankImage(50, 50);
		
	}
	
}
