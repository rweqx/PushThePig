package com.rweqx.ccomp;

import javax.swing.JLabel;

import com.rweqx.vars.Constants;

public class DisplayLabel extends JLabel{

	public DisplayLabel(String size){
		super();
		if(size.equals(Constants.LABEL_LARGE)){
			this.setFont(Constants.LARGER_FONT);
		}else if(size.equals(Constants.LABEL_MED)){
			this.setFont(Constants.NORMAL_FONT);
		}else if(size.equals(Constants.LABEL_EXTRA_LARGE)){
			this.setFont(Constants.XL_FONT);
		}
	}
}
