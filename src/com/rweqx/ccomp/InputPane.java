package com.rweqx.ccomp;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.rweqx.global.UIMethods;
import com.rweqx.vars.Constants;

public class InputPane extends JPanel{
	JTextField text;
	JLabel note;
	
	public InputPane(String string){
		
		text = new JTextField("");
		note = new JLabel();
		note.setFont(Constants.LARGER_FONT);
		note.setText(string);
		
		UIMethods.setLayout(this);
		UIMethods.addTo(this, note, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		UIMethods.addTo(this, text, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
	}
	
	public String getInput(){
		String s = text.getText().trim();
		if(s.equals("")){
			return Constants.NO_RESPONSE;
		}
		return s;
	}

}
