package com.rweqx.ccomp;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JTextField;

import com.rweqx.vars.Constants;

public class MyTextField extends JTextField
						implements ActionListener, MouseListener, FocusListener{

	boolean selected = false;
	String saveText = ""; //Holds previous text. 
	
	public MyTextField(){
		super();
		setup();
	}
	public MyTextField(String s) {
		super(s);
		setup();
	}
	
	private void setup(){
		this.addActionListener(this);
		this.addMouseListener(this);
		this.addFocusListener(this);
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(Constants.DARK_DEFAULT),
				BorderFactory.createEmptyBorder(0, 5, 0, 5)));
		
		update();
	}
	
	private void update(){
		if(selected){
			this.setEditable(true);
			this.setOpaque(true);
			this.setBackground(Constants.EDITING_BACKGROUND_COLOUR);
		}else{
			this.setOpaque(false);
			this.setBackground(Constants.DEFAULT_BACKGROUND_COLOUR);
			this.setEditable(false);
		}
	}
	
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		select();
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {}
	@Override
	public void mouseExited(MouseEvent arg0) {}
	@Override
	public void mousePressed(MouseEvent arg0) {
		select();
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		notSelect();
	}
	@Override
	public void focusGained(FocusEvent arg0) {
		//select();
	}
	
	@Override
	public void focusLost(FocusEvent arg0) {
		//Treat same as clicking enter. 
		this.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
	}
	
	
	private void select(){
		if(!selected){
			selected = true;
			saveText = this.getText();
			//this.setText("");
			this.selectAll();
			update();
		}
	}
	private void notSelect(){
		if(selected){
			selected = false;
			if(this.getText().equalsIgnoreCase("")){
				this.setText(saveText);
			}
			saveText = "";
			update();
		}
	}

}
