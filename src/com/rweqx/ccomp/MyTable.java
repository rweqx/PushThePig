package com.rweqx.ccomp;

import javax.swing.JTable;

import com.rweqx.vars.Constants;

public class MyTable extends JTable{
	public MyTable(Object[][] data, String[] titles){
		super(data, titles);
		setFont(Constants.STATS_FONT);
		getTableHeader().setFont(Constants.STATS_FONT_SMALL_BOLD);
		setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		setRowHeight(Constants.STATS_FONT.getSize() + 6);
		
	}
	
	@Override 
	public boolean getScrollableTracksViewportWidth(){
		return(getPreferredSize().width < getParent().getWidth());
	}
}
