package com.rweqx.ccomp;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

/**
 * Scrollpane that autoresizes the interior such that the width is always matching the scrollpane's width.
 * @author RWEQX
 *
 */
public class MyScrollPane extends JScrollPane{

	public MyScrollPane(boolean horizontal, boolean vertical){

		super();
		setup(horizontal, vertical);
	}
	
	public MyScrollPane(){
		super();
		setup(false, true);
	}
	
	
	private void setup(boolean horizontal, boolean vertical){
		if(vertical){
			setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			getVerticalScrollBar().setUnitIncrement(15);
		}else{
			setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		}
		
		if(horizontal){
			setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		}else{
			setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		}

		this.getVerticalScrollBar().setUI(new MyInvisScrollBarUI());
		this.getHorizontalScrollBar().setUI(new MyInvisScrollBarUI());
	}
	
	
	
}
