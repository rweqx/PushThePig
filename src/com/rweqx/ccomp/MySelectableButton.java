package com.rweqx.ccomp;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.rweqx.vars.Constants;

public class MySelectableButton extends JButton implements ActionListener{
	
	public MySelectableButton(String name, boolean selectable, boolean selected){
		super(name);
		this.name = name;
		this.selected = selected;
		this.selectable = selectable;
		this.setContentAreaFilled(false);
		
	
		this.addActionListener(this);
		
	}

	boolean selected; 
	boolean selectable;
	String name;
	
	@Override
	protected void paintComponent(Graphics g){
		if(selected){
			g.setColor(Constants.CLICK_SELECTED_COLOUR);
		}else{
			g.setColor(Constants.CLICK_UNSELECTED_COLOUR);
		}
		
        g.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g);
	}
	
	public String getPlayerName() {
		return name;
	}
	
	public void setSelectable(boolean b){
		selectable = b;
		selected = false;
		this.setFocusPainted(b);
		this.setFocusable(b);
		this.repaint();
	}

	public void setSelected(boolean b){
		selected = b;
		this.repaint();
	}
	
	public boolean isSel(){
		return selected;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(selectable){
			selected = !selected;
		}
		
	}
	
	public void reset() {
		setSelected(false);
	}
	

	
	
	
}
