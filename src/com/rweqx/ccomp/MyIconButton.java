package com.rweqx.ccomp;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import com.rweqx.global.ImageParser;
import com.rweqx.vars.Constants;

public class MyIconButton extends JButton{
	public MyIconButton(String path){
		super();
		
		// Set icon in seperate thread...
		Thread runLater = new Thread(()->{
			updateIcon(path);
		});
		runLater.start();
		
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);
		
	}

	public void updateIcon(String path) {
		ImageIcon i;
		if(!path.equals(Constants.NO_IMAGE_ICON)){
			i = new ImageIcon(ImageParser.readImage(path, Constants.ICON_SIZE));
		}else{
			i = new ImageIcon(ImageParser.makeBlankImage(Constants.ICON_SIZE, Constants.ICON_SIZE));
		}
		
		this.setIcon(i);
		
	}
	
}
