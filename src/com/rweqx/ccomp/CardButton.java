package com.rweqx.ccomp;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import com.rweqx.vars.Constants;

public class CardButton extends JButton{

	public CardButton(){
		super();
		format();
	}
	
	private void format(){
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, true));
		this.setPreferredSize(new Dimension(Constants.CARD_WIDTH, Constants.CARD_HEIGHT));
		
		this.setFont(Constants.CARD_FONT);
		this.setIconTextGap(10);
		this.setHorizontalAlignment(SwingConstants.CENTER);
		this.setHorizontalTextPosition(SwingConstants.CENTER);
	    this.setVerticalTextPosition(SwingConstants.BOTTOM);
	    this.setContentAreaFilled(false);
		this.setFocusPainted(false);
		this.setFocusable(false);
	}
	
	
	//Paints backgroudn to always be tthe card background!
	@Override
    protected void paintComponent(Graphics g) {
        g.setColor(Constants.CARD_BACKGROUND_COLOUR);
        g.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g);
    }
	
}
