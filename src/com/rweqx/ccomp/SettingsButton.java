package com.rweqx.ccomp;

import javax.swing.Icon;
import javax.swing.JButton;

import com.rweqx.vars.Constants;

public class SettingsButton extends JButton{
	
	public enum Size{
		SMALL, MEDIUM, LARGE;
	}
	
	Size size;
	
	public SettingsButton(Size size){
		super();
		this.size = size;
		setLook();
	}
	
	public SettingsButton(Size size, String s){
		super(s);
		this.size = size;
		setLook();
	}
	
	private void setLook(){
		this.setContentAreaFilled(false);
		switch(size){
			case SMALL:
				this.setFont(Constants.NORMAL_FONT);
				break;
			case MEDIUM:
				this.setFont(Constants.LARGER_FONT);
				break;
			case LARGE:
				this.setFont(Constants.XL_FONT);
				break;
		}
		
		
		this.setBorderPainted(false);
	}
}
