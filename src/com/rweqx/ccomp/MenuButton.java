package com.rweqx.ccomp;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import com.rweqx.vars.Constants;

public class MenuButton extends JButton 
							implements MouseListener{

	int ID;
	boolean switchablePanel;
	Color currentBGColour;
	final int SPACER = Constants.INDENT;
	boolean bMajorButton = false;
	
	public MenuButton(int ID, String displayName, boolean switchablePanel) {
		super(displayName);
		this.ID = ID;
		this.switchablePanel = switchablePanel;	
		if(ID <= 10){
			bMajorButton = true;
		}
		setupLook();
	}

	
	private void setupLook(){
		this.setBackground(Constants.MENU_UNCLICKED_COLOUR);
		
		if(bMajorButton){
			this.setFont(Constants.MENU_BUTTON_FONT);
			drawBorder(false);
		}else{
			this.setFont(Constants.SMALL_MENU_BUTTON_FONT);
			this.setBorderPainted(false);
			setMenuDeselected();
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createMatteBorder(1, 0, 1, 0, Constants.BUTTON_BORDER_COLOUR),
					BorderFactory.createEmptyBorder(0, (int)(SPACER*2.5), 0, 0)));
		}
		
		this.setHorizontalAlignment(SwingConstants.LEFT);
	//	this.setHorizontalTextPosition(SwingConstants.LEFT);
		
		this.setFocusable(false);
		this.setFocusPainted(false);
		
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {
		this.setBackground(Constants.MENU_CLICKED_COLOUR);
		this.setSelected(false);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		this.setBackground(currentBGColour);
	}

	public int getID() {
		return ID;
	}

	
	public void setMenuSelected() {
		if(bMajorButton){
			this.setBackground(Constants.MENU_SELECTED_COLOUR);
			currentBGColour = Constants.MENU_SELECTED_COLOUR;
			drawBorder(false);
		}else{
			this.setBackground(Constants.MENU_SMALL_SELECTED_COLOUR);
			currentBGColour = Constants.MENU_SMALL_SELECTED_COLOUR;
		}
	}
	
	public void setMenuDeselected(){
		if(bMajorButton){
			this.setBackground(Constants.MENU_UNCLICKED_COLOUR);
			currentBGColour = Constants.MENU_UNCLICKED_COLOUR;
		
			drawBorder(true);
		}else{
			this.setBackground(Constants.MENU_SELECTED_COLOUR);
			currentBGColour = Constants.MENU_SELECTED_COLOUR;
		}
	}
	
	
	private void drawBorder(boolean b){
		if(b){
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createMatteBorder(1, 0, 1, 0, Constants.BUTTON_BORDER_COLOUR),
					BorderFactory.createEmptyBorder(0, SPACER, 0, 0)));
			
		}else{
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createMatteBorder(1, 0, 0, 0, Constants.BUTTON_BORDER_COLOUR),
					BorderFactory.createEmptyBorder(0, SPACER, 1, 0)));
		}
	}

	public boolean isSwitchablePanel() {
		return switchablePanel;
	}
}
