package com.rweqx.ccomp;

import javax.swing.JButton;

import com.rweqx.vars.Constants;

/**
 * Default Button with the look and feel for the program.
 * @author RWEQX
 *
 */
public class MyButton extends JButton{
	

	public MyButton(){
		super();
		setupLook();
	}
	public MyButton(String s){
		super(s);
		setupLook();
	}
	
	private void setupLook(){
		this.setBackground(Constants.DEFAULT_BACKGROUND_COLOUR);
		
	}
	
}
