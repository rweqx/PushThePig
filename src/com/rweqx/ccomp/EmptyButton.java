package com.rweqx.ccomp;

import javax.swing.JButton;

import com.rweqx.vars.Constants;

public class EmptyButton extends JButton{

	public EmptyButton(){
		super();
		
		this.setEnabled(false);
		this.setFocusable(false);
		this.setBorderPainted(false);
		this.setBackground(Constants.MENU_UNCLICKED_COLOUR);
		this.setOpaque(false);
	}
}
