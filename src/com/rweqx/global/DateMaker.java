package com.rweqx.global;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateMaker {
	static Date d;
	
	public static String getDateYYYYMMDD(){
		SimpleDateFormat SDF = new SimpleDateFormat("yyyy/MM/dd");
		d = new Date();
		return SDF.format(d);
	}
	
	public static String getDateHHMMSS(){
		SimpleDateFormat SDF = new SimpleDateFormat("hh:mm:ss:SSS");
		d = new Date();
		return SDF.format(d);
	}
	
	public static long currentExactTime() {
		return System.currentTimeMillis();
	}

	public static int convertDateStringToInt(String s){
		String split[] = s.split("/");
		if(split.length == 3){
			int i = Integer.parseInt(split[0]) * 10000;
			int j = Integer.parseInt(split[1]) * 100;
			int k = Integer.parseInt(split[2]);

			return i + j + k;
		}else{
			return -1;
		}
	}
	
	public static String convertIntToDateString(int i) {
		String s = String.valueOf(i);
		s = s.substring(0, 4) + "/" + s.substring(4, 6) + "/" + s.substring(6);
		return s;
	}

	private static int[] getSplitDate(String s){
		int i[] = new int[3];
		i[0] = Integer.parseInt(s.substring(0, 4));
		i[1] = Integer.parseInt(s.substring(5, 7));
		i[2] = Integer.parseInt(s.substring(8, 10));
		return i;
	}
	
	public static List<String> getAllDates(String start, String end) {
		System.out.println("Getting all dates from " + start + " to " + end);
		List<String> dates = new ArrayList<String>();
		
		int[] startDate = getSplitDate(start);
		int[] endDate = getSplitDate(end);
		for(int year = startDate[0]; year<=endDate[0]; year++){
			int startMonth = 1; 
			int endMonth = 12;
			if(year == startDate[0]){
				startMonth = startDate[1];
			}
			if(year == endDate[0]){
				endMonth = endDate[1];
			}
			
			for(int month = startMonth; month <= endMonth; month++){
				int startDay = 1;
				int endDay =  31;
				if(year == startDate[0] && month == startDate[1]){
					startDay = startDate[2];
				}
				if(year == endDate[0] && month == endDate[1]){
					endDay = endDate[2];
				}else if(month == 4 || month == 6 || month == 9 || month == 11){
					endDay = 30;
				}else if(month == 2){
					endDay = 29;
				}
				
				for(int day = startDay; day <= endDay; day++){
					int i = year * 10000;
					int j = month * 100;
					int k = day;
					String date = convertIntToDateString(i + j + k);
					dates.add(date);
				}
			}
		}
		Logger.log("Dates found: " + dates.size(), Logger.PROCESS);
		
		return dates;
	}
}
