package com.rweqx.global;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import com.rweqx.ccomp.MyButton;
import com.rweqx.ccomp.MyScrollPane;
import com.rweqx.main.Main;
import com.rweqx.vars.Constants;

public class Updater {
	Main main;

	String CURRENT_VERSION = Constants.CURRENT_VERSION;
	String URL = Constants.UPDATE_URL;
	
	String VERSION_TAG = Constants.VERSION_TAG;
	String CHANGELOG_TAG = Constants.CHANGELOG_TAG;
	String DOWNLOAD_TAG = Constants.DOWNLOAD_TAG;
	
	
	String updateVersion = null;
	String updateData = null;
	String updateFileURL = null;
	
	
	//Default location to save file to temporarily... 
	String EXPORT_FILE = Constants.ROOT_DIR + "Files/update.jar";
	

	
	JTextArea textA;
	MyButton update, cancel, launch;
	
	JPanel panel;
	JFrame frame;
	MyScrollPane MSP;
	

	public Updater(Main main){
		this.main = main;
	}
	
	
	private String updateText(boolean updateFound, String onlineVersion, String changelog){
		String s = "";
		if(updateFound){
			s += "New Update Found: Version " + onlineVersion + "\n";
			s += "    (Current - " + CURRENT_VERSION + ")" + "\n";
			s += "Changlog:\n" + changelog + "\n";
			s += "Click Update to Update to latest version or cancel to start program normally" + "\n";
		}else{
			s += "Update Not Found"
					+ "\nCurrent Version - " + CURRENT_VERSION
					+ "\nOnline Version - " + onlineVersion;
		}
		return s;
	}
	
	
	public boolean isUpdateAvailable(){
		updateVersion = "NOT FOUND";
		try{
			String parse = readURL(URL);
			parse = parse.substring(
					parse.indexOf("<body"));
			updateVersion = (parseLatestVersion(parse));
			System.out.println(updateVersion);
			
			int[] updateVer = getIntArrayFromVersion(updateVersion);
			int[] curVer = getIntArrayFromVersion(CURRENT_VERSION);

			for(int i=0; i<curVer.length; i++){
				if(updateVer[i] > curVer[i]){
					
					updateData = parseChangeData(parse);
					updateFileURL = parseDownloadLocation(parse);
					//Logger.log(updateText(true, updateVersion, updateData), Logger.PROCESS);
					return true;
				}else if(updateVer[i] < curVer[i]){
					Logger.log(updateText(false, updateVersion, null), Logger.PROCESS);
					return false;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		Logger.log(updateText(false, updateVersion, null), Logger.PROCESS);
		return false;
	}
	
	private String parseDownloadLocation(String s){
		s = s.substring(
				s.indexOf("[" + DOWNLOAD_TAG + "]") + DOWNLOAD_TAG.length() + 2,
				s.indexOf("[/" + DOWNLOAD_TAG + "]"));
		return s;
	}
	private String parseChangeData(String s) {
		s = s.substring(
				s.indexOf("[" + CHANGELOG_TAG + "]") + CHANGELOG_TAG.length() + 2,
				s.indexOf("[/" + CHANGELOG_TAG + "]"));
		s = s.replaceAll("<br />", "\n");
		s = s.replaceAll("&lt;t&gt;", "    ");
		return s;
	}
	private String parseLatestVersion(String readURL) {
		readURL = readURL.substring(
				readURL.indexOf("[" + VERSION_TAG + "]") + VERSION_TAG.length() + 2,
				readURL.indexOf("[/" + VERSION_TAG + "]"));
		return readURL;
	}
	private int[] getIntArrayFromVersion(String version){
		String s[] = version.split("\\.");
		int i[] = new int[s.length];
		for(int x=0; x<s.length; x++){
			i[x] = Integer.parseInt(s[x]);
		}
		return i;
	}
	private String readURL(String address) throws Exception{
		URL url = new URL(address);
		InputStream input = url.openStream();
		
		int c = 0;
		StringBuffer buffer = new StringBuffer("");
		while((c = input.read()) != -1){
			buffer.append((char) c);
			
		}
		input.close();
		return buffer.toString();
	}

	
	
	public class RunUpdate implements ActionListener{
		Thread updater;
		String JAR_FILE_PATH = null;
		String JAR_NAME = "Push The Pig.jar";
		
		public RunUpdate(){
			showUpdateUI();
			updater = new Thread(new Runnable(){
				@Override
				public void run(){
					try{
						downloadFile(updateFileURL);
						findAndSetNameOfCurrentInstance(); 
						replace();
						clean();
						showLaunchButton();
					}catch(MalformedURLException e){
						error(e);
					}catch(IOException e){
						error(e);
					}catch(Exception e){
						error(e);	
					}
				}
			});
		}
		
		private void error(Exception e){
			e.printStackTrace();
			Logger.log("Update Failed - ERROR:" + e.getMessage(), Logger.BAD);
			appendText("An Error Occured!");
			launch(false); //Launch without restarting since update failed... 
		}
		
		/**
		 * 	If running in jar file, finds hte name of the jar file and changes it... 
		 * 	If running in IDE, then retains the defualt JARNAME.
		 */
		private void findAndSetNameOfCurrentInstance() throws URISyntaxException, UnsupportedEncodingException{
			String path = Main.class.getResource(Main.class.getSimpleName() + ".class").getPath();
			if(path.startsWith("/")) {
		    //    System.out.println("This is not a jar file: \n" + path);
				System.out.println("Not in a jar file");
		    }else{
		    	path = ClassLoader.getSystemClassLoader().getResource(path).getFile();
			//	Logger.log(path, Logger.PROCESS);
		    	System.out.println("In a jar file");
		    	path = path.substring(6, path.lastIndexOf('!'));

		    	path = URLDecoder.decode(path, "UTF-8");
			//	Logger.log(path, Logger.PROCESS);
		    	JAR_FILE_PATH = path;	
		    }
			
			Logger.log(path, Logger.PROCESS);
			
		}
		
		/**
		 * Launches the program either restarting it or just showing Window w in main. 
		 * @param restart
		 */
		private void launch(boolean restart){
	
			if(restart){
				String[] run = {
					"java", 
					"-jar", 
					("\"" + JAR_NAME +"\""),
					String.valueOf(true)
				};
				System.out.println("\"" + JAR_NAME +"\"");
				try{
					Runtime.getRuntime().exec(run);
				}catch(Exception e){
					e.printStackTrace();
				}

				System.exit(0);
			}else{
				main.show();
			}
		}
		
		/**
		 * Deletes temp downloaded file
		 */
		private void clean(){
			appendText("Cleaning up Downloads");
			File f = new File(EXPORT_FILE);
			f.setWritable(true);
			f.delete();
			appendText("Temp Download Files deleted");
		}
		
		private void replace() throws Exception{
			appendText("Copying Files");
			File updateFile = new File(EXPORT_FILE);
			
			String path = new File("").getAbsolutePath();
			
			File to;
			if(JAR_FILE_PATH != null){
				to = new File(JAR_FILE_PATH);
				JAR_NAME = JAR_FILE_PATH.substring(JAR_FILE_PATH.lastIndexOf("/")+1);
				to.setWritable(true);
			}else{
				//Use default... 
				to = new File(path + "/" + JAR_NAME);
		
			}
			
			InputStream in = new FileInputStream(updateFile);
			OutputStream out = new FileOutputStream(to);
			
			byte[] buf = new byte[1024];
			int len;
			while((len = in.read(buf))>0){
				out.write(buf,0,len);
			}
			in.close();
			out.close();
			
			appendText("Copied Files Successfully");
		}
		
		private void downloadFile(String s) throws Exception{
			appendText("Downloading File.");
			long start = DateMaker.currentExactTime();
			URL url = new URL(s);
			URLConnection conn = url.openConnection();
			InputStream in = conn.getInputStream();
			
			long size = conn.getContentLength();

			appendText("File Size = " + Maths.decimalCalc(size, 1024*1024) + "mb");
			
			File f = new File(EXPORT_FILE);
			
			if(!f.exists()){
				f.getParentFile().mkdirs();
				f.createNewFile();
				f.setWritable(true);
			}
			
			BufferedOutputStream fOut = new BufferedOutputStream(new FileOutputStream(f));
			byte[] buffer = new byte[32*1024];
			int read = 0;
			int total = 0;
			
			while((read = in.read(buffer)) != -1){
				total += read;
				fOut.write(buffer, 0, read);
			}
			appendText("Actually downloaded - " + Maths.decimalCalc(size, 1024*1024) + "mb");
			
			fOut.flush();
			fOut.close();
			in.close();
			
			long end = DateMaker.currentExactTime();
			appendText("Download Complete. Time Taken -" + (end-start) + "ms");
			
		}
		
		private void showLaunchButton(){
			panel.remove(cancel);
			panel.remove(update);

			UIMethods.addTo(panel, launch, 0, 1, 2, 1, 1, 0.1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
			panel.validate();
		}

		private void showUpdateUI(){
			textA = new JTextArea();
			textA.setEditable(false);
			textA.setWrapStyleWord(true);
			textA.setLineWrap(true);
			
			
			MSP = new MyScrollPane(false, true);
			MSP.setViewportView(textA);
			
			panel = new JPanel();
			UIMethods.setLayout(panel);
			UIMethods.addTo(panel, MSP, 0, 0, 2, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
			
			update = new MyButton("Update");
			cancel = new MyButton("Cancel");
			launch = new MyButton("Launch");
			
			UIMethods.addTo(panel, update, 0, 1, 1, 1, 1, 0.1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
			UIMethods.addTo(panel, cancel, 1, 1, 1, 1, 1, 0.1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, 0, 0, 0, 0);
			
			launch.addActionListener(this);
			update.addActionListener(this);
			cancel.addActionListener(this);
			frame = new JFrame("Updater");
			
			frame.setContentPane(panel);
			frame.setSize(500, 500);
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	        frame.setLocation(
	        		dim.width/2 - frame.getSize().width/2, 
	        		dim.height/2 - frame.getSize().height/2);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			appendText(updateText(true, updateVersion, updateData));
			
			
			frame.setVisible(true);
			
		}
		
		/*
		 * Allows autoscrolling of added text to textA
		 */
		
		private void appendText(String message){
			
			Logger.log(message, Logger.PROCESS);
			
			SwingUtilities.invokeLater(new Runnable(){
				public void run(){
					textA.append("\n" + (message));
					int i = MSP.getVerticalScrollBar().getMaximum();
					MSP.getVerticalScrollBar().setValue(i);
					MSP.validate();
					panel.validate();
				}
			});
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			Object source = arg0.getSource();
			if(source == update){
				updater.run();
			}else if(source == cancel){
				//frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				frame.setVisible(false);
				frame.dispose();
				launch(false);
			}else if(source == launch){
				launch(true);
			}
			
		}
	}
	
	public void runUpdate() {
		RunUpdate RU = new RunUpdate();
	}
}
