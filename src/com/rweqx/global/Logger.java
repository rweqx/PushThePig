package com.rweqx.global;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import com.rweqx.ccomp.MyScrollPane;
import com.rweqx.settings.Settings;
import com.rweqx.vars.Constants;

public class Logger {

	
	public final static String EXTREME = "Extreme";
	public final static String PROCESS = "Process";
	public static final String WARNING = "Warning";
	public static final String BAD = "Bad";
	
	static boolean bLoggerOn = false;
	
	public static void showLogger(){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				logFrame.setVisible(true);
			}
		});
	}
	
	public static void log(String s, String severity){
		String date = DateMaker.getDateHHMMSS();
		
		String message = "[" + date + "]{" +  severity + "} " +  s;
		
		
		System.out.println(message);
		
		if(bLoggerOn){		
			SwingUtilities.invokeLater(new Runnable(){;
				public void run(){
					text.setText(text.getText() + "\n" + (message));
					int i = scroll.getVerticalScrollBar().getMaximum();
					scroll.getVerticalScrollBar().setValue(i);
				}
			});
		}
	}
	
	static JFrame logFrame;
	static MyScrollPane scroll;
	static JTextArea text; 
	
	Settings settings;
	
	
	public Logger(Settings settings){
		this.settings = settings;
		
		bLoggerOn = true;
		text = new JTextArea("");
		text.setEditable(false);
		text.setBackground(Constants.LIGHT_DEFAULT);
		text.setFont(Constants.LOGGER_FONT);
		text.setWrapStyleWord(true);
		text.setLineWrap(true);
		scroll = new MyScrollPane();
		scroll.setViewportView(text);
		
		logFrame = new JFrame("Log Window");
		logFrame.setSize(Constants.LOG_SIZE);
		logFrame.setContentPane(scroll);
		
		logFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		if(settings.getProperties().getShowLoggerOnStart()){
			logFrame.setVisible(true);
		}
	}
	
}
