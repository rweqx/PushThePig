package com.rweqx.global;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.rweqx.vars.Constants;
import com.rweqx.vars.Globals;

public class ImageParser {
	public static Image readImage(String path){
		if(!path.equals(Constants.NO_IMAGE_ICON)){
			try{
				File f = new File(path);
				Image i = ImageIO.read(f);
				return i;
			}catch(Exception e){
				System.out.println(path);
				e.printStackTrace();
			}
		}
		return makeBlankImage(0, 0);
	}
	
	public static Image readImage(String path, int side){
		return readImage(path, side, side);
	}
	
	public static Image readImage(String path, int w, int h){
		if(!path.equals(Constants.NO_IMAGE_ICON)){
			try{
				Image i = readImage(path);
				i = resize(i, w, h);
				return i;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return makeBlankImage(0, 0);
	}
	
	public static Image resize(Image i, int w, int h) {
		if(w <= 0 || h <= 0){
			w = Constants.ICON_SIZE;
			h = Constants.ICON_SIZE;
		}
		try{
			Image resize = i.getScaledInstance(w, h, BufferedImage.SCALE_SMOOTH);	
			return resize;
		}catch(Exception e){
			e.printStackTrace();
		}
		return makeBlankImage(w, h);
	}
	
	public static Image makeTranslucent(Image i){
		
		int w = i.getWidth(null);
		int h = i.getHeight(null);
		
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		
		Graphics2D g2d = image.createGraphics();
		float opacity = Globals.opacity;
		
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
		g2d.drawImage(i, 0, 0, w, h, null);
		
		return image;
	}
	
	public static Image makeBlankImage(int w, int h){
		if(w <= 0){
			w = Constants.ICON_SIZE;
			h = Constants.ICON_SIZE;
		}
		
		BufferedImage i = new BufferedImage(w, h, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g2d = i.createGraphics();
		float opacity = 0.0f;
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
		g2d.setColor(Color.WHITE); //Doesnt' matter unless computer doesn't suppport opacity changes
		g2d.drawRect(0, 0, w, h);	
		//Fully clear image... 
		return i;
	}
}
