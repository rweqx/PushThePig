package com.rweqx.global;

import java.awt.Cursor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;

import com.rweqx.CardLogic.Card;

public class MyDragGestureListener implements DragGestureListener{
	
	
	public MyDragGestureListener() {}
	
	@Override
	public void dragGestureRecognized(DragGestureEvent event) {
		Cursor cursor = null;
		
		Card card;
		if(event.getComponent() instanceof Card){
			card = (Card) event.getComponent();
		}else{ //Must be a panel containing card... 
			card = (Card) event.getComponent().getParent();
		}
		System.out.println("Card " + card);
		
		if(event.getDragAction() == DnDConstants.ACTION_MOVE){
			cursor = DragSource.DefaultMoveDrop;
		}
		
		event.startDrag(cursor, new TransferCard(card));
	}
	

	
}
