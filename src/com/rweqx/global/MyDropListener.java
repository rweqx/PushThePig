package com.rweqx.global;

import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

import javax.swing.JPanel;

import com.rweqx.CardLogic.Card;
import com.rweqx.ui.tracker.Mover;
import com.rweqx.vars.Constants;

public class MyDropListener implements DropTargetListener {

	JPanel panel;
	Mover m;
	
	public MyDropListener(Mover m, JPanel panel){
		this.m = m;
		this.panel = panel;
	}
	
	@Override
	public void drop(DropTargetDropEvent event) {
		try{
			Transferable tr = event.getTransferable();
			
			Card c = (Card) tr.getTransferData(Constants.CARD_FLAVOUR);
			System.out.println("Adding card " + c.getName());
			DragSource ds = new DragSource();
			ds.createDefaultDragGestureRecognizer(c,  DnDConstants.ACTION_MOVE, new MyDragGestureListener());
			
			if(event.isDataFlavorSupported(Constants.CARD_FLAVOUR)){
				event.acceptDrop(DnDConstants.ACTION_MOVE);
				
				m.moveCard(c, panel);
				event.dropComplete(true);
				
				return;
			}
			
			event.rejectDrop();
		}catch(Exception e){
			e.printStackTrace();
			event.rejectDrop();
		}
		
	}

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {}
	@Override
	public void dragExit(DropTargetEvent dte) {}
	@Override
	public void dragOver(DropTargetDragEvent dtde) {}
	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {}
}
