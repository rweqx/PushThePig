package com.rweqx.global;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import com.rweqx.CardLogic.Card;
import com.rweqx.vars.Constants;

public class TransferCard implements Transferable {

	Card c;
	public TransferCard(Card c){
		this.c = c;
	}
	
	@Override
	public Object getTransferData(DataFlavor flavour) throws UnsupportedFlavorException, IOException {
		if(flavour.equals(Constants.CARD_FLAVOUR)){
			return c;
		}else{
			throw new UnsupportedFlavorException(flavour);
		}
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[] {Constants.CARD_FLAVOUR};
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavour) {
		return flavour.equals(Constants.CARD_FLAVOUR);
	}

}
