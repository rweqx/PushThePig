package com.rweqx.global;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.rweqx.vars.Constants;



public class FileImporter {
	public FileImporter(){}

	/**
	 * TAkes files f and copies it to Icons Root directory + name;
	 */
	public static String importFileForIcon(File f, String name) {
		String loc = "";
		
		File newF = new File(Constants.ICONS_ROOT + "/" + name);
		File parent = newF.getParentFile();
		if(!parent.exists()){
			parent.mkdirs();
		}
		newF.setWritable(true);
		
		//Copy.
		try{
			InputStream in = new FileInputStream(f);
			OutputStream out = new FileOutputStream(newF);
		
			byte[] buf = new byte[1024];
			int len;
			while((len = in.read(buf))>0){
				out.write(buf,0,len);
			}
			in.close();
			out.close();
			
			loc = newF.getPath();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return loc;
	}
}
