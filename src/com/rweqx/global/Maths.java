package com.rweqx.global;

/**
 * Does math with doubles, in a way that no weird rounding issues occur!
 * 
 * @author RWEQX
 *
 */
public class Maths {
	public static String percentString(int i, int j) {
		return " (" + (double) (Math.round((double) i / j * 10000)) / 100 + "%)";
	}
	
	public static Double percentCalc(int i, int j){
		return (double) (Math.round((double) i / j * 10000)) / 100;
	}
	
	public static Double decimalCalc(double i, double j){
		return (double) (Math.round((double) i / j * 100)) / 100;
	}
	
	public static Double decimalCalc(double i, double j, int digits){
		double d = Math.pow(10, digits);
		return (double) (Math.round((double) i / j * d)) / d;
	} 
	public static Double subtract(double a, double b){
		return subtract(a, b, 2);
	}
	public static double subtract(double a, double b, int digits) {
		double pow = Math.pow(10, digits);
		return (double)(Math.round(pow*(a-b)))/pow;
	}

	
	/**
	 * round to i number of digits
	 * @param round- Number to be rounded. 
	 * @param i - Number of digits to round to
	 * @return
	 */
	public static double round(double round, int i) {
		double fac = Math.pow(10, i);
		double d = ((double)Math.round(round * fac))/fac; 
		return d;
	}

	public static double multiply100(double x) {
		double b = ((double)Math.round(x * 10000.0))/100.0;
		return b; 
	}

	//faster than parse int apparently :o
	//https://stackoverflow.com/a/237204
    public static boolean isInt(String s) {
		if (s == null) {
	        return false;
	    }
	    int length = s.length();
	    if (length == 0) {
	        return false;
	    }
	    int i = 0;
	    if (s.charAt(0) == '-') {
	        if (length == 1) {
	            return false;
	        }
	        i = 1;
	    }
	    for (; i < length; i++) {
	        char c = s.charAt(i);
	        if (c < '0' || c > '9') {
	            return false;
	        }
	    }
	    return true;
		
	}

}
