package com.rweqx.global;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JPanel;

public class UIMethods {
	public static void addTo(JPanel panel, JComponent comp, int x, int y, int w, int h, double wx, double wy, 
			int fill, int anchor, int j, int k, int l, int m) {
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = x;
		c.gridy = y;
		c.gridwidth = w;
		c.gridheight = h;
		c.weightx = wx;
		c.weighty = wy; 
		c.fill = fill;
		c.anchor = anchor;
		Insets i = new Insets(j, k, l, m);
		c.insets = i;
		
		panel.add(comp, c);
		
	}
	
	public static void setLayout(JPanel panel){
		panel.setLayout(new GridBagLayout());
	}
}
