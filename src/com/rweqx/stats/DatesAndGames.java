package com.rweqx.stats;

import java.util.ArrayList;
import java.util.List;

import com.rweqx.CardLogic.Game;
import com.rweqx.PlayerLogic.Player;
import com.rweqx.global.DateMaker;
import com.rweqx.global.Logger;
import com.rweqx.global.Maths;
import com.rweqx.io.DataParser;
import com.rweqx.vars.Constants;

public class DatesAndGames {
	List<Player> players;
	
	DataParser DP;
	
	public DatesAndGames(DataParser DP, List<Player> players){
		this.DP = DP;
		this.players = players;
		build();
	}
	
	List<String> dates = null;
	
	List<Object[]>[] perfData;
	List<Object[]>[] cumPerfData;
	
	List<Object[]>[] winRateData;
	List<Object[]>[] loseRateData;
	
	public Object[][] getDailyPerformanceData(Player player) {
		int i = getIndexOfPlayer(player);
		return perfData[i].toArray(new Object[perfData[i].size()][]);
	}
	
	public Object[][] getDailyCumulativePerformance(Player player){
		int i = getIndexOfPlayer(player);
		return cumPerfData[i].toArray(new Object[cumPerfData[i].size()][]);
		
	}
	
	public Object[][] getWinRate(Player player){
		int i = getIndexOfPlayer(player);
		return winRateData[i].toArray(new Object[winRateData[i].size()][]);
		
	}
	
	public Object[][] getLoseRate(Player player){
		int i = getIndexOfPlayer(player);
		return loseRateData[i].toArray(new Object[loseRateData[i].size()][]);
		
	}
	private void build(){
		setAllDates();
		perfData = new List[players.size()];
		cumPerfData = new List[players.size()];
		
		winRateData = new List[players.size()];
		loseRateData = new List[players.size()];
		for(int i=0; i<players.size(); i++){
			perfData[i] = new ArrayList<Object[]>();
			cumPerfData[i] = new ArrayList<Object[]>();
			winRateData[i] = new ArrayList<Object[]>();
			loseRateData[i] = new ArrayList<Object[]>();
			
			readDailyPerformance(players.get(i), i);
		}
	}
	public void rebuild(List<Player> players){
		this.players = players;
		build();
	}
	/**
	 * For now, performance == AVG score of the day!
	 * 	Returns null if no games played
	 * 
	 * 
	 * Runs under assumption that records are in chronological order...
	 * @param player - Player variable to get Games and posiiton from. 
	 * @param index - number in array
	 * @return
	 */
	public void readDailyPerformance(Player player, int index){
		
		String name = player.getName();
		
		
		int cumScore = 0;
		int noGames = 0;
		
		int wins = 0;
		int losses = 0;
		
		
		for(String date : dates){
			Object o[] = new Object[2];
			Object cO[] = new Object[2];
			Object wO[] = new Object[2];
			Object lO[] = new Object[2];
			
		//	TODO REMOVE
		//	System.out.println("Checking games played on " + date);
			List<Game> games = player.getGames();
			List<Integer> pos = player.getPosition();
			int count = 0;
			int totalScore = 0;
			for(int i=0; i<games.size(); i++){
				Game g = games.get(i);
				if(g.getDate().equals(date)){
					int score =  g.getGameScores()[pos.get(i)];
					count++;
					totalScore += score;
					cumScore += score; 
					noGames ++;
					
					if(g.getWinners().contains(name)){
						wins ++;
					}
					if(g.getLosers().contains(name)){
						losses ++;
					}
					
				}
			}
			if(count != 0){
				double performance = Maths.decimalCalc(totalScore, count);
				o[0] = date;
				o[1] = performance;
				perfData[index].add(o);
			}
			if(noGames != 0 && count != 0){
				double cumPerformance = Maths.decimalCalc(cumScore, noGames);
				cO[0] = date;
				cO[1] = cumPerformance;
				cumPerfData[index].add(cO);
				
				
				double winrate = Maths.percentCalc(wins, noGames);
				double loserate = Maths.percentCalc(losses, noGames);
				
				wO[0] = date;
				lO[0] = date;
				
				wO[1] = winrate; 
				lO[1] = loserate;
				winRateData[index].add(wO);
				loseRateData[index].add(lO);
				
			}
		}
	}
	
	
	
	private int getIndexOfPlayer(Player player) {
		for(int i=0; i<players.size(); i++){
			if(player.getName().equals(players.get(i).getName())){
				return i;
			}
		}
		Logger.log("Cannot find Player with name " + player.getName(), Logger.BAD);
		return -1;
	}

	public void setAllDates(){
		String start = DP.getStartDate(); 
		String end = DP.getEndDate();
		dates = DateMaker.getAllDates(start, end);
		
	}
	public List<String> getDates() {
		return dates;
	}
	
	
}
