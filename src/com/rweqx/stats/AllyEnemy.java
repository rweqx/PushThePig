package com.rweqx.stats;

import java.util.ArrayList;
import java.util.List;

import com.rweqx.CardLogic.Game;
import com.rweqx.PlayerLogic.Player;
import com.rweqx.global.Logger;
import com.rweqx.global.Maths;
import com.rweqx.io.DataParser;

/**
 * Calculates the avg score of Player X when they are also in a game with Player Y
 * 
 * Calculates the avg score of Player X when Player Y is directly after, opposite, before them. 
 * 
 * Also WinRates... 
 * @author RWEQX
 *
 */
public class AllyEnemy {
	
	
	ScoreKeeper[][] scoreYSameGame;
	ScoreKeeper[][] scoreYAfter;
	ScoreKeeper[][] scoreYBefore;
	ScoreKeeper[][] scoreYOpposite;
	ScoreKeeper[][] winYSameGame;
	ScoreKeeper[][] winYAfter;
	ScoreKeeper[][] winYBefore;
	ScoreKeeper[][] winYOpposite;
	
	
	List<Game>[][] gamesWith;
	

	List<Player> players;
	DataParser DP;
	StatsManager SM;
	
	public AllyEnemy(DataParser DP, StatsManager SM){
		this.DP = DP;
		this.SM = SM;
		calculate();
	}

	public ScoreKeeper[][] getSameScore() {
		return scoreYSameGame;
	}
	public ScoreKeeper[][] getAfterScore() {
		return scoreYAfter;
	}
	public ScoreKeeper[][] getBeforeScore() {
		return scoreYBefore;
	}
	public ScoreKeeper[][] getOppositeScore() {
		return scoreYOpposite;
	}
	public ScoreKeeper[][] getSameWin() {
		return winYSameGame;
	}
	public ScoreKeeper[][] getAfterWin() {
		return winYAfter;
	}
	public ScoreKeeper[][] getBeforeWin() {
		return winYBefore;
	}
	public ScoreKeeper[][] getOppositeWin() {
		return winYOpposite;
	}
	
	public void calculate(){
		Logger.log("Calculating Ally/Enemy Stats... ", Logger.PROCESS);
		players = SM.getPlayers();
		
		int size = players.size();
		gamesWith = new List[size][size];
		
		scoreYSameGame = new ScoreKeeper[size][size];
		scoreYAfter = new ScoreKeeper[size][size];
		scoreYBefore = new ScoreKeeper[size][size];
		scoreYOpposite = new ScoreKeeper[size][size];
		winYSameGame = new ScoreKeeper[size][size];
		winYAfter = new ScoreKeeper[size][size];
		winYBefore = new ScoreKeeper[size][size];
		winYOpposite = new ScoreKeeper[size][size];
		
		for(int i = 0; i<size; i++){
			for(int j = 0; j<size; j++){
				scoreYSameGame[i][j] = new ScoreKeeper();
				scoreYAfter[i][j] = new ScoreKeeper();
				scoreYBefore[i][j] = new ScoreKeeper();
				scoreYOpposite[i][j] = new ScoreKeeper();
				
				winYSameGame[i][j] = new ScoreKeeper();
				winYAfter[i][j] = new ScoreKeeper();
				winYBefore[i][j] = new ScoreKeeper();
				winYOpposite[i][j] = new ScoreKeeper();
				
				gamesWith[i][j] = new ArrayList<Game>();
			}
		}
		
		readSameGames();
		
		for(int i = 0; i<size; i++){
			for(int j = 0; j<size; j++){
				if(i != j){
					Player iP = players.get(i);
					Player jP = players.get(j);
					for(Game g : gamesWith[i][j]){
						int iS = g.getPlayerFromName(iP.getName());
						int jS = g.getPlayerFromName(jP.getName());
						
						int iScore = g.getGameScores()[iS];
						
						int z = 0;
						if(g.getWinners().contains(iP.getName())){
							z = 1;
						}
						
						scoreYSameGame[i][j].addScore(iScore);
						winYSameGame[i][j].addScore(z);
						
						if((iS - jS) == 1 || (iS-jS) == -3){ //J is before I. 
							scoreYBefore[i][j].addScore(iScore);
							winYBefore[i][j].addScore(z);
						}else if((jS - iS) == 1 || (jS - iS) == -3){ //J is after I
							scoreYAfter[i][j].addScore(iScore);
							winYAfter[i][j].addScore(z);
						}else if(Math.abs(iS - jS) == 2){
							scoreYOpposite[i][j].addScore(iScore);
							winYOpposite[i][j].addScore(z);
						}else{
							System.out.println("BOOM " + iS + " " + jS);
						}
						
					}
				}
			}
		}		
	}
	
	/**
	 * Finds all games where teammates played together andd adds them to gamesWith... 
	 */
	private void readSameGames(){
		Game[] games = DP.getAllGames(null, null);
		for(Game g : games){
			String[] ps = g.getNames();
			
			for(int i=0; i<players.size(); i++){
				for(int j= (i + 1); j<players.size(); j++){
					boolean iMatch = false;
					boolean jMatch = false;
					String iName = players.get(i).getName();
					String jName = players.get(j).getName();
					for(String s : ps){
						if(s.equals(iName)){
							iMatch = true;
						}else if(s.equals(jName)){
							jMatch = true;
						}
					}
					
					if(iMatch && jMatch){ //Both players are in this game... Add game to list. 
						gamesWith[i][j].add(g);
						gamesWith[j][i].add(g);
					}
				}
			}
		}
		
	}
	
	/**
	 * easy way to keep track of both scores and number of games (count). 
	 * @author RWEQX
	 *
	 */
	public class ScoreKeeper{
		int count = 0;
		int score = 0;
		public ScoreKeeper(){
			
		}
		
		public double getAvg(){
			if(count != 0){
				return Maths.decimalCalc(score, count);
			}else{
				return 0;
			}
		}
		public void setScore(int i){
			score = i;
		}
		public void addScore(int i){
			score += i;
			count++;
		}
		public void addCount(){
			count++; // addScore(0);
		}
		public int getScore(){
			return score;
		}
		public int getCount(){
			return count;
		}
		
		
	}

}
