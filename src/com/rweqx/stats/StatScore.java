package com.rweqx.stats;

import java.util.List;

import com.rweqx.CardLogic.Game;
import com.rweqx.PlayerLogic.Player;
import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.global.Logger;
import com.rweqx.global.Maths;
import com.rweqx.io.DataParser;

/**
 * Holds stats related to scores
 * 
 * @author RWEQX
 *
 */
public class StatScore {

	DataParser DP;

	
	StatsManager SM;

	public StatScore(DataParser DP, StatsManager SM) {
		this.DP = DP;
		this.SM = SM;
	}
	
	public int getNumberOfLosses(String name) {
		int i = 0;

		List<Player> players = SM.getPlayers();
		for (Player p : players) {
			if (p.getName().equals(name)) {
				List<Game> games = p.getGames();
				for (Game g : games) {
					if (g.getLosers().contains(name)) {
						i++;
					}
				}
			}
		}
		return i;
	}

	public int getNumberOfWins(String name) {
		int i = 0;

		Player p = getPlayer(name);
		if(p != null){
			List<Game> games = p.getGames();
			for (Game g : games) {
				if (g.getWinners().contains(name)) {
					i++;
				}
			}
		
		}
		return i;
	}

	public double[] getAvgScores(Player p) {
		List<Game> games = p.getGames();
		List<Integer> positions = p.getPosition();
		
		double scoreTotal = 0;
		double nATotal = 0;
		double nTTotal = 0;
		
		double totalGames = games.size();
		double attempts = p.getNumberOfAttempts();
		double transforms = p.getNumberOfTransforms();
		
		for(int j=0; j<totalGames; j++){
			Game g = games.get(j);
			int pos = positions.get(j);
			int score = g.getGameScores()[pos];
			
			if(!g.transformSuccess(pos)){
				nTTotal+= score;
				if(!g.attemptTransform(pos)){
					nATotal += score;
				}
			}
			scoreTotal += score;
		}
		
		double[] scores = new double[3];
		scores[0] = Maths.decimalCalc(scoreTotal, totalGames, 2);
		scores[1] = Maths.decimalCalc(nATotal, (totalGames - attempts), 2);
		scores[2] = Maths.decimalCalc(nTTotal, (totalGames - transforms), 2);
		
		return scores;
	}


	/**
	 * 
	 * @param name
	 * @return wins/normal/losses/didn't play.
	 */
	public int[] getMatchWinLoss(String name) {
		int highestMatchNumber = DP.getNewestMatchGame();
		Game[] games = DP.getAllGames(null, null);
		int first = 0;
		int normal = 0;
		int last = 0;

		for (int i = 0; i < games.length; i++) {
			if (games[i].isLastGame()) {
				if (games[i].hasPlayer(name)) {
					int place = games[i].getMatchPlace(name);
					if (place == 1) {
						first++;
					} else if (place == 3) {
						last++;
					} else { // == 2
						normal++;
					}

				}
			}
		}

		int notplayed = highestMatchNumber - first - normal - last;
		
		int array[] = { first, normal, last, notplayed };
		return array;
	}

	/**
	 * Returns self stdev of score against self avg score
	 */
	public double getStandardDeviation(String name, double avgScore, 
			boolean incAttempts, boolean incTransforms) {
		Player p = getPlayer(name);
		List<Game> games = p.getGames();
		List<Integer> position = p.getPosition();
		
		double x = 0;
		int count = 0;
		
		boolean add;
		for(int i=0; i<games.size(); i++){
			Game g = games.get(i);
			int pos = position.get(i);
			add = false;
			if(incAttempts){
				if(incTransforms){
					add = true;
				}else if(!g.transformSuccess(pos)){
					add = true;
				}
			}else if(!g.attemptTransform(pos)){
				add = true;
			}
			
			if(add){
				x += Math.pow(((double)g.getGameScores()[pos] - avgScore), 2);
				count++;
			}
		}

		if(count == 0){
			return 0;
		}
		
		double stdev = Math.sqrt(x/(double)count);
		
		return Maths.round(stdev, 2); 
	}
	

	public double getStandardDeviation(Player p, int i) {
		//True true for 0, false false for 1, true false for 2. 
		boolean incA = true;
		boolean incT = true;
		if(i==1){
			incA = false;
			incT = false;
		}else if(i == 2){
			incT = false;
		}
		return getStandardDeviation(
				p.getName(), getAvgScores(p)[i], incA, incT);
	}
	

	
	private Player getPlayer(String name){
		List<Player> players = SM.getPlayers();
		for (Player p : players) {
			if (p.getName().equals(name)) {
				return p;
			}
		}
		
		Logger.log("Player not found: " + name,  Logger.BAD);
		return null;
	}


	
}
