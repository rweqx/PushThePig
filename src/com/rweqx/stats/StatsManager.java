package com.rweqx.stats;

import java.util.ArrayList;
import java.util.List;

import com.rweqx.CardLogic.Card;
import com.rweqx.CardLogic.Game;
import com.rweqx.PlayerLogic.Player;
import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.global.DateMaker;
import com.rweqx.global.Logger;
import com.rweqx.global.Maths;
import com.rweqx.io.DataParser;
import com.rweqx.vars.Constants;

import javafx.collections.ObservableList;

public class StatsManager {

	Game[] games; 
	
	
	DataParser DP;
	//PlayerReader PR;
	PlayersList PL;
	
	
	DatesAndGames DAG;
	AllyEnemy AE;
	
	StatScore SS;
	
	List<Player> players = new ArrayList<Player>();

	public void refresh(){
		long start = DateMaker.currentExactTime();
		Logger.log("Rebuilding all Stats", Logger.PROCESS);
		DP.readGameState();
		games = DP.getAllGames(null, null);
		players.clear();
		
		makePlayers();
		DAG.rebuild(players);
		AE.calculate();
		long end = DateMaker.currentExactTime();
		Logger.log("Finished Rebuild " + (end-start), Logger.PROCESS);
		
	}
	
	public StatsManager(DataParser DP, PlayersList PL){
		this.DP = DP;
		this.PL = PL;
		games = DP.getAllGames(null, null);
		makePlayers();
		
		DAG = new DatesAndGames(DP, players);
		SS = new StatScore(DP, this);
		AE = new AllyEnemy(DP, this);
	}
	
	public void makePlayers(){
		List<String> names = PL.getListOfPlayerNames();
		for(int i=0; i<names.size(); i++){
			players.add(new Player(names.get(i)));
		}
		
		for(int j=0; j<games.length; j++){
			for(int k=0; k<4; k++){
				for(int l=0; l<players.size(); l++){ 
					if(games[j].getNames()[k].equals(players.get(l).getName())){
						players.get(l).addGame(games[j], k);
						l = players.size();
					}
				}
			}
		}
	}
	
	public List<Player> getPlayers(){
		return players;
	}

	public String[][] getAverageScores() {
		List<String> playerNames = PL.getListOfPlayerNames();
		
		String data[][] = new String[players.size()][4];
		for(int i=0; i<playerNames.size(); i++){
			data[i][0] = playerNames.get(i);
			double[] avgScores = SS.getAvgScores(players.get(i));
			data[i][1] = String.valueOf(avgScores[0]); // Avg
			data[i][2] = String.valueOf(avgScores[1]); // nA Avg
			data[i][3] = String.valueOf(avgScores[2]); // nT Avg
		}
		
		return data;
	}

	public int[] getNumberOfGamesPlayed() {
		int i[] = new int[players.size()];
		for(int j=0; j<players.size(); j++){
			i[j] = players.get(j).getGames().size();
		}
		return i;
	}
	
	public int[] getNumberOfWins(){
		int i[] = new int[players.size()];
		for(int j=0; j<players.size(); j++){
			i[j] = SS.getNumberOfWins(players.get(j).getName());
		}
		
		return i;
	}
	
	public int[] getNumberOfLosses(){
		int i[] = new int[players.size()];
		for(int j=0; j<players.size(); j++){
			i[j] = SS.getNumberOfLosses(players.get(j).getName());
		}
		
		return i;
	}

	/**
	 * Match win, normal, loss, didn't play
	 * @return
	 */
	public int[][] getMatchWinLoss(){
		int i[][] = new int[players.size()][4];
		for(int j=0; j<players.size(); j++){
			i[j] = SS.getMatchWinLoss(players.get(j).getName());
		}
		
		return i;
	}
	
	/**
	 * Performance by day
	 * 
	 */
	public DatesAndGames getDateStats(){
		return DAG;
	}
	
	public Object[][] getAllNonCardStats(String name) {
		Player player = getPlayer(name);
		List<Object[]> data = new ArrayList<Object[]>();
		
		int games = player.getGames().size();
		int wins = SS.getNumberOfWins(name);
		int losses = SS.getNumberOfLosses(name);
		double avgScores[]  = SS.getAvgScores(player);
		
		data.add(makeObject("Games Played", games));
		data.add(makeObject("Games Won", wins));
		data.add(makeObject("Games Lost", losses));

		data.add(makeDivider());
		
		int i[] = SS.getMatchWinLoss(name);
		int played = i[0] + i[1] + i[2];
		data.add(makeObject("Matches Played", played));
		data.add(makeObject("Matches Won", i[0]));
		data.add(makeObject("Matches Lost", i[2]));
		
		data.add(makeObject("Avg Match Length", player.getAvgMatchLength()));

		data.add(makeDivider());
		data.add(makeObject("Avg Score", avgScores[0]));
		data.add(makeObject("Avg No Attempt Score", avgScores[1]));
		data.add(makeObject("Avg No Trans Score", avgScores[2]));
		
		
		int transform = player.getNumberOfTransforms();
		int attempts = player.getNumberOfAttempts();
		double success = Maths.percentCalc(transform, attempts);

		data.add(makeDivider());
		data.add(makeObject("Success", success + " (" + transform + "/" + attempts + ")"));
		

		data.add(makeDivider());
		//STDeviation
		double stdev = SS.getStandardDeviation(name, avgScores[0], true, true);
		
		double nAStDev = SS.getStandardDeviation(name, avgScores[1], false, false);
		double nTStDev = SS.getStandardDeviation(name, avgScores[2], true, false);
		data.add(makeObject("StDev +-", stdev));
		data.add(makeObject("StDev no A", nAStDev));
		data.add(makeObject("StDev no T", nTStDev));
		

		//Cards Captured
		double captureTotal = 0;
		double attemptCaptureTotal = 0;
		double noAttemptCaptureTotal = 0;
		double noTransformCaptureTotal = 0;
		for(int j=0; j<Constants.allCards.length; j++){
			captureTotal += player.getCardsEaten(j);
			attemptCaptureTotal += player.getCardsEatenWhenAttempt(j);
			noAttemptCaptureTotal += player.getCardsEatenNotAttempt(j);
			noTransformCaptureTotal += player.getCardsEatenNotTransform(j);
		}
		captureTotal = Maths.round(captureTotal, 2);
		attemptCaptureTotal = Maths.round(attemptCaptureTotal, 2);
		noAttemptCaptureTotal = Maths.round(noAttemptCaptureTotal, 2);
		noTransformCaptureTotal = Maths.round(noTransformCaptureTotal, 2);

		data.add(makeDivider());
		data.add(makeObject("Card Eaten", captureTotal));
		data.add(makeObject("Attempt Eaten", attemptCaptureTotal));
		data.add(makeObject("No Attempt Eaten", noAttemptCaptureTotal));
		data.add(makeObject("No Transform Eaten", noTransformCaptureTotal));
		
		data.add(makeObject("Bad Cards Eaten", player.getBadCardsEaten()));
		data.add(makeObject("Bad Cards Eaten (no attempt)", player.getBadCardNoAttemptEaten()));
		data.add(makeObject("Bad Cards Eaten (no transform)", player.getBadCardNoTransEaten()));
		data.add(makeObject("Hearts Eaten on Attempt", player.getHeartsEatenOnAttempt()));
		
		
		
		return data.toArray(new Object[data.size()][]);
	}
	
	
	

	Object[] o = Constants.DIVIDER;
	private Object[] makeDivider() {
		return o;
	}


	public int getTotalNumberOfGamesPlayed() {
		return games.length;
	}

	public int[] getNumberOfAttempts(){
		int[] i = new int[players.size()];
		for(int j=0; j<players.size(); j++){
			i[j] = players.get(j).getNumberOfAttempts();
		}
		return i;
	}
	public int[] getNumberOfTransforms() {
		int[] i = new int[players.size()];
		for(int j=0; j<players.size(); j++){
			i[j] = players.get(j).getNumberOfTransforms();
		}
		return i;
	}

	public double[][] getStdev() {
		//Cycles through: Overall STDev, STDev no Attempt, STDev no Trans
		double d[][] = new double[players.size()][3];
		for(int j=0; j<players.size(); j++){
			for(int z=0; z<3; z++){
				d[j][z] = SS.getStandardDeviation(players.get(j), z);
			}
		}
		return d;
	}

	public String[] getCardStatTitles() {
		Card card[] = Constants.allCards;
		String s[] = new String[card.length + 1];
		for(int i=0; i<card.length; i++){
			s[i+1] = card[i].getName();
		}
		s[0] = "Capture Stats";
		return s;
	}
	
	/* Methods related to total captured cards...
	 * 
	 */
	public double[] getCaptureTotals(){
		double captureTotal[] = new double[players.size()];
		
		for(int i=0; i<players.size(); i++){
			captureTotal[i] = 0;
			for(int j=0; j<Constants.allCards.length; j++){
				captureTotal[i] += players.get(i).getCardsEaten(j);
			}
			captureTotal[i] = Maths.round(captureTotal[i], 2);
		}
		return captureTotal;
	}
	
	public double[] getAttemptCaptureTotals(){
		double captureTotal[] = new double[players.size()];
		
		for(int i=0; i<players.size(); i++){
			captureTotal[i] = 0;
			for(int j=0; j<Constants.allCards.length; j++){
				captureTotal[i] += players.get(i).getCardsEatenWhenAttempt(j);
			}
			captureTotal[i] = Maths.round(captureTotal[i], 2);
		}
		return captureTotal;
	}
	
	public double[] getNoAttemptCaptureTotals(){
		double captureTotal[] = new double[players.size()];
		
		for(int i=0; i<players.size(); i++){
			captureTotal[i] = 0;
			for(int j=0; j<Constants.allCards.length; j++){
				captureTotal[i] += players.get(i).getCardsEatenNotAttempt(j);
			}
			captureTotal[i] = Maths.round(captureTotal[i], 2);
		}
		return captureTotal;
	}
	
	public double[] getNoTranformCaptureTotals(){
		double captureTotal[] = new double[players.size()];
		
		for(int i=0; i<players.size(); i++){
			captureTotal[i] = 0;
			for(int j=0; j<Constants.allCards.length; j++){
				captureTotal[i] += players.get(i).getCardsEatenNotTransform(j);
			}
			captureTotal[i] = Maths.round(captureTotal[i], 2);
		}
		return captureTotal;
	}
	
	public double[] getBadCaptureTotals(){
		double captureTotal[] = new double[players.size()];
		
		for(int i=0; i<players.size(); i++){
			captureTotal[i] += players.get(i).getBadCardsEaten();
			captureTotal[i] = Maths.round(captureTotal[i], 2);
		}
		return captureTotal;
	}
	public double[] getBadNoAttemptCaptureTotals(){
		double captureTotal[] = new double[players.size()];
		
		for(int i=0; i<players.size(); i++){
			captureTotal[i] = players.get(i).getBadCardNoAttemptEaten();
			captureTotal[i] = Maths.round(captureTotal[i], 2);
		}
		return captureTotal;
	}
	public double[] getBadNoTransCaptureTotals(){
		double captureTotal[] = new double[players.size()];
		
		for(int i=0; i<players.size(); i++){
			captureTotal[i] = players.get(i).getBadCardNoTransEaten();
			captureTotal[i] = Maths.round(captureTotal[i], 2);
		}
		return captureTotal;
	}
	public double[] getHeartsEatenOnAttempt(){
		double captureTotal[] = new double[players.size()];
		
		for(int i=0; i<players.size(); i++){
			captureTotal[i] = players.get(i).getHeartsEatenOnAttempt();
			captureTotal[i] = Maths.round(captureTotal[i], 2);
		}
		
		return captureTotal;
	}
	public double[] getAvgMatchLength(){
		double length[] = new double[players.size()];
		
		for(int i=0; i<players.size(); i++){
			length[i] = players.get(i).getAvgMatchLength();
		}
		return length;
	}
	
	
	
	
	

	public Object[][] getCardStats(String name) {
		Player player = getPlayer(name);
		List<Object[]> data = new ArrayList<Object[]>();

		int games = player.getGames().size();
		
		int nCards = Constants.allCards.length;
		Object[] totalEaten = new Object[nCards + 1];
		Object[] noTransEaten = new Object[nCards + 1];
		Object[] noAttemptEaten = new Object[nCards + 1];
		Object[] attemptEaten = new Object[nCards + 1];
		totalEaten[0] = "Total Eaten";
		noTransEaten[0] = "Eaten when not Transforming";
		noAttemptEaten[0] = "Eaten when not attempting";
		attemptEaten[0] = "Attempt";
		
		for(int j=0; j<Constants.allCards.length; j++){
			
			
			if(games != 0){
				// the /100 is to convert from decimal to percentages. 
				double k = Maths.multiply100(player.getCardsEaten(j));
				double l = Maths.multiply100(player.getCardsEatenNotTransform(j));
				double m = Maths.multiply100(player.getCardsEatenNotAttempt(j));
				double n = Maths.multiply100(player.getCardsEatenWhenAttempt(j));
				k = Maths.subtract(k, 25);
				l = Maths.subtract(l, 25);
				m = Maths.subtract(m, 25);
				//n = Maths.subtract(n, 25);
				totalEaten[j+1] = k;
				noTransEaten[j+1] = l;
				noAttemptEaten[j+1] = m;
				attemptEaten[j+1] = n;
			}else{
				totalEaten[j+1] = 0;
				noTransEaten[j+1] = 0;
				noAttemptEaten[j+1] = 0;
			}
			
		}
		data.add(totalEaten);
		data.add(noTransEaten);
		data.add(noAttemptEaten);
		data.add(attemptEaten);
			
		return data.toArray(new Object[data.size()][]);
	}

	public List<Object[][]> getAllCaptureData() {
		List<Object[][]> datadata = new ArrayList<Object[][]>();

		
		int nCards = Constants.allCards.length;
		Object[][] titles = new Object[4][nCards + 1];
		titles[0][0] = "Total Eaten";
		titles[1][0] = "No Trans Eaten";
		titles[2][0] = "No Attempt Eaten";
		titles[3][0] = "Attempt Eaten";
		
		for(int i=0; i<nCards; i++){
			for(int j=0; j<titles.length; j++){
				titles[j][i+1] = Constants.allCards[i].getName();
			}
		}
		datadata.add(titles);
		
		Object[][] totalEaten = new Object[players.size()][nCards+1];
		Object[][] noTransEaten = new Object[players.size()][nCards+1];
		Object[][] noAttemptEaten = new Object[players.size()][nCards+1];
		Object[][] attemptEaten = new Object[players.size()][nCards+1];
		
		for(int j=0; j<players.size(); j++){
			Player p = players.get(j);
			int i = p.getGames().size();
			totalEaten[j][0] = p.getName();
			noTransEaten[j][0] = p.getName();
			noAttemptEaten[j][0] = p.getName();
			attemptEaten[j][0] = p.getName();
			
			for(int k=0; k<Constants.allCards.length; k++){
				if(i !=0){
					totalEaten[j][k+1] = Maths.subtract(Maths.multiply100(p.getCardsEaten(k)), 25);
					noTransEaten[j][k+1] = Maths.subtract(Maths.multiply100(p.getCardsEatenNotTransform(k)), 25);
					noAttemptEaten[j][k+1] = Maths.subtract(Maths.multiply100(p.getCardsEatenNotAttempt(k)), 25);
					attemptEaten[j][k+1] = Maths.multiply100(p.getCardsEatenWhenAttempt(k));
				}else{
					totalEaten[j][k+1] = 0;
					noTransEaten[j][k+1] = 0;
					noAttemptEaten[j][k+1] = 0;
					attemptEaten[j][k+1] = 0;
				}
			}
		}
		datadata.add(totalEaten);
		datadata.add(noTransEaten);
		datadata.add(noAttemptEaten);
		datadata.add(attemptEaten);

		return datadata;
	}
	
	private Object[] makeObject(String ID, Object o){
		Object[] ob = {ID, o};
		return ob;
	}

	private Player getPlayer(String name) {
		for(int j=0; j<players.size(); j++){
			if(players.get(j).getName().equals(name)){
				return players.get(j);
			}
		}
		Logger.log("Player could not be found " + name, Logger.BAD);
		return null;
	}

	public AllyEnemy getAllyEnemy(){
		return AE;
	}
	public List<String> getAllDates() {
		return DAG.getDates();
	}
	
	
}
