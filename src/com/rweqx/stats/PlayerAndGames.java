package com.rweqx.stats;

import java.util.ArrayList;
import java.util.List;

import com.rweqx.CardLogic.Game;

public class PlayerAndGames {

	public PlayerAndGames(){
		
	}
	
	/**
	 * Takes all current games and name and returns all games played with that player... 
	 */
	public List<Game> getAllGamesWithX(Game[] currentGameState, String name){
		List<Game> games = new ArrayList<Game>();
		
		String s[];
		
		for(Game g : currentGameState){
			s = g.getNames();
			for(int i=0; i<4; i++){
				if(s[i].equals(name)){
					games.add(g);	
				}
			}
		}
		return games;
	}
		
}
