package com.rweqx.main;

import javax.swing.SwingUtilities;

import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.global.DateMaker;
import com.rweqx.global.Logger;
import com.rweqx.global.Updater;
import com.rweqx.io.DataParser;
import com.rweqx.io.GameStateWriter;
import com.rweqx.io.PlayerJReader;
import com.rweqx.io.PlayerJWriter;
import com.rweqx.io.Reader;
import com.rweqx.settings.Settings;
import com.rweqx.settings.SettingsProperties;
import com.rweqx.stats.StatsManager;
import com.rweqx.ui.Window;
import com.rweqx.vars.Globals;

import javafx.application.Platform;

public class Main {
	
	
	public static void main(String args[]){		
		boolean b = true;
		if(args.length != 0){
			System.out.println("args" + args.length);
			b = Boolean.parseBoolean(args[0]);
		}
		Main m = new Main(b);
	}
	
	Updater u;
	Window w;
	Settings settings;
	SettingsProperties SP;
	PlayerJWriter PJW;
	PlayersList PL;
	
	
	//Starts program
	public Main(boolean checkForUpdate){
		
		settings = new Settings();
		SP = settings.getProperties();
		Globals globals = new Globals();
		
		Logger logger = new Logger(settings);
		
		
		u = new Updater(this);
		
		start();
		
		if(checkForUpdate){
			Globals.lastCheckUpdateTime = "Last Checked: " + DateMaker.getDateYYYYMMDD() + " " + DateMaker.getDateHHMMSS();
			if(u.isUpdateAvailable()){
				java.awt.EventQueue.invokeLater(new Runnable(){
					@Override
					public void run(){
						u.runUpdate();
					}
				});
			}else{
				show();
			}
		}else{
			show();
		}		
	}
	
	//Displays window for users, waiting for the thread initiating the UI to complete... 
	public void show(){
		while(thread.isAlive()){
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		SwingUtilities.invokeLater(()->w.showWindow());
	}
	
	Thread thread;
	
	//Initiates thread to start UI stuff... Does NOT display window.
	private void start(){
		thread = new Thread(()->{
				Platform.setImplicitExit(false);
				Long start = DateMaker.currentExactTime();
			
				//Initialize Logger. 
				
				Logger.log("Initializing Program", Logger.PROCESS);
				
				
				
				PlayerJReader PJR = new PlayerJReader();
				PJW = new PlayerJWriter(PJR);
				PJR.readData();
				PL = new PlayersList(PJR.getPlayerData(), PJW);

				Reader reader = new Reader();
				GameStateWriter writer = new GameStateWriter(PL);
				
				
				Logger.log("Reading Game States", Logger.PROCESS);
				DataParser DP = new DataParser(writer, reader);
				DP.readGameState();
				

				Logger.log("Creating Stats", Logger.PROCESS);
				StatsManager SM = new StatsManager(DP, PL);

				Logger.log("Creating UI", Logger.PROCESS);
				w = new Window(writer, DP, SM, u, PL, SP, this);
				

				Logger.log("Reading and Setting Latest Game", Logger.PROCESS);
				//Read stuff from last time, unless there is no save stuff, in which go to defaults!
				int i = DP.getNewestMatchGame();
				if(i != -1){
					w.getTrackP().setMatchGame(DP.getNewestMatchGame()+1, 1); //Sets game to be 2-1 if the last recorded game was 1-3 for example.
				}
				
				String s[] = DP.getLastPlayers();
				if(s != null){
					w.getTrackP().getPlayerArea().setPlayerNames(s); //Keep the player names from last time
				}
				
				

				Long end = DateMaker.currentExactTime();
				Logger.log("Program initialization complete.", Logger.PROCESS);

				Logger.log("Time Taken: " + (double)(end-start)/1000 + " seconds", Logger.PROCESS);
				
			});
		thread.start();
		
	}
	
	public void saveAndExit(){

		Logger.log("Saving and Closing", Logger.PROCESS);
		settings.getWriter().save(SP);
		PJW.save(PL.getPData());
		System.exit(0);
	}
}
