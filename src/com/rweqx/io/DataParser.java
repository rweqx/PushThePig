package com.rweqx.io;

import java.util.ArrayList;
import java.util.List;

import com.rweqx.CardLogic.Game;
import com.rweqx.global.DateMaker;
import com.rweqx.global.Logger;

public class DataParser {

	GameStateReader GSR;
	GameStateWriter GSW;
	
	Game[] currentGameState;
	
	boolean read = false;

	public void readGameState(){
		currentGameState = GSR.readData(null, null);
		startDate = setStart();
		endDate = setEnd();
		read = true;
	}
	
	
	public DataParser(GameStateWriter GSW, Reader reader){
		this.GSW = GSW;
		GSR = new GameStateReader(GSW, reader);
		
	}
	String startDate;
	String endDate;
	
	public Game[] getAllGames(String startDate, String endDate){
		return currentGameState; //TODO - currently gives all dates, later will give only ones in that date frame.
	}
	
	private String setStart(){
		int first = DateMaker.convertDateStringToInt(DateMaker.getDateYYYYMMDD()); 
		for(Game g : currentGameState){
			int i = DateMaker.convertDateStringToInt(g.getDate());
			if(i != -1 && i < first){
				first = i;
			}
		}	
		return DateMaker.convertIntToDateString(first);
	}
	private String setEnd(){
		int last = 0; 
		for(Game g : currentGameState){
			int i = DateMaker.convertDateStringToInt(g.getDate());
			if(i > last){
				last = i;
			}
		}
		
		//Should be impossible unless just first time running the app, in which case... 
		if(last == 0){
			last = DateMaker.convertDateStringToInt(DateMaker.getDateYYYYMMDD());
		}

		return DateMaker.convertIntToDateString(last);
	}
	
	public String getStartDate(){
		return startDate;
	}
	public String getEndDate(){
		return endDate;
	}
	
	
	private void readIf(){
		if(!read){
			readGameState();
		}
	}
	
	public int getNewestMatchGame() {
		if(isEmpty()){
			return -1;
		}
		readIf();
		int m = 1;
		for(Game g : currentGameState){
			int i = g.getMatchNumber();
			if(i > m){
				m = i;
			}
		}
		return m;
	}
	
	public String[] getLastPlayers(){
		if(isEmpty()){
			return null;
		}
		
		Game g = currentGameState[currentGameState.length-1];
		return g.getNames();
		
	}
	
		
	private boolean isEmpty(){
		return (currentGameState.length == 0); 
	}

	public Game getLastPlayedGame() {
		readGameState();
		
		int i = getNewestMatchGame(); //Last Match
		int j = getLastGameOfMatch(i);
		
		Game g = getGameFromMatchGame(i, j);
		return g;
	}
	
	public Game getPreviousGameOf(Game g) {
		int match = g.getMatchNumber();
		int game = g.getGameNumber();
		return getGameFromMatchGame(match, game-1);
	}
	
	
	private boolean gameExists(int m, int g){
		return (getGameFromMatchGame(m, g) != null);
	}
	
	public Game getGameFromMatchGame(int m, int g){
		for(Game game : currentGameState){
			if(game.getGameNumber() == g && game.getMatchNumber() == m){
				return game;
			}
		}
		
		Logger.log("Could NOT find Game with ID " + m + " " + g, Logger.PROCESS);
		return null;
	}

	

	public int getLastGameOfMatch(int i) {
		for(Game g : currentGameState){
			int j = g.getMatchNumber();
			if(j == i){
				if(g.isLastGame()){
					return g.getGameNumber();
				}
			}
		}
		Logger.log("Cannot find last game of match for match: " + i, Logger.BAD);
		return -1;
	}
	
	
	public void saveGame(String[][] compileScores) {
		int m = Integer.parseInt(compileScores[0][0]);
		int g = Integer.parseInt(compileScores[0][1]);
		GSW.writeGameState(compileScores, gameExists(m,g));
		
	}

	
	
	
}
