package com.rweqx.io;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rweqx.PlayerLogic.PlayerData;
import com.rweqx.global.Logger;
import com.rweqx.vars.Constants;

public class PlayerJWriter {
	PlayerJReader reader;
	
	ObjectMapper mapper;
	public PlayerJWriter(PlayerJReader reader){
		this.reader = reader;
		mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		
		checkFile();
	}
	
	File f;
	
	private void checkFile(){
		f = new File(Constants.PLAYER_DATA_FILE);
		if(!f.exists()){
			f.getParentFile().mkdirs();
			try{
				f.createNewFile();
				List<PlayerData> pData = new ArrayList<PlayerData>();
				save(pData);
				Logger.log("Creating new Player List File as none exists", Logger.PROCESS);
				f.setWritable(true);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public void save(List<PlayerData> pData){
		try{
			mapper.writerWithDefaultPrettyPrinter().writeValue(f, pData);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
