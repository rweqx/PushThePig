package com.rweqx.io;

import java.io.File;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rweqx.settings.SettingsProperties;
import com.rweqx.vars.Constants;

public class SettingsJReader {

	public SettingsJReader(){
		factory = new JsonFactory();
		
	}	
	
	JsonFactory factory;
	ObjectMapper mapper = new ObjectMapper();
	SettingsProperties properties = null;
	

	public void readFile(){
		File file = new File(Constants.SETTINGS_FILE);
		try{
			 properties = mapper.readValue(file, SettingsProperties.class);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public SettingsProperties getProperties() {
		return properties;
	}
}
