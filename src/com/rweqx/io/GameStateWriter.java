package com.rweqx.io;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

import com.rweqx.PlayerLogic.PlayersList;
import com.rweqx.global.DateMaker;
import com.rweqx.global.Logger;
import com.rweqx.vars.Constants;

public class GameStateWriter {

	/**
	 * Game State:
	 * {
	 * 		{Match#, Game#}
	 * 		{P1Name, P1MatchScore, P1GameScore, ... (cards)}
	 * 		{P2...}
	 * 		... (to P4)
	 * }
	 * @param compileScores
	 */
	
	File saveFile;
	FileWriter FW;
	FileReader FR;
	
	
	PlayersList PL;
	
	Reader reader; 
	
	public GameStateWriter(PlayersList PL){
		this.PL = PL;
		reader = new Reader();
		
		String s = Constants.DATA_FILE;
		saveFile = new File(s);
		if(!saveFile.exists()){
			try {
				Logger.log("Game Save data file not found, creating new blank one", Logger.PROCESS);
				saveFile.getParentFile().mkdirs();
				saveFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/*
		try {
			FW = new FileWriter(saveFile, true);
			FR = new FileReader(saveFile);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}
	
	public File getFile(){
		return saveFile;
	}
	
	public void writeGameState(String[][] gameState, boolean overwrite) {
		if(overwrite){
			findAndOverwrite(gameState);
			int i = 0;

			File f = new File(Constants.ROOT_DIR + "Files/BACKUP.txt");
			while(f.exists()){
				i++;
				f = new File(Constants.ROOT_DIR + "Files/BACKUP_" + i + ".txt");	
			}
			
			Logger.log("Since overwriting, saved a backup version of data @ "+ f.getAbsolutePath() + 
					"\nNote that as we are still in development, it is likely that data.txt may get corrupted at any time...",
					Logger.PROCESS);
			f.getParentFile().mkdirs();
			
			try {
				Files.copy(saveFile.toPath(), f.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			writeGameToEnd(gameState);
		}
		
	}
	
	private void findAndOverwrite(String[][] gameState) {
		int Match = Integer.parseInt(gameState[0][0]);
		int Game = Integer.parseInt(gameState[0][1]);
		String s = buildGameString(gameState);
		
		String MatchGame = "[" + Match + "-" + Game + "]";
		
		String data[] = reader.readSaveFile(saveFile);
		
		try{
			FW = new FileWriter(saveFile, false);
			for(int i=0; i<data.length; i++){
				if(data[i].startsWith(MatchGame)){
					FW.write(s);
					Logger.log("Overwriting game ", Logger.PROCESS);
				}else{
					FW.write(data[i]);
				}
				FW.write(System.lineSeparator());
			}
			FW.flush();
			FW.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	
	}
	
	//OLD GAME SAVE WRITER!
	/*//TODO - DELETE
	private void backupWrite(String[][] gameState){
		try{
			String MatchGameNumber = gameState[0][0] + "-" + gameState[0][1];
			String playerData[] = new String[4];

			FW.write("[" + MatchGameNumber + "]");
			
			FW.write("{" + DateMaker.getDateYYYYMMDD() + "}");
			
			for(int i=0; i<4; i++){
				playerData[i] = buildPlayerString(gameState[i+1]);
				FW.write(playerData[i]);
			}
			FW.write("[/" + MatchGameNumber + "]");
			FW.write(System.lineSeparator());
			
			FW.flush();
		
		}catch(IOException e){
			e.printStackTrace();
		}
	}*/
	
	private void writeGameToEnd(String[][] gameState){
		try{
			FW = new FileWriter(saveFile, true);
			String s = buildGameString(gameState); 
			FW.write(s);
			FW.write(System.lineSeparator());
			FW.flush();
			FW.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	
	private String buildGameString(String[][] gameState){
		String s = "";
		String MatchGameNumber = gameState[0][0] + "-" + gameState[0][1];
		s += "[" + MatchGameNumber + "]";
		s += "{" + DateMaker.getDateYYYYMMDD() + "}";
		
		for(int i=0; i<4; i++){
			s += buildPlayerString(gameState[i+1]);
		}
		s += "[/" + MatchGameNumber + "]";
		return s;
	}
	
	private String buildPlayerString(String[] s){
		String ss;
		
		String name = s[0];
		String mScore = s[1];
		String gScore = s[2];
		String bAttempt = s[3];
		String pos = s[4];
		
		String cards = "";
		if(s.length >= 5){
			for(int i=5; i<s.length; i++){
				cards = cards + ";" + s[i];
			}
		}
		
		ss = "{" + name + "|(M:" + mScore + ")(G:" + gScore + ")(A:" + bAttempt + ")(Pos:" + pos + ")|" + cards + "|" + "}";
		
		PL.addIfNew(name);
		
		return ss;
	}

}
