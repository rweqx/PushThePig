package com.rweqx.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.rweqx.CardLogic.Game;
import com.rweqx.global.DateMaker;
import com.rweqx.global.Logger;

public class GameStateReader {

	GameStateWriter GSW;
	Reader reader;
	
	public GameStateReader(GameStateWriter GSW, Reader reader){
		this.GSW = GSW;
		this.reader = reader;
	}
	
	public Game[] readData(String startDate, String endDate){
		
		long timeStart = DateMaker.currentExactTime();
		

		Logger.log("Starting to read save data", Logger.PROCESS);
		
		String s[] = reader.readSaveFile(GSW.getFile());
		
		Game g[] = new Game[s.length];
		
		for(int i=0; i<s.length; i++){
			g[i] = new Game(s[i]);
			if(i>0 && g[i].getGameNumber() == 1){
				g[i-1].setLastGameOfMatch(true);
			}else if (i == (s.length-1)){
				g[i].setLastGameOfMatch(true);
			}
			
			
		}
		
		long timeEnd = DateMaker.currentExactTime();
		
		Logger.log("Time Taken to read Save Data " + (timeEnd-timeStart) + " ms", Logger.PROCESS);
		
		return g;
		
	}	
	
}
