package com.rweqx.io;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.rweqx.PlayerLogic.PlayerData;
import com.rweqx.vars.Constants;

/**
 * JSON Reader
 * @author RWEQX
 *
 */
public class PlayerJReader {
	
	ObjectMapper mapper;
	
	List<PlayerData> pData = new ArrayList<PlayerData>();
	
	public PlayerJReader(){
	}
	
	public void readData(){
		File f = new File(Constants.PLAYER_DATA_FILE);
		mapper = new ObjectMapper();
		TypeFactory typeFactory = mapper.getTypeFactory();
		
		try{
			pData = mapper.readValue(f, typeFactory.constructCollectionType(List.class, PlayerData.class));
			System.out.println("Players " + pData.size());
			for(PlayerData p : pData){
				System.out.println(p.getName());
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public List<PlayerData> getPlayerData(){
		return pData;
	}
	
	
}
