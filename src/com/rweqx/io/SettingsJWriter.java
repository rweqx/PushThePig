package com.rweqx.io;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rweqx.settings.SettingsProperties;
import com.rweqx.vars.Constants;

public class SettingsJWriter {
	
	SettingsJReader reader;

	JsonFactory factory;
	JsonGenerator generator;
	
	public SettingsJWriter(SettingsJReader reader){
		factory = new JsonFactory();
		
		createGenerator();
	}
	
	File f;
	
	private void createGenerator(){
		System.out.println("Creating Generator");
		f = new File(Constants.SETTINGS_FILE);
		if(!f.exists()){
			f.getParentFile().mkdirs();
			try{
				f.createNewFile();
				
				writeDefaultFile();
				System.out.println("Creating new Settings File");
			}catch(IOException e){
				e.printStackTrace();
			}
			f.setWritable(true);
			
		}
	}
	
	public void writeDefaultFile(){
		save(new SettingsProperties());
	}
	
	public void save(){
		save(reader.getProperties());
	}
	
	public void save(SettingsProperties properties){
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(f, properties);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	
}
