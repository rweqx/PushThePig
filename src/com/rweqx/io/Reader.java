package com.rweqx.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads file and returns it in an Array. 
 * @author RWEQX
 *
 */
public class Reader {
	
	public String[] readSaveFile(String s){
		return readSaveFile(new File(s));
	}
	
	public String[] readSaveFile(File saveFile){
		List<String> data = new ArrayList<String>();
		
		FileReader FR;
		BufferedReader BR;
		
		try {
			FR = new FileReader(saveFile);
			BR  = new BufferedReader(FR);
			String s;
			while((s = BR.readLine()) != null){
				data.add(s);
			}	
			BR.close();

			FR.close();
			
		} catch (FileNotFoundException e) {
			System.err.println("Reading Save File Failed");
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			System.err.println("Reading Save File Failed");
			e.printStackTrace();
			return null;
		}finally{
			
		}
		

		return (String[]) data.toArray(new String[data.size()]);
		
	}
	
}
