package com.rweqx.CardLogic;

import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;
import java.io.IOException;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import com.rweqx.ccomp.CardButton;
import com.rweqx.ccomp.CardImageFactory;
import com.rweqx.global.MyDragGestureListener;
import com.rweqx.global.UIMethods;
import com.rweqx.vars.Constants;


public class Card extends JPanel {

	
	String name;
	int score;
	
	transient Image suitImg;
	
	transient CardButton bName;
	
	/*
	 * Allows for importing of cards. -> Duplicates given card. 
	 */
	public Card(Card card) {
		this.name = card.getName();
		this.score = card.getScore();
		this.suitImg = card.getImg();
		makePanel();
	}
	
	public Card(String name, int score){
		this.name = name;
		this.score = score;

		//Technically slow, but Java seems to be smart enough to copy it?
		CardImageFactory CIF = new CardImageFactory();
		this.suitImg = CIF.getSuitImage(name.substring(0, 1));

		makePanel();
	}
	
	private void makePanel(){
		this.setOpaque(false);
		UIMethods.setLayout(this);
		
		CardButton x = new CardButton();
		x.setText(name.substring(1).trim());
		x.setIcon(new ImageIcon(suitImg));
		UIMethods.addTo(this, x, 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE, GridBagConstraints.CENTER, 0, 0, 0, 0);
		
		
		DragSource ds = new DragSource();
		ds.createDefaultDragGestureRecognizer(this,  DnDConstants.ACTION_MOVE, new MyDragGestureListener());
		ds.createDefaultDragGestureRecognizer(x,  DnDConstants.ACTION_MOVE, new MyDragGestureListener());
		
	}
		
	public String getName(){
		return name;
	}
	
	public int getScore(){
		return score;
	}
	
	public Image getImg(){
		return suitImg;
	}


	
}
