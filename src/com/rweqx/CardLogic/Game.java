package com.rweqx.CardLogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.rweqx.global.Logger;
import com.rweqx.vars.Constants;

/**
 * 
 * Data type that holds:
 * Game Match
 * Date
 * Player Names
 * Player Scores
 * Player Cards
 * 
 * @author RWEQX
 *
 */
public class Game {
	String date;
	int MatchNumber;
	int GameNumber;
	
	String[] playerNames = new String[4];
	Card[][] playerCards = new Card[4][16]; //Max eat is 16 cards.

	
	int[] playerMatchScore = new int[4];
	int[] playerGameScore = new int[4];
	
	boolean[] playerAttempt = new boolean[4];
	
	String[] playerPos = new String[4];
	
	boolean isLastGameOfMatch = false;
	
	int[] matchPlace = new int[4];
	int lastMatchPlaceNumber;
	
	int[] gamePlace = new int[4];
	int lastGamePlaceNumber;
	
	List<Card> TRANSFORM_CARDS = Arrays.asList(Constants.TransCards);
	
	public Game(int Match, int Game,  String[] playerNames, Card[][] playerCards,
			int[] playerMatchScore,  boolean[] playerAttempt, String[] playerPos){
		this.MatchNumber = Match;
		this.GameNumber = Game;
		this.playerNames = playerNames;
		this.playerCards = playerCards;
		this.playerMatchScore = playerMatchScore;
		this.playerAttempt = playerAttempt;
		this.playerPos = playerPos;
	}
	
	//Converts String to Game Element. 
	public Game(String s){
		String MatchGameSub = s.substring(
				s.indexOf("[") + 1,
				s.indexOf("]"));
		
		MatchNumber = Integer.parseInt(MatchGameSub.substring(
				0, 
				MatchGameSub.indexOf("-")));
		GameNumber = Integer.parseInt(MatchGameSub.substring(
				MatchGameSub.indexOf("-") + 1));
		
		
		int index = s.indexOf("}");
		date = s.substring(
				s.indexOf("{") + 1,
				index);
		
		
		String rest = s.substring(index + 1);
		
		String playerData[] = new String[4];
		for(int i=0; i<4; i++){
			int start = rest.indexOf("{") + 1;
			int end = rest.indexOf("}");
			playerData[i] = rest.substring(start, end);
			rest = rest.substring(end + 1);
			
			readPlayerData(playerData[i], i);
		}
		
		getPlaces();
	}
	private void getPlaces(){
		List<Integer> scores = new ArrayList<Integer>();
		for(int i : playerGameScore){
			scores.add(i);
		}
		
		scores.sort(Collections.reverseOrder());
		
		int placeX = 1;
		int lastValue = -10000;
		for(int i=0; i<4; i++){
			int s = scores.get(i);
			if(s != lastValue){
				lastValue = s;
				for(int j=0; j<4; j++){
					if(playerGameScore[j] == s){
						gamePlace[j] = placeX;
					}
				}
				placeX++;
			}
		}
		lastGamePlaceNumber = (placeX - 1);
		
		
		//Match Place: Either 1 2 3 (1 if won, 2 if neither, 3 if lost)
		for(int i=0; i<4; i++){
			if(playerMatchScore[i] >= Constants.WIN_SCORE){
				matchPlace[i] = 1;
				isLastGameOfMatch = true; //Since someone won, must be last game of match. 
			}else if(playerMatchScore[i] <= Constants.LOSE_SCORE){
				matchPlace[i] = 3;
				isLastGameOfMatch = true; //Since someone lost, must be last game of match. 
			}else{
				matchPlace[i] = 2;
			}
		}
	}
	private void readPlayerData(String s, int i){
		int indexS;
		int indexE = s.indexOf("|");
		playerNames[i] = s.substring(0, indexE);
		
		
		s = s.substring(indexE+1);
		
		indexS = s.indexOf(":");
		indexE = s.indexOf(")");
		
		playerMatchScore[i] = Integer.parseInt(
				s.substring(indexS + 1, indexE));
		
		s = s.substring(indexE+1);
		indexS = s.indexOf(":");
		indexE = s.indexOf(")");
		
		playerGameScore[i] = Integer.parseInt(
				s.substring(indexS+1, indexE));
		
		s = s.substring(indexE+1);
		indexS = s.indexOf(":");
		indexE = s.indexOf(")");
		
		playerAttempt[i] = Boolean.parseBoolean(s.substring(
				indexS+1, indexE));

		
		s = s.substring(indexE+1);
		indexS = s.indexOf(":");
		indexE = s.indexOf(")");
		playerPos[i] = s.substring(indexS+1, indexE);
		
		s = s.substring(indexE + 1);
		
		int currentCard = 0;
		while((indexS = s.indexOf(";")) != -1){
			if((indexE = s.indexOf(";", indexS+1)) == -1){
				indexE = s.indexOf("|", indexS+1);
			}
			
			
			playerCards[i][currentCard] = getCardFromName(
					s.substring(
							indexS + 1,
							indexE));
			currentCard ++;
			
			s = s.substring(indexE);
			
		}
		
	}
	
	private Card getCardFromName(String name){		
		for(int i=0; i<Constants.allCards.length; i++){
			if(Constants.allCards[i].getName().equals(name)){
				return Constants.allCards[i];
			}
		}
		
		Logger.log("Error, card not found for name " + name, Logger.BAD);
		return null;
		
	}

	public int getMatchNumber() {
		return MatchNumber;
	}

	public String[] getNames() {
		return playerNames;
	}

	public int getGameNumber() {
		return GameNumber;
	}

	public int[] getMatchScores() {
		return playerMatchScore;
	}

	public int[] getGameScores() {
		return playerGameScore;
	}
	
	public String getPos(int i){
		return playerPos[i];
	}
	public boolean isAttempt(int i){
		return playerAttempt[i];
	}
	
	public List<String> getWinners(){
		List<String> s = new ArrayList<String>();
		
		/*//TODO Remove Obsolete. 
		int index = 0;
		s.add(playerNames[index]);
		
		for(int i=1; i<4; i++){
			if(playerGameScore[i] > playerGameScore[index]){
				index = i;
				s.clear();
				s.add(playerNames[i]);
			}else if(playerGameScore[i] == playerGameScore[index]){
				s.add(playerNames[i]);
			}
		}
		*/
		for(int i=0; i<4; i++){
			if(gamePlace[i] == 1){
				s.add(playerNames[i]);
			}
		}
		
		
		/*//TODO remove debugging.
		if(s.size() > 1){
			Logger.log("More than one winner ", Logger.PROCESS);
			for(int i=0; i<4; i++){
				Logger.log("" + playerGameScore[i], Logger.PROCESS);
			}
		}
		*/
		
		return s;
		//return s.toArray(new String[s.size()]);
	}
	
	public List<String> getLosers(){
		List<String> s = new ArrayList<String>();
		
		/*//TODO - Remove obsolete. 
		int index = 0;
		
		s.add(playerNames[index]);
		
		for(int i=1; i<4; i++){
			if(playerGameScore[i] < playerGameScore[index]){
				index = i;
				s.clear();
				s.add(playerNames[i]);
			}else if(playerGameScore[i] == playerGameScore[index]){
				s.add(playerNames[i]);
			}
		}
		*/
		
		//Add all who are last place
		for(int i=0; i<4; i++){
			if(gamePlace[i] == lastGamePlaceNumber){
				s.add(playerNames[i]);
			}
		}
		
		
		/*//TODO remove debugging. 
		if(s.size() > 1){
			Logger.log("More than one loser ", Logger.PROCESS);
			for(int i=0; i<4; i++){
				Logger.log("" + playerGameScore[i], Logger.PROCESS);
			}
		}
		*/

		return s;
		//return s.toArray(new String[s.size()]);
	}

	public boolean isLastGame(){
		return isLastGameOfMatch;
	}
	public void setLastGameOfMatch(boolean b) {
		isLastGameOfMatch = b;		
	}

	public int getMatchPlace(String name) {
		for(int i = 0; i<4; i++){
			if(name.equals(playerNames[i])){
				return matchPlace[i];
			}
		}
		return -1; 
	}
	
	public boolean hasPlayer(String name) {
		for(int i=0; i<4; i++){
			if(playerNames[i].equals(name)){
				return true;
			}
		}
		return false;
	}
	public String getDate() {
		return date;
	}
	public List<Card> getCards(int i) {
		List<Card> cards = new ArrayList<Card>();
		for(Card c : playerCards[i]){
			if(c != null){
				cards.add(c);
			}
		}
		return cards;
	}
	public List<Card> getCards(String name) {
		int i = getPlayerFromName(name);
		return getCards(i);
	}
	
	public int getPlayerFromName(String name){
		for(int i=0; i<4; i++){
			if(name.equals(playerNames[i])){
				return i;
			}
		}
		return -1;
	}
	
	public boolean transformSuccess(int position) {
		List<Card> cards = new ArrayList<Card>();
		for(Card c : playerCards[position]){
			if(c!=null){
				cards.add(c);
			}
		}
		return cards.containsAll(TRANSFORM_CARDS);			
	}
	
	public boolean attemptTransform(int position) {
		return playerAttempt[position];
	}
	
	
	
}
