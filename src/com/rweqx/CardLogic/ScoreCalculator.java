package com.rweqx.CardLogic;

import java.awt.List;
import java.util.ArrayList;

import com.rweqx.vars.Constants;

public class ScoreCalculator {

	//Sum up score values of all cards present
	public static int updateScore(ArrayList<Card> cards, boolean someoneTransformed, boolean iTransformed){
		
		int score = 0;
		if(!iTransformed){
			boolean d = false; // Double?
			boolean empty = true;
			
			for(int i=0; i<cards.size(); i++){
				int a = cards.get(i).getScore();
				if(a == 0){
					d = true;
				}else{
					score += a;
					if(empty)
						empty = false;
				}
			}
		
			if(d && empty){
				score = 50;
			}else if(d){
				score *= 2;
			}
			
			if(someoneTransformed){
				score = score*-1;
			}
			
		}else{
			ArrayList<String> names = new ArrayList<String>();
			
			for(Card c : cards){
				names.add(c.getName());
			}
			
			score = 200;
			boolean SQ = false;
			boolean DJ = false;
			
			if(names.contains(Constants.SQ.getName())){
				SQ = true;
			}
			if(names.contains(Constants.DJ.getName())){
				DJ = true;
			}
			
			if(SQ && DJ){
				score += 300;
			}else if(SQ){
				score += 100;
			}else if(DJ){
				score -= 100;
			}
			
			if(names.contains(Constants.C10.getName())){
				score *= 2;
			}
		}
		return score;
	}
	
	

	public static int compare(Card c1, Card c2){
		int a = c1.getScore();
		int b = c2.getScore();
		
		boolean specialA = false;
		boolean specialB = false;
		
		if(a == 100 || a == -100 || a == 0){
			specialA = true;
		}
		if(b == 100 || b == -100 || b == 0){

			specialB = true;
		}
		
		if(specialA && specialB){
			return a - b; // returns positive if A larger, negative if smaller 
			//Technically can combine, but will keep separate for noticeability purposes...
		}else if(specialA){
			return 1; 
		}else if(specialB){
			return -1;
		}else{
			return a - b;
		}
	}


}
